--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.9
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: discount_type; Type: TYPE; Schema: public; Owner: printa
--

CREATE TYPE discount_type AS ENUM (
    'fixed',
    'percentage'
);


ALTER TYPE discount_type OWNER TO printa;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: assigned_orders; Type: TABLE; Schema: public; Owner: printa
--

CREATE TABLE assigned_orders (
    "order" json NOT NULL,
    assigned_to character varying NOT NULL,
    creation_date timestamp with time zone
);


ALTER TABLE assigned_orders OWNER TO printa;

--
-- Name: discount_codes; Type: TABLE; Schema: public; Owner: printa
--

CREATE TABLE discount_codes (
    code character varying,
    type discount_type,
    amount numeric,
    used integer,
    max_uses integer
);


ALTER TABLE discount_codes OWNER TO printa;

--
-- Name: products_laser; Type: TABLE; Schema: public; Owner: printa
--

CREATE TABLE products_laser (
    id integer NOT NULL,
    material text NOT NULL,
    color text DEFAULT 'No Aplica'::text NOT NULL,
    espesor numeric NOT NULL,
    cantidad numeric NOT NULL,
    cantidad_actual numeric NOT NULL,
    categoria text DEFAULT 'laser'::text NOT NULL,
    habilitado boolean DEFAULT true NOT NULL
);


ALTER TABLE products_laser OWNER TO printa;

--
-- Name: products_laser_id_seq; Type: SEQUENCE; Schema: public; Owner: printa
--

CREATE SEQUENCE products_laser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_laser_id_seq OWNER TO printa;

--
-- Name: products_laser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: printa
--

ALTER SEQUENCE products_laser_id_seq OWNED BY products_laser.id;


--
-- Name: products_normal; Type: TABLE; Schema: public; Owner: printa
--

CREATE TABLE products_normal (
    id integer NOT NULL,
    material text NOT NULL,
    tamano text NOT NULL,
    gramaje text NOT NULL,
    cantidad numeric NOT NULL,
    cantidad_alerta numeric NOT NULL,
    habilitado boolean DEFAULT true NOT NULL,
    tipo text DEFAULT 'normal'::text NOT NULL
);


ALTER TABLE products_normal OWNER TO printa;

--
-- Name: products_normal_id_seq; Type: SEQUENCE; Schema: public; Owner: printa
--

CREATE SEQUENCE products_normal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_normal_id_seq OWNER TO printa;

--
-- Name: products_normal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: printa
--

ALTER SEQUENCE products_normal_id_seq OWNED BY products_normal.id;


--
-- Name: products_vinyl; Type: TABLE; Schema: public; Owner: printa
--

CREATE TABLE products_vinyl (
    id integer NOT NULL,
    color text NOT NULL,
    acabado text NOT NULL,
    formato text DEFAULT 'Rollo'::text NOT NULL,
    gramaje numeric NOT NULL,
    metros_actuales numeric NOT NULL,
    metros_ancho numeric NOT NULL,
    metros_alerta numeric NOT NULL,
    habilitado boolean DEFAULT true NOT NULL,
    categoria text DEFAULT 'vinyl'::text NOT NULL
);


ALTER TABLE products_vinyl OWNER TO printa;

--
-- Name: products_vinyl_id_seq; Type: SEQUENCE; Schema: public; Owner: printa
--

CREATE SEQUENCE products_vinyl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_vinyl_id_seq OWNER TO printa;

--
-- Name: products_vinyl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: printa
--

ALTER SEQUENCE products_vinyl_id_seq OWNED BY products_vinyl.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: printa
--

ALTER TABLE ONLY products_laser ALTER COLUMN id SET DEFAULT nextval('products_laser_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: printa
--

ALTER TABLE ONLY products_normal ALTER COLUMN id SET DEFAULT nextval('products_normal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: printa
--

ALTER TABLE ONLY products_vinyl ALTER COLUMN id SET DEFAULT nextval('products_vinyl_id_seq'::regclass);


--
-- Name: products_laser_pkey; Type: CONSTRAINT; Schema: public; Owner: printa
--

ALTER TABLE ONLY products_laser
    ADD CONSTRAINT products_laser_pkey PRIMARY KEY (id);


--
-- Name: products_normal_pkey; Type: CONSTRAINT; Schema: public; Owner: printa
--

ALTER TABLE ONLY products_normal
    ADD CONSTRAINT products_normal_pkey PRIMARY KEY (id);


--
-- Name: products_vinyl_pkey; Type: CONSTRAINT; Schema: public; Owner: printa
--

ALTER TABLE ONLY products_vinyl
    ADD CONSTRAINT products_vinyl_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: jsrolon
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM printa;
GRANT ALL ON SCHEMA public TO printa;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

