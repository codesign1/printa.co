import {provideRouter, RouterConfig}  from '@angular/router';
import {LoginComponent} from "./components/login.component";
import {AdminGuard} from './services/guards/admin.guard';
import {DashboardComponent} from "./components/dashboard.component";
import {AuthService} from "./services/auth.service";
import {UploadComponent} from "./components/upload.component";
import {DataOrderComponent} from "./components/data-order.component";
import {QuoteComponent} from "./components/quote.component";
import {UserGuard} from "./services/guards/user.guard";
import {DataService} from "./services/data.service";
import {SocketService} from "./services/socket.service";
import {HomeComponent} from "./components/home.component";
import {ProximamenteComponent} from "./components/proximamente.component";
import {FlowControlService} from "./services/flow-control.service";
import {SettingsComponent} from "./components/settings.component";
import {AdvisoryComponent} from "./components/advisory.component";
import {PaymentComponent} from "./components/payment.component";
import {StatusComponent} from "./components/status.component";
import {provideForms, disableDeprecatedForms} from "@angular/forms";
import {PaymentService} from "./services/payment.service";
import {OrdersComponent} from "./components/orders.component";
import {PrintOptionsService} from "./services/print-options.service";
import {FAQComponent} from "./components/faq.component";
import {TerminosComponent} from "./components/terminos.component";
import {ColorService} from "./services/color.service";
import {ProductsComponent} from "./components/products.component";
import {PromoCodesComponent} from "./components/promo-codes.component";
import { PlacesDeliveryComponent } from "./components/places-delivery.component";
import { VideosComponent } from "./components/videos.component";
import {CorporateComponent} from "./components/corporate.component";

const routes: RouterConfig = [
    // app routes
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'proximamente', component: ProximamenteComponent },
    { path: 'estado/:orderId', component: StatusComponent },
    { path: 'estado', redirectTo: 'mis-pedidos' },
    { path: 'faq', component: FAQComponent },
    { path: 'terminos', component: TerminosComponent },
    { path: 'corporativo', component: CorporateComponent },

    // user routes
    { path: 'subir', component: UploadComponent, canActivate: [UserGuard] },
    { path: 'datos-orden', component: DataOrderComponent, canActivate: [UserGuard] },
    { path: 'cotiza', component: QuoteComponent, canActivate: [UserGuard] },
    { path: 'paga', component: PaymentComponent, canActivate: [UserGuard] },
    { path: 'mis-pedidos', component: OrdersComponent, canActivate: [UserGuard] },

    // admin routes
    { path: 'dashboard', component: DashboardComponent, canActivate: [AdminGuard] },
    { path: 'products', component: ProductsComponent, canActivate: [AdminGuard] },
    { path: 'promo-codes', component: PromoCodesComponent, canActivate: [AdminGuard] },
    { path: 'places-delivery', component: PlacesDeliveryComponent, canActivate: [AdminGuard] },
    { path: 'videos', component: VideosComponent, canActivate: [AdminGuard] },

    // global redirect
    { path: '', redirectTo: '/home', pathMatch: 'full' },

    // Error
    {path: '**', redirectTo: 'home'}
];

export const appRouterProviders = [
    provideRouter(routes),
    disableDeprecatedForms(),
    provideForms(),
    AuthService,
    AdminGuard,
    UserGuard,
    DataService,
    SocketService,
    PaymentService,
    FlowControlService,
    PrintOptionsService,
    ColorService
];
