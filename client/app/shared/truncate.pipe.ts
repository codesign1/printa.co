/**
 * Created by jsrolon on 19-08-2016.
 */

import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: 'truncate'
})

export class TruncatePipe implements PipeTransform {
    transform(value: string, length: number, separator: string): string {
        let limit = length > 0 ? length : 10;

        if (value.length <= limit){
            return value;
        }

        var separator = separator || '...';

        var sepLen = separator.length,
            charsToShow = limit - sepLen,
            frontChars = Math.ceil(charsToShow/2),
            backChars = Math.floor(charsToShow/2);

        return value.substr(0, frontChars) +
            separator +
            value.substr(value.length - backChars);
    }
}
