/**
 * Created on 06-01-2017.
 */

import {Pipe, PipeTransform} from "@angular/core";
import {Order} from "../classes/order";

@Pipe({
    name: 'groupBy'
})

export class FilterGroupByProductsPipe implements PipeTransform {
    transform(value: any[], filter: string) : any[] {
        var items = [];
        var i = 1;
        value.forEach(function(index, val) {
            if(typeof index[filter] != 'undefined'){
                if(i == 1){
                    items.push(index[filter]);
                }else{
                    var o = 1;
                    if(items.indexOf(index[filter]) == -1){
                        items.forEach(function(item){
                            if(index[filter] == item){
                                o++;
                            }else if(o <= 1){
                                items.push(index[filter]);
                                o++;
                            }

                        });
                    }
                }
            }
            i++;
        });

        return items;
    }
}
