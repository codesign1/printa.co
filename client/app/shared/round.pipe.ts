/**
 * Created by jsrolon on 15-09-2016.
 */

import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: 'round'
})

export class RoundPipe implements PipeTransform {
    transform(value: number, numDecimals: number): string {
        return value.toFixed(numDecimals);
    }
}
