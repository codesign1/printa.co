/**
 * Created by jsrolon on 24-08-2016.
 */

import {Pipe, PipeTransform} from "@angular/core";
import {Order} from "../classes/order";

@Pipe({
    name: 'filterOrders'
})

export class FilterOrdersPipe implements PipeTransform {
    transform(value: Order[], filter: string) : Order[] {
        if(filter === 'none') {
            return value;
        } else {
            return value.filter(elem => {
                return elem.status === filter;
            });
        }
    }
}
