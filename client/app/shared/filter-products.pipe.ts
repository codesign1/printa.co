/**
 * Created by jsrolon on 30-11-2016.
 */

import {Pipe, PipeTransform} from "@angular/core";
import {Order} from "../classes/order";

@Pipe({
    name: 'filterProducts'
})

export class FilterProductsPipe implements PipeTransform {
    transform(value: any[], filter: string) : any[] {
        if(filter === 'none') {
            return value;
        } else {
            return value.filter(elem => {
                return elem.tipo === filter;
            });
        }
    }
}
