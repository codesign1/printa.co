/**
 * Created by jsrolon on 05-10-2016.
 */

import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: 'initials'
})

export class InitialsPipe implements PipeTransform {
    transform(value: string): string {
        if(value.indexOf('@') !== -1) {
            let userAtDomain = value.split('@');
            let domainAtTLD = userAtDomain[1].split('.');
            return userAtDomain[0].charAt(0) + '@' + domainAtTLD[0].charAt(0) + '.' + domainAtTLD[1].charAt(0);
        } else {
            let matches = value.match(/[^\s]+/ug);

            let initials = matches.map(curr => {
                return curr.charAt(0);
            });

            return initials.reduce((prev, curr) => {
                return prev + curr;
            }, '');
        }
    }
}
