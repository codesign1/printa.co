/**
 * Created by jsrolon on 09-09-2016.
 */

import {Component, AfterViewInit, OnInit}       from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import '../rxjs-extensions';
import {AuthService} from "../services/auth.service";
import {LoginComponent} from './login.component'
import {AdminGuard} from "../services/guards/admin.guard";
import {DashboardComponent} from "./dashboard.component";
import {QuoteComponent} from "./quote.component";
import {HomeComponent} from "./home.component";
import {StepsMenuComponent} from "./steps-menu.component";
import {SocketService} from "../services/socket.service";
import {Order} from "../classes/order";
import { VideosFrontComponent } from "./videos-front.component";

@Component({
    moduleId: module.id,
    selector: 'printa',
    templateUrl: '../templates/orders.template.html',
    directives: [ROUTER_DIRECTIVES, LoginComponent, DashboardComponent, HomeComponent, VideosFrontComponent]
})

export class OrdersComponent implements OnInit {
    orders: Order[];

    constructor(private auth: AuthService, private router: Router, private socketService: SocketService) {
    }

    ngOnInit() {
        // register the listener
        this.socketService.socket.on('user-orders', function (msg) {
            console.log('receive user orders');
            console.log(msg);
            this.orders = msg;
        }.bind(this));


        this.socketService.socket.on('order-deleted', function(orderId) {
            console.log(orderId + ' order deleted');
            let index = -1;
            for(let order of this.orders) {
                index++;
                if(order.id === orderId) {
                    this.orders.splice(index, 1);
                }
            }
        }.bind(this));

        this.socketService.socket.on('finished-quote', function (order) {
            for (let i = 0; i < this.orders.length; i++) {
                if (this.orders[i].id === order.id) {
                    this.orders[i] = order;
                    break;
                }
            }
        }.bind(this));

        // call the orders
        this.socketService.socket.emit('get-user-orders', this.auth.userProfile.email);
    }

    navigateToOrderStatus(order) {
        if(order.flowStatus !== 'estado') {
            // make the order available to whatever component will be needing it
            localStorage.setItem('order', JSON.stringify(order));
            this.router.navigate([order.flowStatus]);
        } else {
            let route = 'estado/' + order.id;
            console.log(route);
            this.router.navigate([route]);
        }
    }

    requestOrderDeletion(order) {
        if(this.canDeleteOrder(order)) {
            if(confirm('Estás seguro(a)?')) {
                this.socketService.socket.emit('delete-order', order.id);
            }
        }
    }

    canDeleteOrder(order) {
        return !order.paid && (order.paid || order.paymentType !== 'cash');
    }
}
