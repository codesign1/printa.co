/**
 * Created by jsrolon on 29-11-2016.
 */

import {Component, OnInit, AfterViewChecked}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";
import {NewProductComponent} from "./new-product.component";
import {EditProductComponent} from "./edit-product.component";
import {FilterProductsPipe} from "../shared/filter-products.pipe";
import {AuthService} from "../services/auth.service";

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'printa',
    templateUrl: '../templates/products.template.html',
    directives: [ROUTER_DIRECTIVES, NewProductComponent, EditProductComponent],
    pipes: [FilterProductsPipe]
})

export class ProductsComponent implements OnInit, AfterViewChecked {
    selectedFilter: string;

    openNewProduct: boolean;
    openEditProduct: boolean;

    products: any;
    allProducts: any;

    uniques: any;

    selectedUnique : any;

    totalUniques : any;
    totalProducts : any;
    totalProductsPrice : number;
    dateToday : Date;

    isGerencia : boolean;

    constructor(private socketService: SocketService, private auth: AuthService) {
        this.selectedFilter = 'none';
        this.openNewProduct = false;
        this.openEditProduct = false;
        this.totalProductsPrice = 0;

        this.isGerencia = (this.auth.userProfile.email === 'gerencia@printadelivery.com' || this.auth.userProfile.email === 'operaciones@printadelivery.com');
    }

    ngOnInit() {
        /*
         * create all the event listeners first
         */
        this.socketService.socket.on('products', function (products) {
            this.allProducts = products;
            this.uniques = [];

            for(let product of this.allProducts) {
                let unique = this.uniques.find(function(uniq) {
                    if(product.tipo == 'vinilo'){
                        return uniq.material === product.material && uniq.color === product.color && uniq.acabado === product.acabado;
                    }else if(product.tipo == 'laser'){
                        return uniq.material === product.material && uniq.color === product.color;
                    }else{
                        return uniq.material === product.material;
                    }
                });

                product.valor_unitario = parseInt(product.valor_unitario);

                if(!unique) {
                    unique = {
                        material: product.material,
                        tipo: product.tipo, // TODO this should depend on the product, but how?
                        color : product.color,
                        acabado: product.acabado,
                        products: [],
                        showSubs: false
                    };
                    this.uniques.push(unique);
                }
                this.totalProductsPrice += product.valor_unitario * product.cantidad;
                unique.products.push(product);
            }
            if(this.allProducts.length == 0){
                this.totalProductsPrice = 0;
            }
            this.totalUniques = this.uniques.length;
            this.totalProducts = this.allProducts.length;

            var datemy = new Date();
            var DateUTC = datemy.getTime() - new Date().getTimezoneOffset();
            var DateNow = new Date(DateUTC);
            var dateString = String(DateNow);
            this.dateToday = dateString.substr(4,11);

        }.bind(this));

        this.socketService.socket.emit('get-products');

    }

    toggleFilter(filter) {
        this.selectedFilter = filter;
    }

    createProduct() {
        this.openNewProduct = true;
    }

    closeNewProduct(event) {
        this.openNewProduct = false;
    }

    editProduct(unique:unique) {
        this.openEditProduct = true;
        this.selectedUnique = unique;
    }

    closeEditProduct(event) {
        this.openEditProduct = false;
    }

    ngAfterViewChecked() {
        let accs = $('.accordion');
        accs.accordion({
            singleOpen: false
        });
    }

    showProductsFromUnique(unique) {
        unique.showSubs = !unique.showSubs;
    }
}
