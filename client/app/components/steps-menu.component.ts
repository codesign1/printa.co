/**
 * Created by jsrolon on 16-08-2016.
 */

import {Component} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {Order} from "../classes/order";
import {Document} from "../classes/document";
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {QuoteComponent} from "./quote.component";
import {DataService} from "../services/data.service";
import {FlowControlService} from "../services/flow-control.service";
import {ColorService} from "../services/color.service";

@Component({
    moduleId: module.id,
    selector: 'steps-menu',
    templateUrl: '../templates/steps-menu.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class StepsMenuComponent {
    allDocumentsWithSettings : boolean;

    constructor(private router : Router, private dataService : DataService,
    private flowControlService : FlowControlService, private colorService : ColorService) {}

    isStatus() {
        return this.router.url.match(/\/estado\/.+/);
    }
}
