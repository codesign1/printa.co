/**
 * Created on 08-01-2017.
 */

import {Component, OnInit, AfterViewChecked}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";
import {FilterProductsPipe} from "../shared/filter-products.pipe";
import {NewPlaceDeliveryComponent} from "./new-place-delivery.component";
import {EditPlaceDeliveryComponent} from "./edit-place-delivery.component";
import {AuthService} from "../services/auth.service";

@Component({
    moduleId: module.id,
    selector: 'printa',
    templateUrl: '../templates/places-delivery.template.html',
    directives: [ROUTER_DIRECTIVES, NewPlaceDeliveryComponent, EditPlaceDeliveryComponent],
    pipes: [FilterProductsPipe]
})

export class PlacesDeliveryComponent implements OnInit, AfterViewChecked {

    openNewPlace: boolean;
    openEditPlace: boolean;

    allPlaces: any;

    uniques: any;

    selectedPlace : any;

    totalPlaces : any;
    totalSchedules : any;
    dateToday : Date;

    isGerencia : boolean;

    constructor(private socketService: SocketService, private auth: AuthService) {
        this.openNewPlace = false;
        this.openEditPlace = false;

        this.isGerencia = (this.auth.userProfile.email === 'gerencia@printadelivery.com' || this.auth.userProfile.email === 'operaciones@printadelivery.com');
    }

    ngOnInit() {
        /*
         * create all the event listeners first
         */
        this.socketService.socket.on('place-delivery', function (places) {
            this.allPlaces = places;
            this.uniques = [];

            for (var place of this.allPlaces){
                let unique = this.uniques.find(function(uniq) {
                    return uniq.lugar === place.lugar && uniq.referencia === place.referencia;
                });

                place.valor_envio = parseInt(place.valor_envio);

                if(!unique) {
                    unique = {
                        lugar: place.lugar,
                        referencia: place.referencia,
                        horarios: [],
                        showSubs: false
                    };
                    this.uniques.push(unique);
                }

                unique.horarios.push(place);
            }

            this.totalPlaces = this.uniques.length;
            this.totalSchedules = this.allPlaces.length;
            var today = new Date();
            this.dateToday = this.stringDate(today);

        }.bind(this));

        this.socketService.socket.emit('get-places');

    }

    stringDate(dateToString){

        var datemy = new Date(dateToString);
        var DateUTC = datemy.getTime() - new Date().getTimezoneOffset();
        var DateNow = new Date(DateUTC);
        var dateString = String(DateNow);
        var fecha_string = dateString.substr(4,11);

        return fecha_string
    }

    createPlace() {
        this.openNewPlace = true;
    }

    closeNewPlace(event) {
        this.openNewPlace = false;
    }

    editPlace(unique) {
        this.openEditPlace = true;
        this.selectedPlace = unique;
    }

    closeEditPlace(event) {
        this.openEditPlace = false;
    }

    ngAfterViewChecked() {
        let accs = $('.accordion');
        accs.accordion({
            singleOpen: false
        });
    }

    showPlacesFromUnique(unique) {
        unique.showSubs = !unique.showSubs;
    }

}
