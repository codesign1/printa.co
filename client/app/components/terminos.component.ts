/**
 * Created by jsrolon on 11-10-2016.
 */

import {Component, Input, Output, EventEmitter}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {Order} from "../classes/order";

@Component({
    moduleId: module.id,
    selector: 'faq',
    templateUrl: '../templates/terminos.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class TerminosComponent {

}
