/**
 * Created on 15-06-2017.
 */

import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { ROUTER_DIRECTIVES, Router } from "@angular/router";
import { StepsMenuComponent } from "./steps-menu.component";
import { FlowControlService } from "../services/flow-control.service";
import { Video } from "../classes/video";
import { DataService } from "../services/data.service";
import { TruncatePipe } from "../shared/truncate.pipe";
import { SocketService } from "../services/socket.service";
import { LeftPanelComponent } from "./left-panel.component";
import { NewVideoComponent } from "./new-video.component";
import { EditVideoComponent } from "./edit-video.component";

declare var CryptoJS: any;
declare var NProgress: any;

@Component({
    moduleId: module.id,
    selector: 'videos',
    templateUrl: '../templates/videos.template.html',
    directives: [ROUTER_DIRECTIVES, StepsMenuComponent, LeftPanelComponent, NewVideoComponent, EditVideoComponent],
    pipes: [TruncatePipe]
})

export class VideosComponent implements OnInit {

    openNewVideo: boolean;
    openEditVideo: boolean;

    origin: string;
    videos: Video[];
    videoToChange: any;
    selectedVideo : any;

    constructor(private auth: AuthService, private router: Router, private flowControlService: FlowControlService,
        private dataService: DataService, private socketService: SocketService) {
        // use the polyfill to make sure we're getting the correct value (window.location.origin didnt seem to work)
        this.origin = window.location.protocol + "//" + window.location.hostname
            + (window.location.port ? ':' + window.location.port : '');

        this.openNewVideo = false;
        this.videoToChange = [];
        this.openEditVideo = false;
    }

    ngOnInit() {
        this.socketService.socket.on('get-all-videos', function (msg) {
            this.videos = msg;
            for (var video of this.videos){
                video.thumbnail = "https://img.youtube.com/vi/"+ video.videocode +"/1.jpg";
            };
        }.bind(this));

        // call the orders
        this.socketService.socket.emit('get-videos', this.auth.userProfile.email);
    }

    createVideo() {
        this.openNewVideo = true;
    }

    closeVideo() {
        this.openNewVideo = false;
    }

    changeStatus(id, show){
        console.log(id);
        this.videoToChange = {
            id: id,
            show: !show
        }
        this.socketService.socket.emit('change-video', {
            videoInfo: this.videoToChange
        });
    }

    editVideo(video){
        this.openEditVideo = true;
        this.selectedVideo = video;
    }

    closeEditVideo(event) {
        this.openEditVideo = false;
    }

}
