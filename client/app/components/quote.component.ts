/**
 * Created by jsrolon on 09-08-2016.
 */

import {Component, OnInit, OnDestroy} from "@angular/core";

import {SocketService} from "../services/socket.service";
import {DataService} from "../services/data.service";
import {Order} from "../classes/order";
import {ROUTER_DIRECTIVES, Router} from "@angular/router";
import {StepsMenuComponent} from "./steps-menu.component";
import {FlowControlService} from "../services/flow-control.service";
import {TruncatePipe} from "../shared/truncate.pipe";
import {AuthService} from "../services/auth.service";
import {LeftPanelComponent} from "./left-panel.component";
import {ColorService} from "../services/color.service";
import { VideosFrontComponent } from "./videos-front.component";

declare var NProgress: any;

@Component({
    moduleId: module.id,
    selector: 'quote',
    templateUrl: '../templates/quote.template.html',
    directives: [ROUTER_DIRECTIVES, StepsMenuComponent, LeftPanelComponent, VideosFrontComponent],
    pipes: [TruncatePipe]
})

export class QuoteComponent implements OnInit, OnDestroy {

    order: Order;

    quoteTime: number;

    confirmationIntervalID: any;

    printTypes : any;

    laserExist : boolean;

    constructor(private socketService: SocketService, private dataService: DataService,
                private flowControlService: FlowControlService, private router: Router,
                private auth: AuthService, private colorService : ColorService) {
        localStorage.removeItem('saveProgress');
        this.printTypes = [];
        this.laserExist = false;
    }

    ngOnInit() {
        NProgress.done();
        // take the order that was just stored in the previous component
        this.order = this.dataService.getOrder();
        if(!this.order) {
            this.router.navigate(['/mis-pedidos']);
        } else {
            if (this.order.flowStatus === 'cotiza' || !this.order.flowStatus) {

                // add the flow status to the order
                // this status will be saved to the db
                this.order.flowStatus = 'cotiza';

                // send this to the socket
                this.socketService.socket.emit('new-order', {
                    userProfile: this.auth.userProfile,
                    order: this.order
                });

                //verify type of print : laser
                for (var i = 0; i < this.order.documents.length; i++) {
                    this.printTypes[i] = this.order.documents[i].printSettings.type;
                }

                if(this.printTypes.indexOf('laser') == -1){
                    this.laserExist = false;
                }else{
                    this.laserExist = true;
                }

                console.log(this.laserExist);

                // set up the interval for confirming socket every 30 seconds
                this.confirmationIntervalID = setInterval(function () {
                    this.socketService.socket.emit('socket-confirmation',
                        this.auth.userProfile.email);
                }.bind(this), 30000);

                // bind the event listeners
                this.socketService.socket.on('quote-time-change', function (quoteTime) {
                    this.quoteTime = quoteTime;
                }.bind(this));

                // register the event listener
                this.socketService.socket.on('finished-quote', function (msg) {
                    this.order = msg;
                    this.dataService.saveOrder(msg);
                }.bind(this));
            } else {
                this.router.navigate(['/subir']);
            }
        }
    }

    acceptQuote() {
        console.log(this.order);
        var laser = false;
        this.order.documents.forEach(function (doc) {
            if(doc.printSettings.type == 'laser'){
                laser = true;
            }
        });

        if (this.order.totalPrice > 0 && this.order.shippingCost > 0) {
            NProgress.start();

            if(laser){
                var conf = confirm("Recuerda que la cotizacion solo incluye el costo de los materiales y el envio, el precio del corte aumente en 800 pesos por minuto.");
                if(conf){
                    this.order.quoteAccepted = true;

                    this.dataService.saveOrder(this.order);

                    this.socketService.socket.emit('quote-accepted', this.order);
                    this.flowControlService.hasFinished(2);

                    clearInterval(this.confirmationIntervalID);

                    localStorage.setItem('saveProgress', 'true');

                    this.router.navigate(['/paga']);
                }
            }else{
                this.order.quoteAccepted = true;

                this.dataService.saveOrder(this.order);

                this.socketService.socket.emit('quote-accepted', this.order);
                this.flowControlService.hasFinished(2);

                clearInterval(this.confirmationIntervalID);

                localStorage.setItem('saveProgress', 'true');

                this.router.navigate(['/paga']);
            }
        }
    }

    ngOnDestroy() {
        this.dataService.cleanEverythingUp(true);
    }
}
