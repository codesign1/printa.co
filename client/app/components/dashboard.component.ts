/**
 * Created by jsrolon on 09-08-2016.
 */

import {Component, OnInit, NgZone, AfterViewInit} from "@angular/core";
import {SocketService} from "../services/socket.service";
import {Order} from "../classes/order";
import {Document} from "../classes/document";
import {ROUTER_DIRECTIVES} from "@angular/router";
import {DashboardUserDetailsComponent} from "./dashboard-user-details.component";
import {DashboardDocumentsDetailComponent} from "./dashboard-documents-detail";
import {UpdateStockComponent} from "./update-stock.component";
import {AuthService} from "../services/auth.service";
import {FilterOrdersPipe} from "../shared/filter-orders.pipe";
import {RoundPipe} from "../shared/round.pipe";

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: '../templates/dashboard.template.html',
    directives: [ROUTER_DIRECTIVES, DashboardUserDetailsComponent, DashboardDocumentsDetailComponent, UpdateStockComponent],
    pipes: [FilterOrdersPipe]
})

export class DashboardComponent implements OnInit {
    RELAJA_LA_PELVIS_PRICE = 17241.3793;

    assignedOrders: Order[];

    selectedOwner: any;
    selectedDocuments: Document[];
    selectedDocumentsToStock: Document[];
    selectedDocumentsSize: number;

    selectedOrder: Order;

    selectedFilter: string;

    audio: any;

    isGerencia : boolean;

    despachado : boolean;

    months : any;

    monthToday : string;

    metrics : any;

    constructor(private socketService: SocketService, private ngZone: NgZone,
                private auth: AuthService) {
        this.assignedOrders = [];
        this.selectedFilter = 'none';

        this.audio = new Audio('doorbell.wav');

        this.isGerencia = (this.auth.userProfile.email === 'gerencia@printadelivery.com' || this.auth.userProfile.email === 'operaciones@printadelivery.com');

        this.despachado = false;

        this.months = [];

        this.monthToday = '';

        this.metrics = [];
    }

    ngOnInit() {
        // add all the event listeners
        this.socketService.socket.on('assigned-order', function (message) {
            this.addOrder(message);
            this.audio.play();
        }.bind(this));

        this.socketService.socket.on('assigned-orders', function (msg) {
            
            this.assignedOrders = [];
            for (let item of msg) {
                this.assignedOrders.push(item);
            }
            
            var i = 0;
            if(this.assignedOrders.length > 0){
                
                var combo = [];
                for (var order of this.assignedOrders){
                    
                    var datemy = new Date(order.creation_date);
                    var DateUTC = datemy.getTime() - new Date().getTimezoneOffset();
                    var DateNow = new Date(DateUTC);
                    var dateString = String(DateNow);

                    order.order.creation_time = dateString.substr(15,8);
                    /*order.creation_date = datemy.getDate();*/
                    order.order.creation_month = datemy.getMonth()+1;
                    
                    var today = new Date();
                    this.monthToday =  this.getMonthName(today.getMonth()+1);
                    
                    if(i == 0){
                        combo.push(this.assignedOrders[0].order);
                    }else{
                        if(i == this.assignedOrders.length-1){
                            combo.push(this.assignedOrders[i].order);
                            var totalsell = 0;
                            var totalquote = 0;
                            for(var order of combo){
                                if(order.status != 'cotizar'){
                                    if(typeof(order.paymentTotal) == "undefined"){
                                        order.paymentTotal = 0;
                                    }
                                    totalsell += order.paymentTotal;
                                }

                                if(typeof(order.paymentTotal) == "undefined"){
                                    order.paymentTotal = 0;
                                }
                                totalquote += order.paymentTotal;
                            }
                            
                            var thismes = {'orders': combo, 'name': this.getMonthName(this.assignedOrders[i].order.creation_month+1), 'sell': totalsell, 'quote': totalquote};
                            this.months.push(thismes);
                        }else{
                            if(this.assignedOrders[i-1].order.creation_month == this.assignedOrders[i].order.creation_month){
                                combo.push(this.assignedOrders[i].order);
                            }else{
                                var totalsell = 0;
                                var totalquote = 0;
                                for(var order of combo){
                                    if(order.status != 'cotizar'){
                                        if(typeof(order.paymentTotal) == "undefined"){
                                            order.paymentTotal = 0;
                                        }
                                        totalsell += order.paymentTotal;
                                    }

                                    if(typeof(order.paymentTotal) == "undefined"){
                                        order.paymentTotal = 0;
                                    }
                                    totalquote += order.paymentTotal;
                                }
                                var thismes = {'orders': combo, 'name': this.getMonthName(this.assignedOrders[i].order.creation_month+1), 'sell': totalsell, 'quote': totalquote};
                                this.months.push(thismes);
                                combo = [];
                                combo.push(this.assignedOrders[i].order);
                            }
                        }
                    }

                    i++;
                }
                this.metrics = this.months[0];
                console.log(this.months);
            }
        }.bind(this));

        this.socketService.socket.on('order-deleted', function (orderId) {

            let index = -1;
            for (let order of this.assignedOrders) {
                index++;
                if (order.id === orderId) {
                    this.assignedOrders.splice(index, 1);
                }
            }
            this.months.forEach(function (month) {
                month.orders.forEach(function (order, index) {
                    if(order.id === orderId){
                        month.orders.splice(index, 1);
                    }
                })
            });

            if (this.selectedOrder && this.selectedOrder.id === orderId) {
                this.selectedOrder = null;
                this.selectedDocuments = null;
            }
        }.bind(this));

        // the quote was accepted
        this.socketService.socket.on('quote-accepted', function (msg) {
            for (let i = 0; i < this.assignedOrders.length; i++) {
                if (this.assignedOrders[i].id === msg.id) {
                    this.assignedOrders[i] = msg;
                    break;
                }
            }
            this.months.forEach(function(month){
                month.orders.forEach(function(order){
                    if(order.id === msg.id){
                        order = msg;
                    }
                })
            })
        }.bind(this));

        // we received a status update on the order
        this.socketService.socket.on('dashboard-update-status', function (msg) {
            for (let i = 0; i < this.assignedOrders.length; i++) {
                if (this.assignedOrders[i].id === msg.id) {
                    this.assignedOrders[i] = msg;
                    break;
                }
            }
            for (let m = 0; m < this.months.length; m++ ){
                for (let o = 0; o < this.months[m].orders.length; o++ ){
                    if(this.months[m].orders[o].id === msg.id){
                        this.months[m].orders[o] = msg;
                    }
                }
            }

        }.bind(this));

        // the quote was accepted
        this.socketService.socket.on('payment-done', function (msg) {
            for (let i = 0; i < this.assignedOrders.length; i++) {
                if (this.assignedOrders[i].id === msg.id) {
                    this.assignedOrders[i] = msg;

                    this.assignedOrders[i].status = 'imprimiendo';
                    this.socketService.socket.emit('order-status-change', this.assignedOrders[i]);

                    break;
                }
            }

            for (let m = 0; m < this.months.length; m++ ){
                for (let o = 0; o < this.months[m].orders.length; o++ ){
                    if(this.months[m].orders[o].id === msg.id){
                        this.months[m].orders[o] = msg;
                        
                        this.months[m].orders[o].status = 'imprimiendo';
                    }
                }
            }

        }.bind(this));

        // after all event listeners were added, we ask for the assigned orders
        this.socketService.socket.emit('get-assigned-orders', this.auth.userProfile.email);
    }

    getMonthName(name){
        var months = [];
        months[1] = "Enero";
        months[2] = "Febrero";
        months[3] = "Marzo";
        months[4] = "Abril";
        months[5] = "Mayo";
        months[6] = "Junio";
        months[7] = "Julio";
        months[8] = "Agosto";
        months[9] = "Septiembre";
        months[10] = "Octubre";
        months[11] = "Noviembre";
        months[12] = "Diciembre";

        return months[name];
    }

    onOwnerClicked(order: Order) {
        this.selectedOwner = order.owner;
        this.selectedOrder = order;
    }

    onDocumentsClicked(order: Order, documents: Document[]) {

        this.selectedDocuments = documents;

        // reduce and divide by a million to get size in MB
        this.selectedDocumentsSize = this.selectedDocuments.reduce(function (prev, curr) {
            return prev + curr.fileSize;
        }, 0);
        this.selectedDocumentsSize /= 1000000;

        this.selectedOrder = order;
        // show the documents panel
    }

    addOrder(order) {

        // check if the array already contains an order
        let contains = this.assignedOrders.reduce(function (prev, curr) {
            return prev || curr.id === order.id;
        }, false);

        // make sure the order is added only once
        if (!contains) {
            this.ngZone.run(() => {
                this.months[0].orders.unshift(order);
                this.assignedOrders.unshift(order);
            });
        }
    }

    saveOrderStatus(event) {
        this.recalculatePrice();
        this.socketService.socket.emit('save-order-status', this.selectedOrder);
    }

    /**
     * Here is where price calculations occur
     */
    recalculatePrice() {
        // calculate the value of the printing process
        let totalValue = this.selectedOrder.documents.reduce(function (prev, curr) {
            return prev + curr.price;
        }, 0);

        // tax is applied after all operations
        this.selectedOrder.totalPrice = totalValue + this.selectedOrder.shippingCost;

        /*
         * this code here updates the paymentTotal, which takes into account applied discounts
         */
        if (this.selectedOrder.promoCode) {
            if (this.selectedOrder.discountType) {
                // descuento fijo $
                this.selectedOrder.paymentTotal = (this.selectedOrder.totalPrice + this.selectedOrder.redbullprice) - this.selectedOrder.discountAmount;
            } else {
                // descuento por porcentaje %
                let totalneto = this.selectedOrder.totalPrice + this.selectedOrder.redbullprice;
                this.selectedOrder.paymentTotal = totalneto * this.selectedOrder.discountAmount/100;
                this.selectedOrder.paymentTotal =  totalneto - this.selectedOrder.paymentTotal;
            }

            // make sure the value is never negative
            this.selectedOrder.paymentTotal = this.selectedOrder.paymentTotal < 0 ? 0 : this.selectedOrder.paymentTotal;
            console.log(this.selectedOrder.paymentTotal);
        } else {
            this.selectedOrder.paymentTotal = this.selectedOrder.totalPrice + this.selectedOrder.redbullprice;
        }

        // add the relajalapelvis prices
        if (this.selectedOrder.specialPromotions.numRelajaLaPelvis > 0) {
            this.selectedOrder.specialPromotions.totalRelajaLaPelvisPrice = (this.selectedOrder.specialPromotions.numRelajaLaPelvis
            * this.RELAJA_LA_PELVIS_PRICE);
            this.selectedOrder.paymentTotal += this.selectedOrder.specialPromotions.totalRelajaLaPelvisPrice;

        }

        // apply the tax and make sure the price is an integer
        this.selectedOrder.paymentTotal = Math.ceil(this.selectedOrder.paymentTotal * (1 + this.selectedOrder.taxRate));
    }

    allDocumentsWithPrice(): boolean {
        return this.selectedOrder.documents.reduce(function (prev, curr) {
            return prev && (curr.price > 0);
        }, true);
    }

    alertUserOfFinishedQuote(order) {
        this.socketService.socket.emit('finished-quote', order);
    }

    onOrderStatusChange(order, documents: Document[]) {
        
        this.socketService.socket.emit('order-status-change', order);

        if(order.status == 'entregado'){
            this.selectedDocumentsToStock = documents;
            this.despachado = true;
        }
    }

    toggleFilter(filter) {
        this.selectedFilter = filter;
    }

    updateQuoteTime(quoteTime) {
        // TODO: tell the user that the time has changed i.e. emit
        this.socketService.socket.emit('quote-time-change', {
            orderId: this.selectedOrder.id,
            quoteTime: quoteTime
        });
    }

    saveAndSendQuote(order) {
        this.selectedOrder = order;

        // make sure everything's ready to send the quote:
        // i.e. there is a shipping price and all the documents have a price
        if (this.selectedOrder.shippingCost > 0 && this.allDocumentsWithPrice() &&
            (this.selectedOrder.deliveryTime || this.selectedOrder.deliveryTime !== '')) {
            this.selectedOrder.quoteSent = true;
            this.alertUserOfFinishedQuote(this.selectedOrder);
        } else {
            alert('Hace falta completar la información de la cotización. Puede ser el costo de envío, los precios de los archivos,'
                + ' o el tiempo estimado de cotización.');
        }
    }

    requestOrderDeletion(orderId) {
        if (confirm('Estás seguro(a)?')) {
            this.socketService.socket.emit('delete-order', orderId);
        }
    }

    closeUpdateStock(){
        this.despachado = false;
    }

}
