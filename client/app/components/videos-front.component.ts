/**
 * Created by jsrolon on 21-09-2016.
 */

import { Component, Input, OnInit } from "@angular/core";
import { TruncatePipe } from "../shared/truncate.pipe";
import { ColorService } from "../services/color.service";
import { DataService } from "../services/data.service";
import { SocketService } from "../services/socket.service";
import { BROWSER_SANITIZATION_PROVIDERS, DomSanitizationService, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
    moduleId: module.id,
    selector: 'videos-cont',
    templateUrl: '../templates/videos-front.template.html',
    pipes: [TruncatePipe]
})

export class VideosFrontComponent implements OnInit{

    videos: Video[];
    videocode: SafeUrl;
    url: string;

    constructor(private dataService: DataService, private socketService: SocketService, private sanitizer: DomSanitizationService, private colorService : ColorService) {
        this.socketService.socket.on('get-all-videos-front', function (msg) {
            this.videos = msg;
            this.changeVideo(0);
        }.bind(this));

        this.socketService.socket.emit('get-videos-front');
    }

    changeVideo(id) {
        this.url = this.videos[id].videocode;
        this.videocode = this.sanitizer.bypassSecurityTrustHtml('<iframe width="560" height="315" src="https://www.youtube.com/embed/' + this.url + '" frameborder="0" allowfullscreen></iframe>');
    }
}
