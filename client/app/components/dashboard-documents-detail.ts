/**
 * Created by jsrolon on 18-08-2016.
 */

import {Component, Input, OnInit, OnChanges, Output, EventEmitter}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {Document} from "../classes/document";
import {TruncatePipe} from "../shared/truncate.pipe";
import {RoundPipe} from "../shared/round.pipe";
import {Order} from "../classes/order";

@Component({
    moduleId: module.id,
    selector: 'dashboard-documents-detail',
    templateUrl: '../templates/dashboard-documents-detail.template.html',
    directives: [ROUTER_DIRECTIVES],
    pipes: [TruncatePipe, RoundPipe]
})

export class DashboardDocumentsDetailComponent {
    quoteTime : number;

    @Input()
    order : Order;

    @Input()
    totalDocumentsSize : number;

    @Input()
    documents : Document[];

    @Output()
    documentPriceChange = new EventEmitter();

    @Output()
    quoteTimeChange = new EventEmitter();

    @Output()
    saveSendQuote = new EventEmitter();

    constructor() {
    }

    quoteTimeChanged() {
        if(typeof this.quoteTime !== 'undefined' && this.quoteTime > 0) {
            this.quoteTimeChange.emit(this.quoteTime);
        }
    }

    saveAndSendQuote() {
        this.saveSendQuote.emit('');
    }

    saveDocumentPrice(price) {
        this.documentPriceChange.emit(price);
    }

    openFile(src, format){
        if(format == 'psd' || format == 'dwg'){
            window.open('/uploads/'+src, '_blank');
        }else{
            window.open('/uploads/'+src);
        }
    }
}
