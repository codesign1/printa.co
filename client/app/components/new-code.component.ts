/**
 * Created on 06-01-2017.
 */

import {Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, AfterContentInit}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component';
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";
import {FORM_DIRECTIVES, FormBuilder, REACTIVE_FORM_DIRECTIVES, FormGroup, Validators} from "@angular/forms";

@Component({
    moduleId: module.id,
    selector: 'new-code',
    templateUrl: '../templates/new-code.template.html',
    directives: [ROUTER_DIRECTIVES, FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES]
})

export class NewCodeComponent implements OnInit, AfterContentInit {

    @Input() opened : boolean;

    @Input() codes : any;

    @Output() closeWindow = new EventEmitter();

    dataNewCodeForm: FormGroup;

    resultVal : any;

    typeDescount: boolean;

    constructor(private socketService : SocketService, private fb: FormBuilder) {

        this.dataNewCodeForm = fb.group({
            codigo: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(12)])],
            cantidad: [''],
            tipo: [true],
            valor: [''],
            fecha_inicio: [''],
            fecha_caducidad: ['']
        });

        this.resultVal = [];

        this.typeDescount = true;
    }

    ngOnInit() {

    }

    ngAfterContentInit() {
    }

    validateForm(form){
        console.log(form['tipo']);
        if(form['cantidad'] == "" || form['codigo'] == "" || form['fecha_caducidad'] == "" || form['fecha_inicio'] == "" || form['valor'] === "" || form['valor'] < 0){
            $('#validate-all').css('display', 'block');
            this.resultVal[0] = false;
        }else{
            $('#validate-all').css('display', 'none');
            this.resultVal[0] = true;
        }

        if(form['fecha_caducidad'] < form['fecha_inicio']){
            $('#validate-fecha').css('display', 'block');
            this.resultVal[1] = false;
        }else{
            $('#validate-fecha').css('display', 'none');
            this.resultVal[1] = true;
        }
        
        if(this.resultVal.indexOf(false) == -1){
            return true;
        }else{
            return false;
        }
    }

    createCode(form) {
        // console.log(form);

        if(this.validateForm(form)){
            this.socketService.socket.emit('add-code', {
                code: form
            });

            form = [];

            this.closeNewCode();
        }

    }

    changeTypeDescount(){
        this.typeDescount = !this.typeDescount;
    }

    closeNewCode() {
        this.closeWindow.emit('close');
    }
}
