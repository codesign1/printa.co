/**
 * Created by jsrolon on 09-08-2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var order_1 = require("../classes/order");
var document_1 = require("../classes/document");
var router_1 = require("@angular/router");
var steps_menu_component_1 = require("./steps-menu.component");
var UploadComponent = (function () {
    function UploadComponent(router, dataService, flowControlService, authService) {
        this.router = router;
        this.dataService = dataService;
        this.flowControlService = flowControlService;
        this.authService = authService;
        this.order = new order_1.Order();
        this.order.owner = authService.userProfile.email;
    }
    UploadComponent.prototype.navigateToQuoteComponent = function () {
        // store the order in the data service
        this.dataService.newOrder = this.order;
        //update the fact we finished this component
        this.flowControlService.hasFinished(0);
        this.flowControlService.hasFinished(1); // TODO: fix the f out of this
        // navigate
        this.router.navigateByUrl('/cotiza');
    };
    // addDocument() {
    //     // TODO: whole logic for storing new documents, this should not be done here
    //     // TODO: it should depend on the user telling the app to do so
    //     if (this.newDocument.name !== '') {
    //         this.order.documents.push(this.newDocument);
    //         this.newDocument = new Document();
    //     }
    // }
    UploadComponent.prototype.fileChangeEvent = function (fileInput) {
        var file = fileInput.target.files[0];
        var doc = new document_1.Document();
        doc.name = file.name;
        doc.file = file;
        this.order.documents.push(doc);
        // generate the file upload request
        this.uploadFile(file);
        // finish
        this.flowControlService.hasFinished(0);
    };
    UploadComponent.prototype.uploadFile = function (file) {
        this.makeFileRequest("http://localhost:3000/upload", [], file).then(function (result) {
            console.log(result);
        }, function (error) {
            console.error(error);
        });
    };
    UploadComponent.prototype.makeFileRequest = function (url, params, file) {
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("file", file, file.name);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    };
    UploadComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'order-settings',
            templateUrl: '../templates/upload.template.html',
            directives: [router_1.ROUTER_DIRECTIVES, steps_menu_component_1.StepsMenuComponent]
        })
    ], UploadComponent);
    return UploadComponent;
}());
exports.UploadComponent = UploadComponent;
