/**
 * Created on 16-02-2017.
 */

import {Component, Input, OnInit, OnChanges, Output, EventEmitter}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {Document} from "../classes/document";
import {TruncatePipe} from "../shared/truncate.pipe";
import {RoundPipe} from "../shared/round.pipe";
import {Order} from "../classes/order";
import {SocketService} from "../services/socket.service";

@Component({
    moduleId: module.id,
    selector: 'resources-used',
    templateUrl: '../templates/update-stock.template.html',
    directives: [ROUTER_DIRECTIVES],
    pipes: [TruncatePipe, RoundPipe]
})

export class UpdateStockComponent {

    @Input()
    despachado : boolean;

    @Input()
    documents : Document[];

    @Output() closeWindow = new EventEmitter();

    form : any;

    constructor(private socketService : SocketService) {
    }

    ngAfterViewInit() {
    }

    saveDocumentPrice(document,form, i){
               
        if(this.documents[i].printSettings.type == 'vinilo'){
            this.socketService.socket.emit('remove-product-vinyl', {
                    product: this.documents[i],
                    sheets: form['sheets']
            });
        }else if(this.documents[i].printSettings.type == 'normal' && this.documents[i].printSettings.size == 'Rollo'){
            this.socketService.socket.emit('remove-product-normal', {
                    product: this.documents[i],
                    sheets: form['sheets']
            });
        }else{
            this.socketService.socket.emit('remove-product', {
                    product: this.documents[i],
                    sheets: form['sheets']
            });
        }

        this.closeUpdateStock();

    }

    closeUpdateStock() {
        this.closeWindow.emit('close');
    }

}
