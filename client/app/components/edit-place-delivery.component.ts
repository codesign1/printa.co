/**
 * Created on 08-02-2017.
 */

import {Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, AfterContentInit}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";

@Component({
    moduleId: module.id,
    selector: 'edit-place',
    templateUrl: '../templates/edit-place-delivery.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class EditPlaceDeliveryComponent implements OnInit, AfterContentInit {

    @Input() openedEdit : boolean;

    @Input() place : [];

    @Input() allPlaces : [];

    @Output() closeWindow = new EventEmitter();

    placesToSave : any;

    onePlace : any;

    idPlaces : any;

    placesToDelete : any;

    newVoid : any;

    form : any;  

    resultVal : any;
    placeToRemove: any;

    constructor(private socketService : SocketService) {
        this.placesToSave = [];
        this.placesToDelete = [];
        this.onePlace = [];
        this.idPlaces = [];

        this.newVoid = [];

        this.resultVal = [];
        this.placeToRemove = [];

    }

    ngOnInit() {
        
    }

    ngAfterContentInit() {
    }

    addPlace(){
        
        this.newVoid = {
            'lugar' : this.place['lugar'],
            'referencia' : this.place['referencia'],
            'hora_inicio' : 0,
            'hora_fin' : 0,
            'valor_envio' : 0,
            'habilitado' : false,
            'id' : 0
        }

        this.place['horarios'].push(this.newVoid);
    }

    validateForm(form){
        for(let i = 0; i < form['count']; i ++){
            
            if(form['lugar'] == "" || form['referencia'] == "" || form['hora_inicio-'+i] == 0 || form['hora_fin-'+i] == 0 || form['valor_envio-'+i] == 0 || form['valor_envio-'+i] == null){
                $('#validate-all').css('display', 'block');
                this.resultVal[i] = false;
            }else{
                $('#validate-all').css('display', 'none');
                this.resultVal[i] = true;
            }

        }

        if(this.resultVal.indexOf(false) == -1){
            return true;
        }else{
            return false;
        }

    }

    editPlace(form) {

        if(this.validateForm(form)){
            for(let i = 0; i < form['count']; i ++){
                this.onePlace = {
                    'lugar': form['lugar'],
                    'referencia' : form['referencia'],
                    'hora_inicio' : form['hora_inicio-'+i],
                    'hora_fin' : form['hora_fin-'+i],
                    'valor_envio' : form['valor_envio-'+i],
                    'habilitado' : form['habilitado-'+i],
                    'id' : form['id-'+i]
                };

                this.placesToSave.push(this.onePlace);
            }  
            
            for(var place of this.placesToSave){
                if(place.id == 0){
                    this.socketService.socket.emit('add-place', {
                        place: place
                    });
                }else{
                    this.socketService.socket.emit('edit-place', {
                        place: place
                    });
                }
            }

            for(var toRemove of this.placeToRemove){
                this.socketService.socket.emit('delete-place', {
                    place: toRemove
                });
            }

            this.placesToSave = [];

            this.closeEditPlace();
        }
        
    }

    deletePlace(form){
        var doDelete = confirm('¿Está seguro de eliminar este lugar? Esta acción no se puede deshacer.');

        if(doDelete){
            for(let i = 0; i < form['count']; i ++){
                this.idPlaces = {
                    'id' : form['id-'+i]
                };
                this.placesToDelete.push(this.idPlaces);
            }

            for(var place of this.placesToDelete){
                this.socketService.socket.emit('delete-place', {
                    place: place,
                });
            }

            this.closeEditPlace();
        }

    }

    removeSchedule(i, schedule){
        var doRemove = confirm('¿Está seguro de eliminar este horario? Esta acción no se puede deshacer.');
        if(doRemove){
            this.place.horarios.splice(i, 1);
            this.placeToRemove.push(schedule);
            event.preventDefault();
        }else{
            event.preventDefault();
        }
    }

    closeEditPlace() {
        this.closeWindow.emit('close');
    }
}
