/**
 * Created by jsrolon on 18-08-2016.
 */

import {Component, Input, Output, EventEmitter}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {Order} from "../classes/order";

@Component({
    moduleId: module.id,
    selector: 'dashboard-user-details',
    templateUrl: '../templates/dashboard-user-details.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class DashboardUserDetailsComponent {
    @Input()
    order : Order;

    @Output()
    shippingCostChange = new EventEmitter();

    onShippingCostChange(event) {
        this.shippingCostChange.emit(this.order.shippingCost);
    }
}
