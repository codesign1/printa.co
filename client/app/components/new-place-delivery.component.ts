/**
 * Created on 08-02-2017.
 */

import {Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, AfterContentInit}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";

@Component({
    moduleId: module.id,
    selector: 'new-place',
    templateUrl: '../templates/new-place-delivery.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class NewPlaceDeliveryComponent implements OnInit, AfterContentInit {

    @Input() opened : boolean;

    @Input() allPlaces : any;

    @Input() uniques : any;

    @Output() closeWindow = new EventEmitter();

    placeToSave : any;

    placeType : string;

    value : any

    newVoid : any;
    inicialVoid : any;

    onePlace : any;

    selectedFilter: string;
    groupByFilter: string;

    resultVal : any;

    constructor(private socketService : SocketService) {
        this.onePlace = [];
        this.placeToSave = [];

        this.inicialVoid = [{
                'lugar' : '',
                'referencia' : '',
                'hora_inicio' : 0,
                'hora_fin' : 0,
                'valor_envio' : 0,
                'habilitado' : false
        }];

        this.resultVal = [];

    }

    ngOnInit() {

    }

    ngAfterContentInit() {
    }

    addPlace(){
        
        this.newVoid = {
            'lugar' : '',
            'referencia' : '',
            'hora_inicio' : 0,
            'hora_fin' : 0,
            'valor_envio' : 0,
            'habilitado' : false
        };

        this.inicialVoid.push(this.newVoid);
    }

    validateForm(form){
        for(let i = 0; i < form['count']; i ++){
            
            //impresion
            if(form['lugar'] == "" || form['referencia'] == "" || form['hora_inicio-'+i] == 0 || form['hora_fin-'+i] == 0 || form['valor_envio-'+i] == 0 || form['valor_envio-'+i] == null){
                $('#validate-all').css('display', 'block');
                this.resultVal[i] = false;
            }else{
                $('#validate-all').css('display', 'none');
                this.resultVal[i] = true;
            }

        }

        if(this.resultVal.indexOf(false) == -1){
            return true;
        }else{
            return false;
        }
    }

    createPlace(form) {

        if(this.validateForm(form)){
            for(let i = 0; i < form['count']; i ++){
                this.onePlace = {
                    'lugar': form['lugar'],
                    'referencia' : form['referencia'],
                    'hora_inicio' : form['hora_inicio-'+i] ? form['hora_inicio-'+i] : 0,
                    'hora_fin' : form['hora_fin-'+i] ? form['hora_fin-'+i] : 0,
                    'valor_envio' : form['valor_envio-'+i] ? form['valor_envio-'+i] : 0,
                    'habilitado' : form['habilitado-'+i]
                };

                this.placeToSave.push(this.onePlace);
            } 

            for(var place of this.placeToSave){
                this.socketService.socket.emit('add-place', {
                    place: place
                });
            }

            this.placeToSave = [];

            this.inicialVoid = [{
                'lugar' : '',
                'referencia' : '',
                'hora_inicio' : 0,
                'hora_fin' : 0,
                'valor_envio' : 0,
                'habilitado' : false
            }];

            this.closeNewPlace();
        }
        
    }

    removeSchedule(i){
        var doRemove = confirm('¿Está seguro de eliminar este horario? Esta acción no se puede deshacer.');
        if(doRemove){
            console.log(i);
            this.inicialVoid.splice(i, 1);
            event.preventDefault();
        }else{
            event.preventDefault();
        }
    }

    materalFilter(filter) {
        this.selectedFilter = filter;
    }

    closeNewPlace() {
        this.closeWindow.emit('close');
    }
}
