/**
 * Created by jsrolon on 21-09-2016.
 */

import {Component, Input} from "@angular/core";
import {Order} from "../classes/order";
import {TruncatePipe} from "../shared/truncate.pipe";
import {ColorService} from "../services/color.service";
import {DataService} from "../services/data.service";
import {SocketService} from "../services/socket.service";

@Component({
    moduleId: module.id,
    selector: 'left-panel',
    templateUrl: '../templates/left-panel.template.html',
    pipes: [TruncatePipe]
})

export class LeftPanelComponent {
    @Input()
    order : Order;

    @Input()
    toPay : boolean;

    redbullCant : number;
    totalRedbull : number;
    tax : number;

    totaldocs : number;

    constructor(private colorService : ColorService, private dataService: DataService, private socketService: SocketService) {
        this.redbullCant = 0;
        this.totalRedbull = 0;
        this.totaldocs = 0;
    }

    changeRedbull(){
        
        if(this.order.totalPrice === 0){
            this.totalRedbull = 5500*this.redbullCant;
            this.order.redbullprice = this.totalRedbull;
            this.order.redbullcant = this.redbullCant;
            this.dataService.saveOrder(this.order);
            
            this.socketService.socket.emit('save-order-status', this.order);
            console.log(this.order);
        }else{
            this.totalRedbull = 5500 * this.order.redbullcant;
            this.order.redbullprice = this.totalRedbull;

            this.order.redbullcant = this.order.redbullcant;
            console.log(this.order.redbullprice);

            for(var doc of this.order.documents){
                this.totaldocs = this.totaldocs + doc.price;
            }

            console.log(this.totaldocs);

            this.order.totalPrice = this.totaldocs + this.order.shippingCost + this.order.redbullprice;
            this.order.paymentTotal = Math.ceil(this.order.totalPrice * (1 + this.order.taxRate));

            this.dataService.saveOrder(this.order);
            
            this.socketService.socket.emit('save-order-status', this.order);

            this.tax = (this.order.totalPrice/(1 + this.order.taxRate)) * this.order.taxRate;
            console.log(this.order);
            this.totaldocs = 0;
        }

        // console.log(this.order.paymentTotal);
        // if(this.order.paymentTotal){
        //     this.totalRedbull = 5500*this.redbullCant;
        //     this.order.redBull = this.totalRedbull;
        //     this.order.paymentTotal = this.order.paymentTotal + (this.totalRedbull + (this.totalRedbull * this.order.taxRate));
        // }else{
            
        // }
    }
}
