/**
 * Created by carolina on 29-12-2017.
 */

import {
    Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit,
    AfterContentInit
} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {Order} from "../classes/order";
import {Document} from "../classes/document";
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {QuoteComponent} from "./quote.component";
import {DataService} from "../services/data.service";
import {StepsMenuComponent} from "./steps-menu.component";
import {FlowControlService} from "../services/flow-control.service";
import {TruncatePipe} from "../shared/truncate.pipe";
import {PrintOptionsService} from "../services/print-options.service";
import {ColorService} from "../services/color.service";
import { SocketService } from "../services/socket.service";

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'advisory',
    templateUrl: '../templates/advisory.template.html',
    directives: [ROUTER_DIRECTIVES, StepsMenuComponent],
    pipes: [TruncatePipe]
})

export class AdvisoryComponent implements OnInit, AfterViewInit, AfterContentInit {

    @Input()
    openAdvisory= true;

    @Output()
    settings = new EventEmitter();

    @ViewChild('advisoryForm') form;

    private order: Order;

    errorForm: boolean = true;

    advisorySettings: any;

    countDocs: number;

    constructor(private router: Router, private flowControlService: FlowControlService, private dataService: DataService,
                private printOptions: PrintOptionsService, private colorService: ColorService, private socketService: SocketService) {
        this.order = new Order();
                
        let orders = this.dataService.getOrder();
        this.countDocs = orders.documents.length;
        
        this.advisorySettings = {
            'asesoria_escala': false,
            'asesoria_color': false,
            'userInstructions': ''
        }
    }

    ngOnInit() {

        let order = this.dataService.getOrder();
        if (order) {
            this.order = order;
        }

        this.advisorySettings = {
            'asesoria_escala': false,
            'asesoria_color': false,
            'userInstructions': ''
        }

        this.form.valueChanges.subscribe(val => {
            console.log(val);
            this.validateForm(val);
        });
    }

    ngAfterContentInit() {
    }

    validateForm(val){
        if((val.asesoria_escala && val.asesoria_color) || 
        (!val.asesoria_escala && val.asesoria_color) ||
        (val.asesoria_escala && !val.asesoria_color)){
            // console.log('Ok ;)');
            this.advisorySettings = val;
            this.errorForm = false;
            return true;
        }else{
            this.errorForm = true;
            // console.log('deng X');
            return false;
        }
    }

    ngAfterViewInit() {
        this.form.valueChanges.subscribe(val => {
            console.log(val);
            this.validateForm(val);
        });
    }

    saveSettings(){
        localStorage.setItem('advisorySettings', JSON.stringify(this.advisorySettings));

        this.settings.emit('advisory-settings-saved');

        this.advisorySettings = {
            'asesoria_escala': false,
            'asesoria_color': false,
            'userInstructions': ''
        }
        $('#advisory-settings-div').remove()
        // $('#advisory-settings-div').fadeIn('slow');
        // $('#advisory-settings-div').addClass('open');
    }

    close(){
        this.settings.emit('advisory-settings-saved');

        this.advisorySettings = {
            'asesoria_escala': false,
            'asesoria_color': false,
            'userInstructions': ''
        }

        $('#advisory-settings-div').remove()
        // $('#advisory-settings-div').fadeOut('slow');
        // $('#advisory-settings-div').removeClass('open');

    }

}
