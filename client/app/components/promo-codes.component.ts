/**
 * Created on 06-01-2017.
 */

import {Component, OnInit, AfterViewChecked}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";
import {FilterProductsPipe} from "../shared/filter-products.pipe";
import {NewCodeComponent} from "./new-code.component";

@Component({
    moduleId: module.id,
    selector: 'printa',
    templateUrl: '../templates/promo-codes.template.html',
    directives: [ROUTER_DIRECTIVES, NewCodeComponent],
    pipes: [FilterProductsPipe]
})

export class PromoCodesComponent implements OnInit, AfterViewChecked {

    openNewCode: boolean;

    codes: any;

    selectedCode : any;

    constructor(private socketService: SocketService) {
        this.openNewCode = false;
    }

    ngOnInit() {
        /*
         * create all the event listeners first
         */
        this.socketService.socket.on('codes', function (codes) {
            this.codes = codes;

            for (var code of this.codes){
                code.fecha_inicio = this.stringDate(code.fecha_inicio);
                code.fecha_caducidad = this.stringDate(code.fecha_caducidad);
            }

        }.bind(this));

        this.socketService.socket.emit('get-codes');

    }

    stringDate(dateToString){

        var datemy = new Date(dateToString);
        var DateUTC = datemy.getTime() - new Date().getTimezoneOffset();
        var DateNow = new Date(DateUTC);
        var dateString = String(DateNow);
        var fecha_string = dateString.substr(4,11);

        return fecha_string
    }

    createCode() {
        this.openNewCode = true;
        console.log(this.openNewCode);
    }

    closeNewCode(event) {
        this.openNewCode = false;
    }

    changeState(code, value){

        this.socketService.socket.emit('change-state-code', {
            id: code,
            habilitado: value
        });

    }

}
