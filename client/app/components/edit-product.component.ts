/**
 * Created on 30-01-2017.
 */

import {Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, AfterContentInit}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";
import {FilterProductsPipe} from "../shared/filter-products.pipe";
import {FilterGroupByProductsPipe} from "../shared/filter-group-by-products.pipe";

@Component({
    moduleId: module.id,
    selector: 'edit-product',
    templateUrl: '../templates/edit-product.template.html',
    directives: [ROUTER_DIRECTIVES],
    pipes: [FilterProductsPipe, FilterGroupByProductsPipe]
})

export class EditProductComponent implements OnInit, AfterContentInit {

    @Input() openedEdit : boolean;

    @Input() product : [];

    @Input() allProducts : [];

    @Output() closeWindow = new EventEmitter();

    productsToSave : any;

    oneProduct : any;

    idProducts : any;

    productsToDelete : any;

    newVoid : any;

    form : any;  

    tipo : any;

    enableImpresion : boolean;
    enableVinilo : boolean;
    enableLaser : boolean;

    selectedFilter: string;
    groupByFilter: string;

    resultVal : any;

    productToRemove : any;

    constructor(private socketService : SocketService) {
        this.selectedFilter = 'impresion';
        this.groupByFilter = '';

        this.productsToSave = [];
        this.productsToDelete = [];
        this.oneProduct = [];
        this.idProducts = [];

        this.newVoid = [];

        this.enableImpresion = false;
        this.enableVinilo = false;
        this.enableLaser = false;

        this.resultVal = [];
        this.productToRemove = [];
    }

    ngOnInit() {
        
    }

    ngAfterContentInit() {
    }

    addProduct(tipo){
        
        switch(tipo){
            case 'impresion':
            this.newVoid = {
                'cantidad' : 0,
                'cantidad_alerta' : 0,
                'gramaje' : 0,
                'habilitado' : false,
                'id' : 0,
                'material' : this.product['material'],
                'parent' : 0,
                'tamano' : '',  
                'tipo' : this.product['tipo'],
                'valor_unitario' : 0,
            }
            break;
            case 'vinilo':
            this.newVoid = {
                'acabado' : '',
                'cantidad' : 0,
                'cantidad_alerta' : 0,
                'color' : '',
                'formato' : 'Rollo',
                'gramaje' : 0,
                'habilitado' : false,
                'id' : 0,
                'material' : this.product['material'],
                'metro_ancho_max' : 0,
                'metro_largo_rollo' : 0,
                'tipo' : this.product['tipo'],
                'valor_unitario' : 0,
            }
            break;
            case 'laser':
            this.newVoid = {
                'cantidad' : 0,
                'cantidad_alerta' : 0,
                'color' : '',
                'espesor' : 0,
                'habilitado' : false,
                'id' : 0,
                'material' : this.product['material'],
                'tipo' : this.product['tipo'],
                'valor_unitario' : 0,
            }
            break;

        }

        this.product['products'].push(this.newVoid);
    }

    enableToggle(type){
        switch(type){
            case 'impresion':
                this.enableImpresion = !this.enableImpresion;
                this.enableVinilo = false;
                this.enableLaser = false;
            break;
            case 'vinilo':
                this.enableVinilo = !this.enableVinilo;
                this.enableImpresion = false;
                this.enableLaser = false;
            break;
            case 'laser':
                this.enableLaser = !this.enableLaser;
                this.enableImpresion = false;
                this.enableVinilo = false;
            break;
        }
        console.log(this.enableImpresion);
    }

    validateForm(form){
        for(let i = 0; i < form['count']; i ++){

            if(form['tipo_producto'].length === 0){
                $('#validate-tipo-producto').css('display', 'block');
                this.resultVal[i] = false;
            }else{
                $('#validate-tipo-producto').css('display', 'none');
                //impresion
                if(form['tipo_producto'] == 'impresion'){
                    if(form['material'] == "" || form['tamano-'+i] === "" || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                //vinilo
                if(form['tipo_producto'] == 'vinilo'){
                    if(form['color'] == "" || form['acabado'] == "" || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                //laser
                if(form['tipo_producto'] == 'laser'){
                    if(form['material'] == "" || form['color'] == "" || form['espesor-'+i] == 0 || form['espesor-'+i] == null || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                //tintas
                if(form['tipo_producto'] == 'tinta'){
                    if(form['material'] == "" || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                if(form['tipo_producto'] == 'vinilo'){
                    //cantidad_inicial > cantidad alerta
                    let totalRollo = form['cantidad-'+i] * form['metro_largo_rollo-'+i];
                    
                    if(totalRollo <= form['cantidad_alerta-'+i]){
                        $('#validate-cantidad').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-cantidad').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }else{
                    //cantidad_inicial > cantidad alerta
                    if(form['cantidad-'+i]<= form['cantidad_alerta-'+i]){
                        $('#validate-cantidad').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-cantidad').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }
            }
        }

        if(this.resultVal.indexOf(false) == -1){
            return true;
        }else{
            return false;
        }
    }

    editProduct(form) {

        if(this.validateForm(form)){
            for(let i = 0; i < form['count']; i ++){
                this.oneProduct = {
                    'tipo_producto': form['tipo_producto'],
                    'material' : form['material'],
                    'tamano' : form['tamano-'+i] == null ? 0 : form['tamano-'+i],
                    'acabado' : form['acabado'],
                    'color' : form['color'],
                    'gramaje' : form['gramaje-'+i] == null ? 0 : form['gramaje-'+i],
                    'espesor' : form['espesor-'+i] == null ? 0 : form['espesor-'+i],
                    'cantidad' : form['cantidad-'+i] == null ? 0 : form['cantidad-'+i],
                    'metro_ancho_max' : form['metro_ancho_max-'+i] == null ? 0 : form['metro_ancho_max-'+i],
                    'metro_largo_rollo' : form['metro_largo_rollo-'+i] == null ? 0 : form['metro_largo_rollo-'+i],
                    'cantidad_alerta' : form['cantidad_alerta-'+i] == null ? 0 : form['cantidad_alerta-'+i],
                    'habilitado' : form['habilitado-'+i],
                    'valor_unitario' : form['valor_unitario-'+i],
                    'id' : form['id-'+i]
                };

                this.productsToSave.push(this.oneProduct);
            }  
            
            for(var product of this.productsToSave){
                if(product.id == 0){
                    this.socketService.socket.emit('add-product', {
                        product: product,
                        category: product.tipo_producto
                    });
                }else{
                    this.socketService.socket.emit('edit-product', {
                        product: product,
                        category: product.tipo_producto
                    });
                }
            }

            this.productsToSave = [];

            for(var toRemove of this.productToRemove){
                this.socketService.socket.emit('delete-product', {
                    product: toRemove,
                    category: toRemove.tipo
                });
            }

            this.closeEditProduct();
        }
    }

    deleteProduct(form){
        var doDelete = confirm('¿Está seguro de eliminar este producto? Esta acción no se puede deshacer.');
        if(doDelete){
            for(let i = 0; i < form['count']; i ++){
                this.idProducts = {
                    'tipo_producto': form['tipo_producto'],
                    'id' : form['id-'+i]
                };
                this.productsToDelete.push(this.idProducts);
            }

            for(var product of this.productsToDelete){
                this.socketService.socket.emit('delete-product', {
                    product: product,
                    category: product.tipo_producto
                });
            }

            this.closeEditProduct();
        }

    }

    removeProperty(i, product){
        var doRemove = confirm('¿Está seguro de eliminar esta referencia? Esta acción no se puede deshacer.');
        if(doRemove){
            this.product.products.splice(i, 1);
            this.productToRemove.push(product);
            event.preventDefault();
        }else{
            event.preventDefault();
        }
    }

    materalFilter(filter) {
        console.log(filter);
        this.selectedFilter = filter;
    }

    closeEditProduct() {
        this.closeWindow.emit('close');
    }
}
