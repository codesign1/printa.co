/**
 * Created on 15-05-2017.
 */

import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, AfterContentInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { AuthService } from "../services/auth.service";
import '../rxjs-extensions';
import { LoginComponent } from './login.component'
import { DashboardComponent } from "./dashboard.component";
import { SocketService } from "../services/socket.service";
import { Video } from "../classes/video";

@Component({
    moduleId: module.id,
    selector: 'edit-video',
    templateUrl: '../templates/edit-video.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class EditVideoComponent implements OnInit, AfterContentInit {

    @Input() openedEdit: boolean;

    @Input() video: [];

    @Output() closeWindow = new EventEmitter();

    videoToUpdate: any;
    form: any;
    resultVal: boolean;
    numVideos: number;
    videoToEdit: Video[];
    positions: any

    constructor(private auth: AuthService, private socketService: SocketService) {
        this.videoToUpdate = [];
        this.resultVal = true;
        this.positions = [];

        this.socketService.socket.on('get-count-videos', function (msg) {
            this.numVideos = msg[0].count;
        }.bind(this));

        // call the orders
        this.socketService.socket.emit('count-videos', this.auth.userProfile.email);
    }

    ngOnInit() {

    }

    ngAfterContentInit() {
    }

    validateForm(form) {

        if (form['title'] == "" || form['url'] == "" || form['position'] < 0) {
            $('#validate-all').css('display', 'block');
            this.resultVal = false;
        } else {
            $('#validate-all').css('display', 'none');
            this.resultVal = true;
        }

        if (this.resultVal) {
            return true;
        } else {
            return false;
        }
    }

    editVideo(form) {
        var videoUpdated = form;
        var oldVideo = this.video;

        if (this.validateForm(form)) {
            console.log(videoUpdated)
            //position
            if (videoUpdated.position > oldVideo.position || videoUpdated.position < oldVideo.position) {

                this.positions = {
                    inicial: oldVideo.position,
                    final: videoUpdated.position
                }
                console.log(this.positions);
                this.socketService.socket.emit('change-positions', {
                    infoPositions: this.positions
                });
            };

            var str = videoUpdated.url;
            var video_code = str.split("=");

            this.videoToUpdate = {
                title: videoUpdated.title,
                url: videoUpdated.url,
                position: videoUpdated.position,
                videocode: video_code[1]
            }

            this.socketService.socket.emit('update-video', {
                video: this.videoToUpdate,
                idVideo: videoUpdated.id
            });

            this.closeEditVideo();
        }
    }

    deleteVideo(form) {
        var doDelete = confirm('¿Está seguro de eliminar este video? Esta acción no se puede deshacer.');
        if (doDelete) {
            
            this.socketService.socket.emit('delete-video', {
                video: form.id
            });
            this.closeEditVideo();
        }

    }

    closeEditVideo() {
        this.closeWindow.emit('close');
    }
}
