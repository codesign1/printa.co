import { Component, AfterViewInit, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import '../rxjs-extensions';
import { AuthService } from "../services/auth.service";
import { LoginComponent } from './login.component'
import { AdminGuard } from "../services/guards/admin.guard";
import { DashboardComponent } from "./dashboard.component";
import { QuoteComponent } from "./quote.component";
import { HomeComponent } from "./home.component";
import { StepsMenuComponent } from "./steps-menu.component";
import { SocketService } from "../services/socket.service";
import { DataService } from "../services/data.service";
import { InitialsPipe } from "../shared/initials.pipe";
import { ColorService } from "../services/color.service";
import { Video } from "../classes/video";
import { BROWSER_SANITIZATION_PROVIDERS, DomSanitizationService, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'printa',
    templateUrl: '../templates/app.template.html',
    directives: [ROUTER_DIRECTIVES, LoginComponent, DashboardComponent, HomeComponent],
    pipes: [InitialsPipe]
})

export class AppComponent implements OnInit, AfterViewInit {
    title = 'Printa';
    userDropdownOpen: boolean;

    viewportWidth: number;

    videos: Video[];
    videocode: SafeUrl;
    url: string;

    ishome: boolean;
    isFirefox: boolean;

    constructor(private auth: AuthService, private router: Router, private socketService: SocketService,
        private dataService: DataService, private colorService: ColorService, private sanitizer: DomSanitizationService) {
        this.colored = false;
        this.viewportWidth = window.innerWidth;
        this.userDropdownOpen = false;

        window.addEventListener('beforeunload', function () {
            // forget the user if he/she is not authenticated
            if (!auth.authenticated() && localStorage.getItem('profile')) {
                localStorage.removeItem('profile');
            }

            // clean up everything
            this.dataService.cleanEverythingUp(true);
        }.bind(this));

        function debounce(func, wait, immediate) {
            var timeout;
            return function () {
                var context = this, args = arguments;
                var later = function () {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        }

        var resizeHandler = debounce(function () {
            var tips_height = $('.tipscont').height();
            $('.palocont').css('height', tips_height);

            var header_height = $('header').height();
            $('.maincontent').css('margin-top', header_height);

            this.viewportWidth = window.innerWidth;
        }.bind(this), 250, false);

        window.addEventListener('resize', resizeHandler);
        resizeHandler();
    }

    ngOnInit() {
        this.router.events.subscribe(val => {
            let splitUrl = val.url.split('?');
            if (splitUrl.length > 1) {
                // there is a querystring, let's remove it
                this.router.navigate([splitUrl[0]]);
            }
        });

        this.socketService.socket.on('get-all-videos-front', function (msg) {
            this.videos = msg;
            this.changeVideo(0);
        }.bind(this));

        this.socketService.socket.emit('get-videos-front');

        // validate webBrowser
        var userAgent = window.navigator.userAgent;

        var browsers = { chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i };

        var mybrowser = [];

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                mybrowser.push(key);
            }
        };

        if (mybrowser.length == 1) {
            if (mybrowser[0] == 'firefox' || mybrowser[0] == 'safari') {
                this.isFirefox = true;
            } else {
                this.isFirefox = false;
            }
        }

    }

    ngAfterViewInit() {
        $(document).on('click', '.dudasbajar', function () {
            $('html, body').animate({
                scrollTop: $(".videosdudas").offset().top - 75
            }, 800);
        });

        this.viewportWidth = window.innerWidth;
    }

    toggleDropdown() {
        if (this.auth.authenticated()) {
            this.userDropdownOpen = !this.userDropdownOpen;
        }
    }

    changeVideo(id) {
        this.url = this.videos[id].videocode;
        this.videocode = this.sanitizer.bypassSecurityTrustHtml('<iframe width="560" height="315" src="https://www.youtube.com/embed/' + this.url + '" frameborder="0" allowfullscreen></iframe>');
    }

    scrollContacto() {
        $('html, body').animate({
            scrollTop: $("#questions").offset().top - 75
        }, 800);
    }

    scrollVideos() {
        $('html, body').animate({
            scrollTop: $("#videosdudas").offset().top - 75
        }, 800);
    }
}
