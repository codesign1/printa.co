/**
 * Created by jsrolon on 11-08-2016.
 */

import {Component, ContentChild}       from '@angular/core';
import {ROUTER_DIRECTIVES, Router, NavigationEnd} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {ColorService} from "../services/color.service";
import { VideosFrontComponent } from "./videos-front.component";

@Component({
    moduleId: module.id,
    selector: 'printa',
    templateUrl: '../templates/home.template.html',
    directives: [ROUTER_DIRECTIVES, VideosFrontComponent]
})

export class HomeComponent {
    showTimePopup: boolean;
    comments : any[];
    actualcomment : any;

    constructor(private router: Router, private colorService : ColorService) {
        
        // if(this.isInsidePopupTime()) {
            this.showTimePopup = false;
        // } else {
        //     this.router.navigate(['/subir']);
        // }

        router.events.subscribe((val) => {
            if (val instanceof NavigationEnd){
                window.scrollTo(0,0);
            }
        });
        this.comments = [
            {pos:0, image:'img/LuisEduardoHuertas.png', comment: " \"He usado Printadelivery un par de veces y he quedado realmente sorprendido de la calidad y velocidad con la que trabajaban. Tengo un startup y muchas de las iniciativas que tenemos necesitan de tiempos de respuesta muy rápidos y no hemos encontrado un servicio que lo haga mejor. La facilidad de uso del servicio es también un gran diferenciador y contribuye a un mundo en donde todo puede hacerse por internet ayudando a empresas como la mía a mejorar su productividad\"", name: "Luis Eduardo Huertas", charge:"Administrador de empresas"},
            {pos:1, image:'img/JuanCarlosCamargo.png', comment: " \"Gracias a Printa se reduce tiempo y estrés en el trabajo, sabiendo que llegan mis planos al día siguiente tal como los solicité\"", name: "Juan Carlos Camargo", charge:"Arquitecto"},
        ];

        this.actualcomment = this.comments[0];
    }

    isInsidePopupTime() : boolean {
        // let now = new Date();
        // return (now.getUTCDay() === 6 && now.getUTCHours() > 5) || // if it's UTC staurday later than 5am
        //     now.getUTCDay() === 0 || // or it's UTC sunday
        //     (now.getUTCDay() === 1 && now.getUTCHours() < 2); // or it's UTC monday earlier than 2am
        return true;
    }

    closeTimePopup() {
        this.showTimePopup = false;
    }

    prevComment(pos){
        if(pos == 0){
            this.actualcomment = this.comments[this.comments.length-1];
        }else{
            this.actualcomment = this.comments[pos-1];
        }
    }

    nextComment(pos){
        if(pos == this.comments.length-1){
            this.actualcomment = this.comments[0];
        }else{
            this.actualcomment = this.comments[pos+1];
        }
    }

    navigateToPrintType(printType) {
        if(printType !== 'normal') {
            localStorage.setItem('selectedPrintType', printType);
        }

        if(this.isInsidePopupTime()) {
            this.showTimePopup = true;
        } else {
            this.router.navigate(['/subir']);
        }
    }
}
