/**
 * Created by jsrolon on 29-11-2016.
 */

import {Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, AfterContentInit}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";
import {FilterProductsPipe} from "../shared/filter-products.pipe";
import {FilterGroupByProductsPipe} from "../shared/filter-group-by-products.pipe";

@Component({
    moduleId: module.id,
    selector: 'new-product',
    templateUrl: '../templates/new-product.template.html',
    directives: [ROUTER_DIRECTIVES],
    pipes: [FilterProductsPipe, FilterGroupByProductsPipe]
})

export class NewProductComponent implements OnInit, AfterContentInit {

    @Input() opened : boolean;

    @Input() products : any;

    @Input() uniques : any;

    @Output() closeWindow = new EventEmitter();

    productToSave : any;

    productType : string;

    productImpresion : any;
    productVinyl : any;
    productLaser : any;
    productTinta : any;

    productTypeToSave : string;

    value : any;
    i : any;

    newVoid : any;
    inicialVoid : any;

    oneProduct : any;

    selectedFilter: string;
    groupByFilter: string;

    validateMessages : string;

    resultVal : any;

    constructor(private socketService : SocketService) {
        // we initialize a normal product because that will be the default state of the
        // radio buttons
        this.selectedFilter = 'impresion';
        this.groupByFilter = '';
        this.productType = 'impresion';

        this.productImpresion = true;
        this.productVinyl = false;
        this.productLaser = false;
        this.productTinta = false;

        this.oneProduct = [];
        this.productToSave = [];

        this.inicialVoid = [{
                'acabado' : '',
                'cantidad' : 0,
                'cantidad_alerta' : 0,
                'color' : '',
                'espesor' : 0,
                'formato' : 'Rollo',
                'gramaje' : 0,
                'habilitado' : false,
                'id' : 0,
                'parent' : 0,
                'tamano' : '',
                'valor_unitario' : 0,
                'metro_ancho_max' : 0,
                'metro_largo_rollo' : 0,
                'is_roll': false
        }];

        this.resultVal = [];

    }

    ngOnInit() {

    }

    ngAfterContentInit() {
    }

    addProduct(){

        this.newVoid = {
            'acabado' : '',
            'cantidad' : 0,
            'cantidad_alerta' : 0,
            'color' : '',
            'espesor' : 0,
            'formato' : 'Rollo',
            'gramaje' : 0,
            'habilitado' : false,
            'id' : 0,
            'parent' : 0,
            'tamano' : '',
            'valor_unitario' : 0,
            'metro_ancho_max' : 0,
            'metro_largo_rollo' : 0,
            'is_roll': false
        };

        this.inicialVoid.push(this.newVoid);
    }

    changeType(value){

        switch(value){
            case 'impresion':
                this.productImpresion = true;
                this.productVinyl = false;
                this.productLaser = false;
                this.productTinta = false;
                break;
            case 'vinilo':
                this.productImpresion = false;
                this.productVinyl = true;
                this.productLaser = false;
                this.productTinta = false;
                break;
            case 'laser':
                this.productImpresion = false;
                this.productVinyl = false;
                this.productLaser = true;
                this.productTinta = false;
                break;
            case 'tinta':
                this.productImpresion = false;
                this.productVinyl = false;
                this.productLaser = false;
                this.productTinta = true;
                break;
        }

    }

    changeTamano(i, form){
        
        if(form['tamano-'+i] == 'Rollo'){
            this.inicialVoid[i].is_roll = true;
        }else{
            this.inicialVoid[i].is_roll = false;
        }

    }

    validateForm(form){
        for(let i = 0; i < form['count']; i ++){

            if(form['tipo_producto'].length === 0){
                $('#validate-tipo-producto').css('display', 'block');
                this.resultVal[i] = false;
            }else{
                $('#validate-tipo-producto').css('display', 'none');
                //impresion
                if(form['tipo_producto'] == 'impresion'){
                    if(form['material'] == "" || form['tamano-'+i] === "" || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                //vinilo
                if(form['tipo_producto'] == 'vinilo'){
                    if(form['color'] == "" || form['acabado'] == "" || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                //laser
                if(form['tipo_producto'] == 'laser'){
                    if(form['material'] == "" || form['color'] == "" || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                //tintas
                if(form['tipo_producto'] == 'tinta'){
                    if(form['material'] == "" || form['valor_unitario-'+i] == 0 || form['valor_unitario-'+i] == null){
                        $('#validate-all').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-all').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }

                if(form['tipo_producto'] == 'vinilo'){
                    //cantidad_inicial > cantidad alerta
                    let totalRollo = form['cantidad-'+i] * form['metro_largo_rollo-'+i];
                    if(totalRollo <= form['cantidad_alerta-'+i]){
                        $('#validate-cantidad').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-cantidad').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }else{
                    //cantidad_inicial > cantidad alerta
                    if(form['cantidad-'+i]<= form['cantidad_alerta-'+i]){
                        $('#validate-cantidad').css('display', 'block');
                        this.resultVal[i] = false;
                    }else{
                        $('#validate-cantidad').css('display', 'none');
                        this.resultVal[i] = true;
                    }
                }
                
            }
        }

        if(this.resultVal.indexOf(false) == -1){
            return true;
        }else{
            return false;
        }
    }

    createProduct(form) {

        if(this.validateForm(form)){
            for(let i = 0; i < form['count']; i ++){
                this.oneProduct = {
                    'tipo_producto': form['tipo_producto'],
                    'material' : form['material'],
                    'tamano' : form['tamano-'+i] == null ? 0 : form['tamano-'+i],
                    'acabado' : form['acabado'],
                    'color' : form['color'],
                    'gramaje' : form['gramaje-'+i] == null ? 0 : form['gramaje-'+i],
                    'espesor' : form['espesor-'+i] == null ? 0 : form['espesor-'+i],
                    'cantidad' : form['cantidad-'+i] == null ? 0 : form['cantidad-'+i],
                    'metro_ancho_max' : form['metro_ancho_max-'+i] == null ? 0 : form['metro_ancho_max-'+i],
                    'metro_largo_rollo' : form['metro_largo_rollo-'+i] == null ? 0 : form['metro_largo_rollo-'+i],
                    'cantidad_alerta' : form['cantidad_alerta-'+i] == null ? 0 : form['cantidad_alerta-'+i],
                    'habilitado' : form['habilitado-'+i],
                    'valor_unitario' : form['valor_unitario-'+i]
                };

                this.productToSave.push(this.oneProduct);
            }

            for(var product of this.productToSave){
                this.socketService.socket.emit('add-product', {
                    product: product,
                    category: product.tipo_producto
                });
            }

            this.productToSave = [];

            this.inicialVoid = [{
                'acabado' : '',
                'cantidad' : 0,
                'cantidad_alerta' : 0,
                'color' : '',
                'espesor' : 0,
                'formato' : 'Rollo',
                'gramaje' : 0,
                'habilitado' : false,
                'id' : 0,
                'parent' : 0,
                'tamano' : '',
                'valor_unitario' : 0,
                'metro_ancho_max' : 0,
                'metro_largo_rollo' : 0
            }];

            this.closeNewProduct();
        }

    }

    removeProperty(i){
        var doRemove = confirm('¿Está seguro de eliminar esta referencia? Esta acción no se puede deshacer.');
        if(doRemove){
            console.log(i);
            this.inicialVoid.splice(i, 1);
            event.preventDefault();
        }else{
            event.preventDefault();
        }
    }

    materalFilter(filter) {
        this.selectedFilter = filter;
    }

    closeNewProduct() {
        this.closeWindow.emit('close');
    }
}
