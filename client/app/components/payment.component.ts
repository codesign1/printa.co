/**
 * Created by jsrolon on 16-08-2016.
 */

import {Component, OnInit, OnDestroy} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {ROUTER_DIRECTIVES, Router} from "@angular/router";
import {StepsMenuComponent} from "./steps-menu.component";
import {FlowControlService} from "../services/flow-control.service";
import {Order} from "../classes/order";
import {DataService} from "../services/data.service";
import {TruncatePipe} from "../shared/truncate.pipe";
import {SocketService} from "../services/socket.service";
import {PaymentService} from "../services/payment.service";
import {LeftPanelComponent} from "./left-panel.component";
import {ColorService} from "../services/color.service";
import { VideosFrontComponent } from "./videos-front.component";

declare var CryptoJS: any;
declare var NProgress: any;

@Component({
    moduleId: module.id,
    selector: 'payment',
    templateUrl: '../templates/payment.template.html',
    directives: [ROUTER_DIRECTIVES, StepsMenuComponent, LeftPanelComponent, VideosFrontComponent],
    pipes: [TruncatePipe]
})

export class PaymentComponent implements OnInit, OnDestroy {
    order: Order;
    signature: string;
    baseReturn: number;
    origin: string;

    constructor(private auth: AuthService, private router: Router, private flowControlService: FlowControlService,
                private dataService: DataService, private socketService: SocketService, private paymentService: PaymentService,
                private colorService: ColorService) {
        // use the polyfill to make sure we're getting the correct value (window.location.origin didnt seem to work)
        this.origin = window.location.protocol + "//" + window.location.hostname
            + (window.location.port ? ':' + window.location.port : '');
    }

    ngOnInit() {
        // execute the call for the order, just like in the status component
        this.socketService.socket.on('order-status-change', function (order) {
            // we only CHANGE the member variable here, because the saving in localStorage is done
            // by the socketservice
            this.order = order;

            // let priceToPay = this.order.totalPrice;
            // if (this.order.promoCode) {
            //     priceToPay = this.order.paymentTotal;
            // }

            this.signature = CryptoJS.MD5('EzP9crSZmCEuwneC9KYm4K2LI1~' + '581951~' + this.order.id + '~'
                + this.order.paymentTotal + '~' + 'COP').toString();
            this.baseReturn = this.order.documents.reduce((prev, curr) => {
                return prev + curr.price;
            }, 0);

            NProgress.done();
        }.bind(this));

        if (this.dataService.getOrder()) {
            let orderId = this.dataService.getOrder().id;
            this.socketService.socket.emit('get-order', orderId);
        } else {
            this.router.navigate(['/mis-pedidos']);
        }
    }

    paymentButtonClick(type) {
        this.order.paymentType = type;
        this.dataService.saveOrder(this.order);

        this.flowControlService.hasFinished(3);

        // TODO: remove this code when adecuate
        // this.socketService.socket.emit('payment-done', {
        //     userEmail: this.auth.userProfile.email,
        //     order: this.order
        // });

        if (type === 'cash') {
            NProgress.start();
            this.socketService.socket.emit('cash-payment-done', {
                orderId: this.dataService.order.id,
                userEmail: this.auth.userProfile.email
            });
            this.router.navigate(['/estado/' + this.dataService.order.id]);
        } else if (type === 'debit' || type === 'credit') {
            this.paymentService.attemptPayment(type, this.order.id);
        }
    }

    ngOnDestroy() {
        this.dataService.cleanEverythingUp(true);
    }
}
