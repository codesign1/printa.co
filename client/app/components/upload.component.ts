/**
 * Created by jsrolon on 09-08-2016.
 */

import {Component, OnInit, OnChanges, SimpleChange, AfterViewInit, OnDestroy, AfterViewChecked} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {Order} from "../classes/order";
import {Document} from "../classes/document";
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {QuoteComponent} from "./quote.component";
import {DataService} from "../services/data.service";
import {StepsMenuComponent} from "./steps-menu.component";
import {FlowControlService} from "../services/flow-control.service";
import {SettingsComponent} from "./settings.component";
import {AdvisoryComponent} from "./advisory.component";
import {TruncatePipe} from "../shared/truncate.pipe";
import {FORM_DIRECTIVES, FormBuilder, REACTIVE_FORM_DIRECTIVES, FormGroup, Validators} from "@angular/forms";
import {SocketService} from "../services/socket.service";
import {ColorService} from "../services/color.service";
import { VideosFrontComponent } from "./videos-front.component";

declare var $: any;
declare var NProgress: any;
declare var ProgressBar : any;

@Component({
    moduleId: module.id,
    selector: 'order-settings',
    templateUrl: '../templates/upload.template.html',
    directives: [ROUTER_DIRECTIVES, FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, StepsMenuComponent, SettingsComponent, AdvisoryComponent, VideosFrontComponent],
    pipes: [TruncatePipe]
})

export class UploadComponent implements OnInit, OnDestroy, AfterViewChecked {
    private order: Order;
    selectedDocument: Document;

    universitySites : any;

    dataForm: FormGroup;

    promoCodeValid: boolean;
    promoCodeWrong: boolean;

    currentProgressBar : any;

    boletasNumber = [0, 1, 2];

    lugar : string;
    splitlugar : any;
    namePlace : string;
    valorEnvioPlace : number;

    countPages : number;

    openAdvisory : boolean = false;

    constructor(private router: Router, private dataService: DataService,
                private flowControlService: FlowControlService, private authService: AuthService,
                private fb: FormBuilder, private socketService: SocketService, private colorService : ColorService) {
        this.order = new Order();

        this.order.owner = {};
        this.order.owner.email = this.authService.userProfile.email;

        var dateNow = new Date();
        var dateString = new String(dateNow);

        this.order.creation_time = dateString.substr(15,8);
        this.order.creation_date = dateNow;

        this.promoCodeValid = false;
        this.promoCodeWrong = false;

        if (!this.dataService.getOrder()) {
            this.dataService.saveOrder(this.order);
        }

        this.splitlugar = [];

        this.dataForm = fb.group({
            'nombre': ['', Validators.required],
            'email': [this.order.owner.email, Validators.compose([
                Validators.pattern('^[a-zA-Z0-9.!#$%&�*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'),
                Validators.required
            ])],
            'cedula': ['', Validators.required],
            'telefono': ['', Validators.compose([
                Validators.pattern('([0-9]{10})'),
                Validators.required
            ])],
            'lugar-entrega': ['casa'],
            'direccion': ['', Validators.required],
            'lugar': [],
            'hora': ['06:30:00'],
            'codigo-promocional': [''],
            'boletas-relaja': ['0']
        });

        this.dataForm.valueChanges.subscribe(form => {
            if (this.dataForm.controls['lugar-entrega'].value === 'universidad') {
                // set delivery location value
                // TODO: this must be better doable using observables
                this.lugar = this.dataForm.controls['lugar'].value;
                
                if(this.lugar != null){
                    this.splitlugar = this.lugar.split("+");
                    
                    this.namePlace = this.splitlugar[0];
                    this.valorEnvioPlace = parseInt(this.splitlugar[1]);

                    this.order.deliveryLocation =  this.namePlace;
                    this.order.shippingCost =  this.valorEnvioPlace;
                }

                this.order.deliveryTime = this.dataForm.controls['hora'].value;
                
                // the address will no longer be required
                this.dataForm.controls['direccion'].setValidators(null);
            } else {
                // add the required validator again
                this.dataForm.controls['direccion'].setValidators([Validators.required]);
            }

            // update the control's validity
            this.dataForm.controls['direccion'].updateValueAndValidity({
                emitEvent: false
            });

            this.dataService.saveOrder(this.order);

            if (this.promoCodeValid && (form['codigo-promocional'] !== this.order.promoCode)|| (!form['codigo-promocional'])) {
                this.promoCodeValid = false;
                this.promoCodeWrong = false;
            }

            this.dataService.orderDataValid = this.wholeUserInputValid();
        });
    }

    ngOnInit() {
        let order = this.dataService.getOrder();
        if (order) {
            this.order = order;
        }

        // register the event listener in case it happens
        this.socketService.socket.on('promo-code-valid', function (msg) {
            let pCode = msg.promoCode;
            let codeValid = msg.valid;

            if (this.order.promoCode === pCode && codeValid) {
                this.promoCodeValid = true;
                this.promoCodeWrong = false;
            } else if (this.order.promoCode === pCode && !codeValid) {
                this.promoCodeValid = false;
                this.promoCodeWrong = true;
            }

            // TODO: this piece is duplicated inside the form subscribe callback
            this.dataService.orderDataValid = this.wholeUserInputValid();

            this.dataService.saveOrder(this.order); // keep the state
        }.bind(this));

         // lugares de entrega
        this.socketService.socket.on('valid-place-delivery', function (places){
            this.universitySites = places;
        }.bind(this));
        this.socketService.socket.emit('get-valid-places');

    }

    ngOnChanges(changes: {[propertyName: string]: SimpleChange}) {
        console.log(changes);
    }

    navigateToQuoteComponent() {
        // store the order in the data service
        this.dataService.saveOrder(this.order);

        //update the fact we finished this component
        this.flowControlService.hasFinished(0);
        this.flowControlService.hasFinished(1); // TODO: fix the f out of this

        // navigate
        this.router.navigateByUrl('/cotiza');
    }

    ngAfterViewChecked() {
        let loaderContainer = $('.loader-container');
        if(loaderContainer.length) { // the fucking object exists
            if(!this.currentProgressBar) {
                this.currentProgressBar = new ProgressBar.Circle('.loader-wrapper', {
                    color: '#000',
                    strokeWidth: 8,

                    trailColor: 'rgba(0,0,0,0.4)',
                    trailWidth: 8,
                    easing: 'easeOut',
                    // step: function(state, circle, attachment) {
                    //     circle.path.setAttribute('stroke', state.color);
                    // },
                });
            }
        } else {
            this.currentProgressBar = null;
        }
    }

    onDocumentSelected(document: Document) {
        if (document.server_filename) {
            this.selectedDocument = document;
        }
    }

    fileChangeEvent(fileInput: any) {

        var file = (<Array<File>> fileInput.target.files)[0];

        $('#file-upload-input').val(null); // clear the value so that the change events are triggered
        // create a new, empty document with the name and add it to the list
        var doc = new Document();
        doc.name = file.name;
        let splitName = doc.name.split('.');
        let format = splitName[splitName.length-1];
        doc.format = format;

        // numero de paginas
        var reader = new FileReader();
        reader.readAsBinaryString(file);
        reader.onloadend = function(){
            doc.filePages = reader.result.match(/\/Type[\s]*\/Page[^s]/g).length;
        }

        let advisorySettings= localStorage.getItem('advisorySettings');
        let docData:any = JSON.parse(advisorySettings);
        if(advisorySettings){
            doc.printSettings.additionalFinishes.asesoria_color = docData.asesoria_color;
            doc.printSettings.additionalFinishes.asesoria_escala = docData.asesoria_escala;
            doc.printSettings.additionalFinishes.userInstructions = docData.instrucciones;
        }

        this.order.documents.push(doc);
        localStorage.removeItem('advisorySettings');
        // generate the file upload request
        this.uploadFile(doc, file)
    }

    uploadFile(document: Document, file: File) {
        this.makeFileRequest('/upload', [], file).then((result: any) => {
            // this is executed when the upload is complete
            document.fileSize = result.size;
            document.server_filename = result.filename;
            this.dataService.saveOrder(this.order);

            // this.flowControlService.hasFinished(0);
        }, (error) => {
            console.error(error);
        });
    }

    makeFileRequest(url: string, params: Array<string>, file: File) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("file", file, file.name);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", url, true);

            xhr.upload.onprogress = function(e) {
                if (e.lengthComputable) {
                    var percentage = (e.loaded / e.total);
                    this.currentProgressBar.animate(percentage);
                }
            }.bind(this);

            xhr.send(formData);
        });
    }

    updateOrder(event) {

        if (event === 'delete-document') {
            let toDelete = this.selectedDocument;
            this.selectedDocument = null;
            for (var i = 0; i < this.order.documents.length; i++) {
                if (this.order.documents[i].server_filename === toDelete.server_filename) {
                    this.order.documents.splice(i, 1);
                    break;
                }
            }
        }

        this.dataService.orderDataValid = this.wholeUserInputValid();
        this.dataService.saveOrder(this.order);
        
    }

    updateAdvisory(event){
        if(event === 'advisory-settings-saved'){
            let advisorySettings= localStorage.getItem('advisorySettings');
            if(advisorySettings){
                $('.input-file-advisory').trigger('click');
            }
            this.openAdvisory = false;
        }
    }

    startOver() {
        this.order = new Order();
        this.order.owner = {};
        this.order.owner.email = this.authService.userProfile.email;
        this.dataService.saveOrder(this.order);
        this.flowControlService.clearPriorTo(4);
    }

    navigateToDataOrder() {
        if (this.dataService.orderDataValid) {
            this.dataService.saveOrder(this.order);
            NProgress.start();
            this.flowControlService.hasFinished(0);
            localStorage.setItem('saveProgress', 'true');
            this.router.navigate(['/datos-orden']);
        }
    }

    verifyPromoCode() {
        if (this.order.promoCode && this.order.promoCode !== '') {
            console.log('promocode ' + this.order.promoCode);
            this.socketService.socket.emit('verify-promo-code', {
                promoCode: this.order.promoCode,
                buyer: this.authService.userProfile.email
            });
        }
    }

    private wholeUserInputValid() {
        // is there at least one document in the order?
        let atLeastOneDocument = this.order.documents.length > 0;

        // have all documents been configured?
        let allDocumentsConfigured = this.order.documents.reduce(function (prev, curr) {
            return prev && curr.verificado;
        }, true);

        return ((allDocumentsConfigured && atLeastOneDocument)
            || this.order.specialPromotions.numRelajaLaPelvis > 0);
        // there's no promo code, or if there is, it must be valid
    }

    saveUploadedData() {
        localStorage.setItem('saveProgress', 'true');
        this.router.navigate(['/']);
    }

    ngOnDestroy() {
        localStorage.removeItem('selectedPrintType');
        this.dataService.cleanEverythingUp(false);
    }

    openAdvisoryModal(){
        this.openAdvisory = true;
    }
}
