/**
 * Created by jsrolon on 09-08-2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var steps_menu_component_1 = require("./steps-menu.component");
var QuoteComponent = (function () {
    function QuoteComponent(socketService, dataService, flowControlService, router) {
        this.socketService = socketService;
        this.dataService = dataService;
        this.flowControlService = flowControlService;
        this.router = router;
    }
    QuoteComponent.prototype.ngOnInit = function () {
        if (this.flowControlService.isAllowedIn(2)) {
            // take the order that was just stored in the previous component
            this.order = this.dataService.newOrder;
            // send this to the socket
            this.socketService.socket.emit('new-order', this.order);
        }
        else {
            this.router.navigate(['subir']);
        }
    };
    QuoteComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'quote',
            templateUrl: '../templates/quote.template.html',
            directives: [router_1.ROUTER_DIRECTIVES, steps_menu_component_1.StepsMenuComponent]
        })
    ], QuoteComponent);
    return QuoteComponent;
}());
exports.QuoteComponent = QuoteComponent;
