/**
 * Created by jsrolon on 11-08-2016.
 */

import {Component}       from '@angular/core';
import {ROUTER_DIRECTIVES, Router, NavigationEnd} from '@angular/router';
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";

@Component({
    moduleId: module.id,
    selector: 'printa',
    templateUrl: '../templates/proximamente.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class ProximamenteComponent {
    constructor(private router: Router) {
        router.events.subscribe((val) => {
            if (val instanceof NavigationEnd){
                window.scrollTo(0,0);
            }
        });
    }
}
