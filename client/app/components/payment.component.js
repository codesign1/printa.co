/**
 * Created by jsrolon on 16-08-2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var steps_menu_component_1 = require("./steps-menu.component");
var PaymentComponent = (function () {
    function PaymentComponent(auth, router, flowControlService) {
        this.auth = auth;
        this.router = router;
        this.flowControlService = flowControlService;
    }
    PaymentComponent.prototype.ngOnInit = function () {
        if (this.flowControlService.isAllowedIn(3)) {
        }
        else {
            this.router.navigate(['subir']);
        }
    };
    PaymentComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'payment',
            templateUrl: '../templates/payment.template.html',
            directives: [router_1.ROUTER_DIRECTIVES, steps_menu_component_1.StepsMenuComponent]
        })
    ], PaymentComponent);
    return PaymentComponent;
}());
exports.PaymentComponent = PaymentComponent;
