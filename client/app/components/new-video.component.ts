/**
 * Created on 15-05-2017.
 */

import {Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, AfterContentInit}       from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import { AuthService } from "../services/auth.service";
import '../rxjs-extensions';
import {LoginComponent} from './login.component'
import {DashboardComponent} from "./dashboard.component";
import {SocketService} from "../services/socket.service";

@Component({
    moduleId: module.id,
    selector: 'new-video',
    templateUrl: '../templates/new-video.template.html',
    directives: [ROUTER_DIRECTIVES]
})

export class NewVideoComponent implements OnInit, AfterContentInit {

    @Input() openvideo : boolean;

    @Output() closeWindow = new EventEmitter();

    videoToSave : any;
    resultVal: any;
    numVideos: number;
    newPosition: number;

    constructor(private auth: AuthService, private socketService : SocketService) {        
        this.videoToSave = [];
        this.resultVal = [];
        this.numVideos = 0;
    }

    ngOnInit() {
        this.socketService.socket.on('get-count-videos', function (msg) {
            this.numVideos = msg[0].count;
        }.bind(this));

        // call the orders
        this.socketService.socket.emit('count-videos', this.auth.userProfile.email);
    }

    ngAfterContentInit() {
    }

    validateForm(form){
        if(form['name'] == "" || form['url'] == ""){
            $('#validate-all').css('display', 'block');
            this.resultVal = false;
        }else{
            $('#validate-all').css('display', 'none');
            this.resultVal = true;
        }

        if(this.resultVal == true){
            return true;
        }else{
            return false;
        }
    }

    createVideo(form) {
        
        if(this.validateForm(form)){
            
            this.newPosition = Math.floor(this.numVideos) + 1;
            var str = form['url'];
            var video_code = str.split("=");
            console.log(form);

            this.videoToSave = {
                title: form['title'],
                url: form['url'],
                show: false,
                position: this.newPosition,
                videocode: video_code[1],
            };

            this.socketService.socket.emit('add-video', {
                videoInfo: this.videoToSave
            });

            this.closeNewVideo();
        }
        
    }

    closeNewVideo() {
        this.closeWindow.emit('close');
    }
}
