/**
 * Created by jsrolon on 09-08-2016.
 */

import {Component, OnInit, OnChanges, SimpleChange, AfterViewInit, OnDestroy, AfterViewChecked} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {Order} from "../classes/order";
import {Document} from "../classes/document";
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {QuoteComponent} from "./quote.component";
import {DataService} from "../services/data.service";
import {StepsMenuComponent} from "./steps-menu.component";
import {FlowControlService} from "../services/flow-control.service";
import {SettingsComponent} from "./settings.component";
import {TruncatePipe} from "../shared/truncate.pipe";
import {FORM_DIRECTIVES, FormBuilder, REACTIVE_FORM_DIRECTIVES, FormGroup, Validators} from "@angular/forms";
import {SocketService} from "../services/socket.service";
import {ColorService} from "../services/color.service";
import { VideosFrontComponent } from "./videos-front.component";

declare var $: any;
declare var NProgress: any;
declare var ProgressBar : any;

@Component({
    moduleId: module.id,
    selector: 'data-order',
    templateUrl: '../templates/data-order.template.html',
    directives: [ROUTER_DIRECTIVES, FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, StepsMenuComponent, SettingsComponent, VideosFrontComponent],
    pipes: [TruncatePipe]
})

export class DataOrderComponent implements OnInit, OnDestroy, AfterViewChecked {
    private order: Order;
    selectedDocument: Document;

    universitySites : any;

    dataForm: FormGroup;

    promoCodeValid: boolean;
    promoCodeWrong: boolean;

    currentProgressBar : any;

    boletasNumber = [0, 1, 2];

    lugar : string;
    splitlugar : any;
    namePlace : string;
    valorEnvioPlace : number;

    countPages : number;

    constructor(private router: Router, private dataService: DataService,
                private flowControlService: FlowControlService, private authService: AuthService,
                private fb: FormBuilder, private socketService: SocketService, private colorService : ColorService) {
        this.order = new Order();

        this.order.owner = {};
        this.order.owner.email = this.authService.userProfile.email;

        var dateNow = new Date();
        var dateString = new String(dateNow);

        this.order.creation_time = dateString.substr(15,8);
        this.order.creation_date = dateNow;

        this.promoCodeValid = false;
        this.promoCodeWrong = false;

        if (!this.dataService.getOrder()) {
            this.dataService.saveOrder(this.order);
        }

        this.splitlugar = [];

        this.dataForm = fb.group({
            'nombre': ['', Validators.required],
            'email': [this.order.owner.email, Validators.compose([
                Validators.pattern('^[a-zA-Z0-9.!#$%&�*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'),
                Validators.required
            ])],
            'cedula': ['', Validators.required],
            'telefono': ['', Validators.compose([
                Validators.pattern('([0-9]{10})'),
                Validators.required
            ])],
            'lugar-entrega': ['casa'],
            'direccion': ['', Validators.required],
            'lugar': [],
            'hora': ['06:30:00'],
            'codigo-promocional': [''],
            'boletas-relaja': ['0']
        });

        this.dataForm.valueChanges.subscribe(form => {
            if (this.dataForm.controls['lugar-entrega'].value === 'universidad') {
                // set delivery location value
                // TODO: this must be better doable using observables
                this.lugar = this.dataForm.controls['lugar'].value;
                
                if(this.lugar != null){
                    this.splitlugar = this.lugar.split("+");
                    
                    this.namePlace = this.splitlugar[0];
                    this.valorEnvioPlace = parseInt(this.splitlugar[1]);

                    this.order.deliveryLocation =  this.namePlace;
                    this.order.shippingCost =  this.valorEnvioPlace;
                }

                this.order.deliveryTime = this.dataForm.controls['hora'].value;
                
                // the address will no longer be required
                this.dataForm.controls['direccion'].setValidators(null);
            } else {
                // add the required validator again
                this.dataForm.controls['direccion'].setValidators([Validators.required]);
            }

            // update the control's validity
            this.dataForm.controls['direccion'].updateValueAndValidity({
                emitEvent: false
            });

            this.dataService.saveOrder(this.order);

            if (this.promoCodeValid && (form['codigo-promocional'] !== this.order.promoCode)|| (!form['codigo-promocional'])) {
                this.promoCodeValid = false;
                this.promoCodeWrong = false;
            }

            this.dataService.orderDataValid = this.wholeUserInputValid();
        });
    }

    ngOnInit() {
        let order = this.dataService.getOrder();
        if (order) {
            this.order = order;
        }

        // register the event listener in case it happens
        this.socketService.socket.on('promo-code-valid', function (msg) {
            let pCode = msg.promoCode;
            let codeValid = msg.valid;

            if (this.order.promoCode === pCode && codeValid) {
                this.promoCodeValid = true;
                this.promoCodeWrong = false;
            } else if (this.order.promoCode === pCode && !codeValid) {
                this.promoCodeValid = false;
                this.promoCodeWrong = true;
            }

            // TODO: this piece is duplicated inside the form subscribe callback
            this.dataService.orderDataValid = this.wholeUserInputValid();

            this.dataService.saveOrder(this.order); // keep the state
        }.bind(this));

         // lugares de entrega
        this.socketService.socket.on('valid-place-delivery', function (places){
            this.universitySites = places;
        }.bind(this));
        this.socketService.socket.emit('get-valid-places');

    }

    ngOnChanges(changes: {[propertyName: string]: SimpleChange}) {
        console.log(changes);
    }

    // navigateToQuoteComponent() {
    //     // store the order in the data service
    //     this.dataService.saveOrder(this.order);

    //     //update the fact we finished this component
    //     this.flowControlService.hasFinished(0);
    //     this.flowControlService.hasFinished(1); // TODO: fix the f out of this

    //     // navigate
    //     this.router.navigateByUrl('/cotiza');
    // }

    ngAfterViewChecked() {
        let loaderContainer = $('.loader-container');
        if(loaderContainer.length) { // the fucking object exists
            if(!this.currentProgressBar) {
                this.currentProgressBar = new ProgressBar.Circle('.loader-wrapper', {
                    color: '#000',
                    strokeWidth: 8,

                    trailColor: 'rgba(0,0,0,0.4)',
                    trailWidth: 8,
                    easing: 'easeOut',
                    // step: function(state, circle, attachment) {
                    //     circle.path.setAttribute('stroke', state.color);
                    // },
                });
            }
        } else {
            this.currentProgressBar = null;
        }
    }

    updateOrder(event) {
        if (event === 'delete-document') {
            let toDelete = this.selectedDocument;
            this.selectedDocument = null;
            for (var i = 0; i < this.order.documents.length; i++) {
                if (this.order.documents[i].id === toDelete.id) {
                    this.order.documents.splice(i, 1);
                    break;
                }
            }
        }
        this.dataService.orderDataValid = this.wholeUserInputValid();
        this.dataService.saveOrder(this.order);
    }

    startOver() {
        this.order = new Order();
        this.order.owner = {};
        this.order.owner.email = this.authService.userProfile.email;
        this.dataService.saveOrder(this.order);
        this.flowControlService.clearPriorTo(4);
    }

    navigateToQuote() {
        if (this.dataService.orderDataValid) {
            NProgress.start();
            this.flowControlService.hasFinished(1);
            localStorage.setItem('saveProgress', 'true');
            this.router.navigate(['/cotiza']);
        }
    }

    verifyPromoCode() {
        if (this.order.promoCode && this.order.promoCode !== '') {
            console.log('promocode ' + this.order.promoCode);
            this.socketService.socket.emit('verify-promo-code', {
                promoCode: this.order.promoCode,
                buyer: this.authService.userProfile.email
            });
        }
    }

    private wholeUserInputValid() {
        console.log(this.dataForm.valid, this.order.promoCode, this.promoCodeValid, this.order.specialPromotions.numRelajaLaPelvis);
        return this.dataForm.valid
            && ((this.order.promoCode === '' || !this.order.promoCode) || (this.order.promoCode !== '' && this.promoCodeValid));
        // there's no promo code, or if there is, it must be valid
    }

    saveUploadedData() {
        localStorage.setItem('saveProgress', 'true');
        this.router.navigate(['/']);
    }

    ngOnDestroy() {
        localStorage.removeItem('selectedPrintType');
        this.dataService.cleanEverythingUp(false);
    }
}
