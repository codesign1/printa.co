/**
 * Created by jsrolon on 16-08-2016.
 */

import {Component, OnInit, AfterViewInit, OnDestroy} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {ROUTER_DIRECTIVES, Router, ActivatedRoute, Params} from "@angular/router";
import {StepsMenuComponent} from "./steps-menu.component";
import {FlowControlService} from "../services/flow-control.service";
import {Order} from "../classes/order";
import {DataService} from "../services/data.service";
import {TruncatePipe} from "../shared/truncate.pipe";
import {SocketService} from "../services/socket.service";
import {LeftPanelComponent} from "./left-panel.component";
import {ColorService} from "../services/color.service";

declare var NProgress: any;
declare var window: any;
declare var FB: any;
declare var gapi: any;
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'state',
    templateUrl: '../templates/status.template.html',
    directives: [ROUTER_DIRECTIVES, StepsMenuComponent, LeftPanelComponent],
    pipes: [TruncatePipe]
})

export class StatusComponent implements OnInit, AfterViewInit, OnDestroy {
    private NOTIFICATION_TIMEOUT = 120000;

    private order: Order;
    origin: string;
    currentUrl: string;
    timeoutID;
    showWaitModal: boolean;

    constructor(private auth: AuthService, private router: Router, private flowControlService: FlowControlService,
                private dataService: DataService, private socketService: SocketService, private activatedRoute: ActivatedRoute,
                private colorService: ColorService) {
        this.origin = window.location.origin;
    }

    ngOnInit() {
        // register the event listener for subsequent changes, which can be order status changes or simply receiving the order
        this.socketService.socket.on('order-status-change', function (order) {
            // we only CHANGE the member variable here, because the saving in localStorage is done
            // by the socketservice
            this.order = order;

            this.currentUrl = this.origin + '/estado/' + this.order.id;

            if (!this.order.paid && this.order.paymentType !== 'cash') {
                // we still don't have payment confirmation from payu
                // show the modal with spinner and wait message
                this.showWaitModal = true;

                // we will wait two minutes for the server to tell us if the payment is in the db or not
                this.timeoutID = setTimeout(function () {
                    // if after all this time the order is still not paid, return the user to the payments page
                    if (!this.order.paid) {
                        alert('Hubo un problema recibiendo el pago desde PayU. Te devolveremos a la página de pagos.');
                        this.router.navigate(['/paga']);
                    }
                }.bind(this), this.NOTIFICATION_TIMEOUT);
            } else if ((!this.order.paid && this.order.paymentType === 'cash') || this.order.paid) {
                // be clean
                this.showWaitModal = false;
                if (this.timeoutID) {
                    clearTimeout(this.timeoutID);
                    this.timeoutID = null;
                }
                // so that it shows correctly in the menu
                this.flowControlService.hasFinished(4);
            }

            NProgress.done();
        }.bind(this));

        // we will get the order from the order-status-change event by asking the server for it
        // the event listener is already registered, so it's ok
        this.activatedRoute.params.forEach(function (params: Params) {
            NProgress.start();
            let orderId = params['orderId'];
            this.socketService.socket.emit('get-order', orderId);

            // we have to register the event listener here so that the orderId is available in the closure
            this.socketService.socket.on('order-not-found', function (recOrderId) {
                if (recOrderId === orderId) {
                    NProgress.done();
                    this.router.navigate(['/mis-pedidos']);
                }
            }.bind(this));
        }.bind(this));
    }

    ngAfterViewInit() {
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.4";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        // render all the google api buttons on the page
        gapi.plus.go();
    }

    exitStatusComponent() {
        this.order = null;
        this.dataService.clearOrder();
        this.flowControlService.clearPriorTo(5);

        this.dataService.cleanEverythingUp(true);

        this.router.navigate(['/mis-pedidos']);
    }

    ngOnDestroy() {
        this.dataService.cleanEverythingUp(true);
    }
}
