"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
require('../rxjs-extensions');
var login_component_1 = require('./login.component');
var dashboard_component_1 = require("./dashboard.component");
var home_component_1 = require("./home.component");
var AppComponent = (function () {
    function AppComponent(auth) {
        this.auth = auth;
        this.title = 'Printa';
    }
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'printa',
            templateUrl: '../templates/app.template.html',
            directives: [router_1.ROUTER_DIRECTIVES, login_component_1.LoginComponent, dashboard_component_1.DashboardComponent, home_component_1.HomeComponent]
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
