/**
 * Created by jsrolon on 09-08-2016.
 */

import {Component} from "@angular/core";
import {AuthService} from "../services/auth.service";

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: '../templates/login.template.html',
})

export class LoginComponent {
    constructor(private auth: AuthService) {

    }
}
