/**
 * Created by jsrolon on 16-08-2016.
 */


import {
    Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit,
    AfterContentInit
} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {Order} from "../classes/order";
import {Document} from "../classes/document";
import {Router, ROUTER_DIRECTIVES} from "@angular/router";
import {QuoteComponent} from "./quote.component";
import {DataService} from "../services/data.service";
import {StepsMenuComponent} from "./steps-menu.component";
import {FlowControlService} from "../services/flow-control.service";
import {TruncatePipe} from "../shared/truncate.pipe";
import {PrintOptionsService} from "../services/print-options.service";
import {ColorService} from "../services/color.service";
import { SocketService } from "../services/socket.service";

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'settings',
    templateUrl: '../templates/settings.template.html',
    directives: [ROUTER_DIRECTIVES, StepsMenuComponent],
    pipes: [TruncatePipe]
})

export class SettingsComponent implements OnInit, AfterViewInit, AfterContentInit {
    @Input()
    document: Document;

    @Output()
    settings = new EventEmitter();

    @ViewChild('settingsForm') form;

    colors = [
        'Full color',
        'Escala de grises'
    ];

    sizes: string[];

    papers: string[];

    grams: string[];

    materials: string[];

    materialColors: any[];

    materialMaxSize: string;
    materialThickness: string;

    selectedPrintType: string;

    finishes: string[];

    normalPrintOptions: any[];
    uniquePrintOptions: any[];

    vinylOptions: any[];

    laserOptions: any[];
    uniqueLaserOptions: any[];

    /*
     * Necessary storage for vinyl cut options
     */
    vinylColors: string[];
    vinylFinishes: string[];

    errorSettings: boolean = true;
    errorAsesoria: boolean = false;

    constructor(private router: Router, private flowControlService: FlowControlService,
                private printOptions: PrintOptionsService, private colorService: ColorService, private socketService: SocketService) {
        this.papers = ["Seleccione tipo de papel"];
        // this.papers = Object.keys(this.printOptions.getPrintOptions()); // get all available papers from the key of the object
        // this.materials = Object.keys(this.printOptions.getCutOptions());
        this.materials = ["Seleccione material"];

        this.selectedPrintType = localStorage.getItem('selectedPrintType');
        
        //this.selectedPrintType = 'normal';
        /*
         * Get the vinyl associated data
         */
        // this.vinylColors = Object.keys(this.printOptions.getVinylOptions());
        this.vinylColors = ["Seleccione color"];
        this.vinylFinishes = [];

        this.finishes = [];
        this.materialColors = [];

        this.normalPrintOptions = [];
        this.uniquePrintOptions = [];

        this.vinylOptions = [];

        this.laserOptions = [];
        this.uniqueLaserOptions = [];

        /* Normal print options */

        this.socketService.socket.on('normal-print-options', function (msg) {
            this.normalPrintOptions = msg;
            this.uniquePrintOptions = [];

            this.normalPrintOptions.forEach(function(product){
                let unique = this.uniquePrintOptions.find(function(uniq) {
                    return uniq.material === product.material;
                });

                if(!unique) {
                    unique = {
                        material: product.material,
                        tipo: product.tipo,
                        tamano: []
                    };

                    this.papers.push(unique.material);
                    
                    this.uniquePrintOptions.push(unique);
                    
                    let tamano = {
                        tamano: product.tamano,
                        gramaje: [product.gramaje]
                    }
                    
                    unique.tamano.push(tamano);

                }else{
                    let tamano = unique.tamano.find(function(uniq) {
                        return uniq.tamano === product.tamano;
                    });

                    if(!tamano) {
                        tamano = {
                            tamano: product.tamano,
                            gramaje: [product.gramaje]
                        };
                        unique.tamano.push(tamano);
                    }else{
                        tamano.gramaje.push(product.gramaje);
                    }
                }

            }, this);
                        
            // console.log(this.uniquePrintOptions);

            this.document.printSettings.paper = this.papers[0];
            
        }.bind(this));

        /* vinyl options */

        this.socketService.socket.on('vinyl-options', function (msg) {
            this.vinylOptions = msg;
            this.vinylOptions.forEach(function(vinyl){
                this.vinylColors.push(vinyl.color);
            }, this);

            let vinylOptionOne = {
                acabado:"Seleccione acabado",
                cantidad:"1",
                cantidad_alerta:"5",
                color:"Seleccione color",
                formato:"Rollo",
                gramaje:"1",
                habilitado:true,
                id:1,
                material:"Vinilo",
                metro_ancho_max:"0.6",
                metro_largo_rollo:"10",
                mtscounter:"3",
                tipo:"vinilo",
                valor_unitario:"0"
            };

            this.vinylOptions.unshift(vinylOptionOne);

        }.bind(this));

        /* laser options */

        this.socketService.socket.on('laser-options', function (msg) {
            this.laserOptions = msg;
            this.uniqueLaserOptions = [];
            this.laserOptions.forEach(function(laser){
                let unique = this.uniqueLaserOptions.find(function(uniq) {
                    return uniq.material === laser.material;
                });

                if(!unique) {
                    unique = {
                        material: laser.material,
                        color: ["Seleccione color"],
                        max: 'Pliego',
                        thickness: laser.espesor
                    };
                    
                    this.materials.push(unique.material);

                    this.uniqueLaserOptions.push(unique);
                }
                                
                let color = laser.color;
                
                unique.color.push(color);

            }, this);

            // console.log(this.uniqueLaserOptions);

        }.bind(this));

        this.socketService.socket.emit('get-normal-print-options');
        this.socketService.socket.emit('get-vinyl-options');
        this.socketService.socket.emit('get-laser-options');
    }

    ngOnInit() {
        if (this.flowControlService.isAllowedIn(1)) {
            // good
        } else {
            this.router.navigate(['/subir']);
        }
    }

    ngAfterContentInit() {
        let storedPrintType = localStorage.getItem('selectedPrintType');
        if (storedPrintType) {
            // this.document.printSettings.type = this.selectedPrintType;
            //
            // // set the correct options coming from the print panel
            // this.selectedPrintType = 'vinilo';
            // this.document.printSettings.color = 'Transparente';
            //
            // var selectedVinylColor = 'Transparente';
            // if (this.printOptions.getVinylOptions()[selectedVinylColor]) {
            //     this.vinylFinishes = this.printOptions.getVinylOptions()[selectedVinylColor].finishes;
            //
            //     if (!this.document.printSettings.paper
            //         || this.vinylFinishes.indexOf(this.document.printSettings.paper) < 0) {
            //         this.document.printSettings.paper = this.vinylFinishes[0];
            //     }
            // }
            this.document.printSettings.type = storedPrintType;
            localStorage.removeItem('selectedPrintType');
        }
    }

    ngAfterViewInit() {
        this.form.valueChanges.subscribe(val => {
            if (parseInt(val['copias']) < 1) {
                this.document.printSettings.numCopies = 1;
            }

            this.validateForm();

            /**
             * Disgusting code follows
             */
            if (val['tipo-impresion'] === 'normal') {
                if (!this.document.verificado) {
                    // set the correct options after being in the laser panel
                    if (this.selectedPrintType !== 'normal') {
                        this.selectedPrintType = 'normal';
                        this.document.printSettings.paper = this.papers[0];
                        this.document.printSettings.color = 'Full color';
                        this.materialColors = [];
                    }

                    if(val['tiro']){
                        this.document.printSettings.tiro = true;
                    }else{
                        this.document.printSettings.tiro = false;
                    }

                    var paperType = val['tipo-papel'];
                    // we start filling in the data in order
                    // this.printOptions.getPrintOptionsTwo()[paperType]
                    if (this.papers.indexOf(paperType) != -1) {
                        
                        if(paperType === 'Seleccione tipo de papel'){
                            
                            this.sizes = ["Seleccione tamaño"];
                            if (!this.document.printSettings.size
                                || this.sizes.indexOf(this.document.printSettings.size) < 0) {
                                this.document.printSettings.size = this.sizes[0];
                            }

                            this.grams = ["Seleccione gramaje"];
                            this.document.printSettings.grams = this.grams[0]

                        }else{
                            this.sizes = ["Seleccione tamaño"];
                            // this.sizes = Object.keys(this.printOptions.getPrintOptionsTwo()[paperType]);
                            this.uniquePrintOptions[this.papers.indexOf(paperType)-1].tamano.forEach(function(size){
                                this.sizes.push(size.tamano);
                            }, this);

                            if(this.sizes[0] != "Seleccione tamaño"){
                                this.grams.unshift("Seleccione tamaño");
                            }
                            
                            // only select the define value if it is appropriate: the value does not exist or is not inside
                            // the new array
                            if (!this.document.printSettings.size
                                || this.sizes.indexOf(this.document.printSettings.size) < 0) {
                                this.document.printSettings.size = this.sizes[0];
                            }
    
                            // now we get the available grams and finishes
                            // var tamanoPaper = this.printOptions.getPrintOptionsTwo()[paperType][val['tamano']];
                            var tamanoPaper = this.uniquePrintOptions[this.papers.indexOf(paperType)-1].tamano[this.sizes.indexOf(val['tamano'])];
                            
                            if (val['tamano'] && tamanoPaper) {
                                // if (!('grams' in tamanoPaper)) { // the material has colors
                                //     this.materialColors = Object.keys(tamanoPaper);
                                    
                                //     // only select the define value if it is appropriate: the value does not exist or is not inside
                                //     // the new array
                                //     if (!this.document.printSettings.additionals.paperColor
                                //         || this.materialColors.indexOf(this.document.printSettings.additionals.paperColor) < 0) {
                                //         this.document.printSettings.additionals.paperColor = this.materialColors[0];
                                //     }
                                    
                                //     this.finishes = tamanoPaper.finishes;
    
                                //     if (val['color-papel']) {
                                //         this.grams = tamanoPaper[val['color-papel']].grams;
    
                                //         // only select the define value if it is appropriate: the value does not exist or is not inside
                                //         // the new array
                                //         if (!this.document.printSettings.grams
                                //             || this.grams.indexOf(this.document.printSettings.size) < 0) {
                                //             this.document.printSettings.grams = this.grams[0];
                                //         }
    
                                //         // select the paper finishes
                                //         this.finishes = tamanoPaper[val['color-papel']].finishes;
                                //         if (!this.document.printSettings.additionals.paperFinish
                                //             || this.finishes.indexOf(this.document.printSettings.additionals.paperFinish) < 0) {
                                //             this.document.printSettings.additionals.paperFinish = this.finishes[0];
                                //         }
                                //     }
                                // } else {
                                    this.materialColors = [];
                                    this.grams = tamanoPaper.gramaje;
                                    if(this.grams[0] != "Seleccione gramaje"){
                                        this.grams.unshift("Seleccione gramaje");
                                    }
                                    // console.log(tamanoPaper.gramaje);
                                    // if(tamanoPaper.gramaje){
                                    //     tamanoPaper.gramaje.forEach(function(gram){
                                    //         this.grams.push(gram);
                                    //     });
                                    // }
                                    
                                    if (!this.document.printSettings.grams
                                        || this.grams.indexOf(this.document.printSettings.grams) < 0) {
                                        this.document.printSettings.grams = this.grams[0] || '';
                                    }
                                    // console.log(this.grams);
                                // }
                            }
                        }
                        
                    }
                }
            } else if (val['tipo-impresion'] === 'vinilo') {
                if (!this.document.verificado) {
                    // set the correct options coming from the print panel
                    if (this.selectedPrintType !== 'vinilo') {
                        this.selectedPrintType = 'vinilo';
                        this.document.printSettings.color = this.vinylOptions[0].color;
                    }
                    
                    var selectedVinylColor = val['color-material'];
                    
                    if (this.vinylOptions[this.vinylColors.indexOf(selectedVinylColor)]) {
                        if(selectedVinylColor != 'Seleccione color'){
                            this.vinylFinishes = [this.vinylOptions[this.vinylColors.indexOf(selectedVinylColor)].acabado];
                            this.vinylFinishes.unshift("Seleccione acabado");
                        }else{
                            this.vinylFinishes = [this.vinylOptions[this.vinylColors.indexOf(selectedVinylColor)].acabado];
                        }
                        
                        
                        if (!this.document.printSettings.paper
                            || this.vinylFinishes.indexOf(this.document.printSettings.paper) < 0) {
                            this.document.printSettings.paper = this.vinylFinishes[0];
                        }
                    
                        if (!this.document.printSettings.grams
                            || this.vinylFinishes.indexOf(this.document.printSettings.grams) < 0) {
                            this.document.printSettings.grams = this.vinylOptions[this.vinylColors.indexOf(selectedVinylColor)].gramaje;
                        }
                    }
                }
            } else if (val['tipo-impresion'] === 'laser') {
                if (!this.document.verificado) {
                    // set the correct options coming from the print panel
                    if (this.selectedPrintType !== 'laser') {
                        this.selectedPrintType = 'laser';
                        this.document.printSettings.paper = this.materials[0];
                    }
                    this.materialColors = ["Seleccione color"];
                    var selectedMaterial = val['material'];

                    if(selectedMaterial == "Seleccione material"){
                        this.materialColors = ["Seleccione color"];
                        this.document.printSettings.color = this.materialColors[0];
                        // put the stuff that should not be
                        this.materialMaxSize = 'No disponible';
                        this.materialThickness = 'No disponible';
                    }else{
                        
                        if (this.materials[this.materials.indexOf(selectedMaterial)]) {
                            this.materialColors = this.uniqueLaserOptions[this.materials.indexOf(selectedMaterial)-1].color || ['No disponible'];
                            
                            if (!this.document.printSettings.color
                                || this.materialColors.indexOf(this.document.printSettings.color) < 0) {
                                this.document.printSettings.color = this.materialColors[0];
                            }
    
                            // put the stuff that should not be
                            this.materialMaxSize = this.uniqueLaserOptions[this.materials.indexOf(selectedMaterial)].max || 'No disponible';
                            this.document.printSettings.size = this.materialMaxSize;
                            
                            this.materialThickness = this.uniqueLaserOptions[this.materials.indexOf(selectedMaterial)-1].thickness || 'No disponible';
                            this.document.printSettings.grams = this.materialThickness;
                        }
                    }
                }
            }
        });
    }

    validateAsesoria(){
        let settings = this.document.printSettings;
        if(this.document.format != 'pdf'){
            if((settings.additionalFinishes.asesoria_escala && settings.additionalFinishes.asesoria_color) || 
            (!settings.additionalFinishes.asesoria_escala && settings.additionalFinishes.asesoria_color) ||
            (settings.additionalFinishes.asesoria_escala && !settings.additionalFinishes.asesoria_color)){
                console.log('Ok ;)');
                this.errorAsesoria = false;
                return true;
            }else{
                this.errorAsesoria = true;
                console.log('deng X');
                return false;
            }
        }

    }

    validateForm(){
        let toReturn = false;
        let settings = this.document.printSettings;
        
        if(settings.type == 'normal'){
            if(this.document.format != 'pdf'){
                toReturn = settings.paper != "Seleccione tipo de papel" &&
                settings.size != "Seleccione tamaño" &&
                settings.grams != "Seleccione gramaje" &&
                this.validateAsesoria();
            }else{
                toReturn = settings.paper != "Seleccione tipo de papel" &&
                settings.size != "Seleccione tamaño" &&
                settings.grams != "Seleccione gramaje";
            }

            if(!toReturn){
                this.errorSettings = true;
            }else{
                this.errorSettings = false;
            }
        }else if(settings.type == 'laser'){
            if(this.document.format != 'pdf'){
                toReturn = settings.color != "Seleccione color" &&
                settings.paper != "Seleccione material" &&
                this.validateAsesoria();
            }else{
                toReturn = settings.color != "Seleccione color" &&
                settings.paper != "Seleccione material";
            }

            if(!toReturn){
                this.errorSettings = true;
            }else{
                this.errorSettings = false;
            }
        }else if(settings.type == 'vinilo'){
            if(this.document.format != 'pdf'){
                toReturn = settings.color != "Seleccione color" &&
                settings.paper != "Seleccione acabado" &&
                this.validateAsesoria();
            }else{
                toReturn = settings.color != "Seleccione color" &&
                settings.paper != "Seleccione acabado";
            }

            if(!toReturn){
                this.errorSettings = true;
            }else{
                this.errorSettings = false;
            }
        }

        return toReturn;
    }

    saveSettings() {
        console.log(this.document);
        if(this.document.printSettings.tiro){
            var pages = parseInt(this.document.filePages);
            var copies = this.document.printSettings.numCopies;
            this.document.sheets = Math.ceil(pages/2)*copies;
        }else{
            var copies = this.document.printSettings.numCopies;
            this.document.sheets = this.document.filePages*copies;
        }
        this.document.verificado = true;
        this.settings.emit('settings-saved');
        if (!$('#settings-div').hasClass('open')) {
            $('#settings-div').fadeIn('slow');
            $('#settings-div').addClass('open');
        }
    }

    deleteDocument() {
        if ($('#settings-div').hasClass('open')) {
            $('#settings-div').fadeOut('slow');
            $('#settings-div').removeClass('open');
        }
        this.settings.emit('delete-document');
    }

    updateRespaldo(respaldo) {
        if (respaldo === 'foamboard') {
            delete this.document.printSettings.additionalFinishes.respaldoCarton;
            this.document.printSettings.additionalFinishes.respaldoFoamboard = true;
        } else {
            delete this.document.printSettings.additionalFinishes.respaldoFoamboard;
            this.document.printSettings.additionalFinishes.respaldoCarton = true;
        }
    }

    openFile(src){
        window.open('/uploads/'+src);
    }
}
