/**
 * Created on 23-05-2017.
 */

import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { ROUTER_DIRECTIVES, Router } from "@angular/router";
import { StepsMenuComponent } from "./steps-menu.component";
import { FlowControlService } from "../services/flow-control.service";
import { Video } from "../classes/video";
import { DataService } from "../services/data.service";
import { TruncatePipe } from "../shared/truncate.pipe";
import { SocketService } from "../services/socket.service";
import { LeftPanelComponent } from "./left-panel.component";
import { VideosFrontComponent } from "./videos-front.component";

declare var CryptoJS: any;
declare var NProgress: any;

@Component({
    moduleId: module.id,
    selector: 'corporate',
    templateUrl: '../templates/corporate.template.html',
    directives: [ROUTER_DIRECTIVES, StepsMenuComponent, LeftPanelComponent, VideosFrontComponent],
    pipes: [TruncatePipe]
})

export class CorporateComponent implements OnInit {

    constructor(private auth: AuthService, private router: Router, private flowControlService: FlowControlService,
        private dataService: DataService, private socketService: SocketService) {
        
    }

    ngOnInit() {
        
    }

}
