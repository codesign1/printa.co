///<reference path="../../typings/index.d.ts"/>

// The usual bootstrapping imports
import { bootstrap }      from '@angular/platform-browser-dynamic';

import { AppComponent }         from './components/app.component';
import { appRouterProviders }   from './app.routes';
import {HTTP_PROVIDERS} from "@angular/http";

bootstrap(AppComponent, [
    appRouterProviders,
    HTTP_PROVIDERS
]);
