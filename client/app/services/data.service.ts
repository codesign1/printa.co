import {Order} from "../classes/order";
import {Injectable, OnChanges} from "@angular/core";
/**
 * Created by jsrolon on 10-08-2016.
 */

@Injectable()
export class DataService {
    order: Order;
    userProfile: any;

    orderDataValid: boolean;

    orderId : string;

    constructor() {
        this.orderDataValid = false;
    }

    saveOrder(order: Order) {
        this.order = order;
        localStorage.setItem('order', JSON.stringify(order));
    }

    getOrder() {
        let savedOrder = JSON.parse(localStorage.getItem('order')) as Order;
        if (savedOrder) {
            this.order = savedOrder;
        }
        return this.order;
    }

    clearOrder() {
        this.order = null;
        localStorage.removeItem('order');
    }

    validOrderData() {
        return this.order.owner.name && this.order.owner.email && this.order.owner.phone_number
            && this.order.deliveryType && this.order.deliveryLocation;
    }

    cleanEverythingUp(removeBoolean) {
        // TODO: this code is kinda duplicated in the app component, when the whole window is closed
        if(!localStorage.getItem('saveProgress')) {
            this.clearOrder();
            localStorage.removeItem('order');
            localStorage.removeItem('hasWalkedThrough');
        }
        if(removeBoolean) {
            localStorage.removeItem('saveProgress');
        }
    }
}
