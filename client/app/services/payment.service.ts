/**
 * Created by jsrolon on 31-08-2016.
 */

import {Injectable, OnInit} from "@angular/core";
import {AuthService} from "./auth.service";
import {DataService} from "./data.service";
import {Http} from "@angular/http";
import {SocketService} from "./socket.service";

declare var $: any;

@Injectable()
export class PaymentService {
    constructor(private socketService: SocketService) {
    }

    attemptPayment(paymentType: string, orderId: string) {
        $('#payment-info-form').submit();
    }
}
