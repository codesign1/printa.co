/**
 * Created by jsrolon on 16-08-2016.
 */

import {Injectable} from "@angular/core";
import {DataService} from "./data.service";

@Injectable()
export class FlowControlService {

    allDocumentsWithSettings : boolean;
    hasWalkedThrough : boolean[];

    constructor(private dataService : DataService) {
        var localHasWalkedThrough = JSON.parse(localStorage.getItem('hasWalkedThrough'));
        if(localHasWalkedThrough) {
            this.hasWalkedThrough = localHasWalkedThrough;
        } else {
            this.hasWalkedThrough = [false, false, false, false, false];
        }
        localStorage.setItem('hasWalkedThrough', JSON.stringify(this.hasWalkedThrough));
    }

    // this one is read only
    isAllowedIn(screen : number) {
        var isAllowed = true;
        for(var i = 0; i < screen; i++) {
            isAllowed = isAllowed && this.hasWalkedThrough[i];
        }
        return isAllowed;
    }

    hasFinished(screen : number) {
        this.hasWalkedThrough[screen] = true;
        localStorage.setItem('hasWalkedThrough', JSON.stringify(this.hasWalkedThrough));
    }

    forbid(screen: number) {
        for(var i = screen; i < this.hasWalkedThrough.length; i++) {
            this.hasWalkedThrough[i] = false;
        }
        localStorage.setItem('hasWalkedThrough', JSON.stringify(this.hasWalkedThrough));
    }

    clearPriorTo(screen: number) {
        for(var i = 0; i < screen; i++) {
            this.hasWalkedThrough[i] = false;
        }
        localStorage.setItem('hasWalkedThrough', JSON.stringify(this.hasWalkedThrough));
    }
}
