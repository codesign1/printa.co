/**
 * Created by jsrolon on 09-08-2016.
 */

import {Injectable, EventEmitter}      from '@angular/core';
import {tokenNotExpired} from 'angular2-jwt';
import {Router} from "@angular/router";
import {SocketService} from "./socket.service";
import {DataService} from "./data.service";

// Avoid name not found warnings
declare var Auth0Lock: any;

@Injectable()
export class AuthService {
    // Configure Auth0
    lock = new Auth0Lock('jTzgUUrI7ZfvIK67HWtYN3h2Ba3glipZ', 'estudiocodesign.auth0.com', {
        language: 'es',
        languageDictionary: {
            title: '¡Bienvenido a Printa!'
        },
        auth: {
            redirectUrl: window.location.origin,
            responseType: 'token'
        },
        theme: {
            logo: 'img/logo_horizontal.svg',
        }
    });

    //Store profile object in auth class
    userProfile: any;
    userProfileChange : EventEmitter<any> = new EventEmitter();

    constructor(private router: Router) {
        // Set userProfile attribute of already saved profile
        this.userProfile = JSON.parse(localStorage.getItem('profile'));

        // alert the server of the new connection
        this.alertServerOfNewConnection();

        // Add callback for the Lock `authenticated` event, which means that the user logged in
        this.lock.on('hide', function() {
            localStorage.removeItem('redirect_url');
            if(this.router.url !== '' && this.router.url !== '/home') {
                this.router.navigate(['']);
            }
        }.bind(this));

        this.lock.on("authenticated", (authResult) => {
            localStorage.setItem('id_token', authResult.idToken);

            // Fetch profile information
            this.lock.getProfile(authResult.idToken, (error, profile) => {
                if (error) {
                    // Handle error
                    alert(error);
                    return;
                }

                localStorage.setItem('profile', JSON.stringify(profile));
                this.userProfile = profile;
                this.userProfileChange.emit(this.userProfile);

                // Redirect to the saved URL, if present.
                var redirectUrl: string = localStorage.getItem('redirect_url');
                if (redirectUrl != undefined) { // by default, redirect to the saved url
                    this.router.navigate([redirectUrl]);
                    localStorage.removeItem('redirect_url');
                } else if(redirectUrl == undefined && this.isAdmin()) {
                    // if there's no saved url but user is admin, send to dashboard
                    this.router.navigate(['/dashboard']);
                }
            });
        });
    };

    public login() {
        // Call the show method to display the widget.
        if(!this.authenticated()) {
            // if(this.router.url !== '/' || this.router.url !== '/home') {
            //     localStorage.setItem('redirect_url', this.router.url);
            // }
            this.lock.show();
        }
    };

    public authenticated() {
        // Check if there's an unexpired JWT
        // This searches for an item in localStorage with key == 'id_token'
        return tokenNotExpired();
    };

    public logout() {
        // Remove token and profile from localStorage
        if(this.authenticated()) {
            localStorage.removeItem('id_token');
            localStorage.removeItem('profile');
            this.userProfile = undefined;
            this.router.navigateByUrl('');
        }
    };

    public isAdmin() {
        let admin = this.userProfile && this.userProfile.app_metadata
            && this.userProfile.app_metadata.roles
            && this.userProfile.app_metadata.roles.indexOf('admin') > -1;
        return admin;
    }

    alertServerOfNewConnection() {
        if(!this.userProfile) { // we need to get the user profile from somewhere

        }
    }
}
