/**
 * Created by jsrolon on 09-08-2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var angular2_jwt_1 = require('angular2-jwt');
var AuthService = (function () {
    function AuthService(router, socketService) {
        var _this = this;
        this.router = router;
        this.socketService = socketService;
        // Configure Auth0
        this.lock = new Auth0Lock('jTzgUUrI7ZfvIK67HWtYN3h2Ba3glipZ', 'estudiocodesign.auth0.com', {
            language: 'es',
            languageDictionary: {
                title: '¡Bienvenido a Printa!'
            },
            logo: ''
        });
        // Set userProfile attribute of already saved profile
        this.userProfile = JSON.parse(localStorage.getItem('profile'));
        // Add callback for the Lock `authenticated` event, which means that the user logged in
        this.lock.on("authenticated", function (authResult) {
            localStorage.setItem('id_token', authResult.idToken);
            // Fetch profile information
            _this.lock.getProfile(authResult.idToken, function (error, profile) {
                if (error) {
                    // Handle error
                    alert(error);
                    return;
                }
                localStorage.setItem('profile', JSON.stringify(profile));
                _this.userProfile = profile;
                // we'll check if the logged in user is an admin, and if so, we tell the server that it is so
                // we send the user profile too
                if (_this.isAdmin()) {
                    _this.socketService.socket.emit('admin-login', _this.userProfile.email);
                    // by default, admins go to the dashboard on login
                    _this.router.navigate(['/dashboard']);
                }
                // Redirect to the saved URL, if present.
                var redirectUrl = localStorage.getItem('redirect_url');
                if (redirectUrl != undefined) {
                    _this.router.navigate([redirectUrl]);
                    localStorage.removeItem('redirect_url');
                }
            });
        });
    }
    ;
    AuthService.prototype.login = function () {
        // Call the show method to display the widget.
        this.lock.show();
    };
    ;
    AuthService.prototype.authenticated = function () {
        // Check if there's an unexpired JWT
        // This searches for an item in localStorage with key == 'id_token'
        return angular2_jwt_1.tokenNotExpired();
    };
    ;
    AuthService.prototype.logout = function () {
        // Remove token and profile from localStorage
        localStorage.removeItem('id_token');
        localStorage.removeItem('profile');
        this.userProfile = undefined;
        this.router.navigateByUrl('');
    };
    ;
    AuthService.prototype.isAdmin = function () {
        var admin = this.userProfile && this.userProfile.app_metadata
            && this.userProfile.app_metadata.roles
            && this.userProfile.app_metadata.roles.indexOf('admin') > -1;
        return admin;
    };
    AuthService = __decorate([
        core_1.Injectable()
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
