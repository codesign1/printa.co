/**
 * Created by jsrolon on 19-09-2016.
 */

import {Injectable, OnInit} from "@angular/core";

declare var $: any;

@Injectable()
export class PrintOptionsService {
    constructor() {
    }

    getPrintOptions(): any {
        return {
            Bond: {
                '75gr': {
                    sizes: ['Rollo (Ancho máx 70cm)', 'Pliego', 'Medio pliego', '1/4 de pliego', '1/8 de pliego',
                        'Doble carta', 'Oficio', 'Carta']
                }
            },
            Opalina: {
                '180gr': {
                    sizes: ['Pliego', 'Medio pliego', '1/4 de pliego']
                },
                '90gr': {
                    sizes: ['1/8 de pliego', 'Doble carta', 'Carta']
                }
            },
            Durex: {
                '180gr': {
                    sizes: ['Pliego', 'Medio pliego']
                }
            },
            Pergamino: {
                '180gr': {
                    sizes: ['Pliego', 'Medio pliego']
                },
                '85gr': {
                    sizes: ['1/4 de pliego']
                }
            },
            Propalcote: {
                '216gr': {
                    sizes: ['Doble carta', 'Carta']
                },
                '240gr': {
                    sizes: ['Rollo (Ancho máx 141cm)', 'Pliego', 'Medio pliego']
                }
            },
            Propalmate: {
                '240gr': {
                    sizes: ['Pliego', 'Medio pliego']
                }
            },
            Banner: {
                '180gr': {
                    sizes: ['Rollo'],
                    finishes: ['Brillante', 'Mate']
                }
            },
            Vinilo: {
                '180gr': {
                    'Transparente': {
                        sizes: ['Rollo (Ancho máx 127cm)'],
                        finishes: ['Brillante', 'Mate']
                    },
                    'Blanco': {
                        sizes: ['Rollo (Ancho máx 127cm)'],
                        finishes: ['Brillante', 'Mate', 'Microperforado']
                    },
                    'Negro': {
                        sizes: ['Rollo (Ancho máx 127cm)'],
                        finishes: ['Brillante', 'Mate', 'Microperforado']
                    }
                }
            }
        };
    }

    getPrintOptionsTwo(): any {
        return {
            Bond: {
                'Rollo (Ancho máx 70cm)':{
                    grams:['75gr']
                },
                'Pliego':{
                    grams:['75gr']
                },
                'Medio pliego':{
                    grams:['75gr']
                },
                '1/4 de pliego':{
                    grams:['75gr']
                },
                '1/8 de pliego':{
                    grams:['75gr']
                },
                'Doble carta':{
                    grams:['75gr']
                },
                'Oficio':{
                    grams:['75gr']
                },
                'Carta':{
                    grams:['75gr']
                }
            },
            Opalina: {
                'Pliego':{
                    grams:['180gr']
                },
                'Medio pliego':{
                    grams:['180gr']
                },
                '1/4 de pliego':{
                    grams:['180gr']
                },
                '1/8 de pliego':{
                    grams:['90gr', '180gr'] /*prueba*/
                },
                'Doble carta':{
                    grams:['90gr']
                },
                'Carta':{
                    grams:['90gr']
                }
            },
            Durex: {
                'Pliego':{
                    grams: ['180gr']
                },
                'Medio pliego' :{
                    grams: ['180gr']
                }
            },
            Pergamino: {
                'Pliego' :{
                    grams: ['180gr']
                },
                'Medio pliego' :{
                    grams: ['180gr']
                },
                '1/4 de pliego' :{
                    grams: ['85gr']
                }
            },
            Propalcote: {
                'Doble carta':{
                    grams: ['216gr']
                },
                'Carta':{
                    grams: ['216gr']
                },
                'Rollo (Ancho máx 141cm)':{
                    grams: ['240gr']
                },
                'Pliego':{
                    grams: ['240gr']
                },
                'Medio pliego':{
                    grams: ['240gr']
                }
            },
            Propalmate: {
                'Pliego':{
                    grams: ['240gr']
                },
                'Medio pliego':{
                    grams: ['240gr']
                }
            },
            Banner: {
                'Rollo':{
                    grams: ['180gr'],
                    finishes: ['Brillante', 'Mate']
                }
            },
            Vinilo: {
                'Rollo (Ancho máx 127cm)': {
                    'Transparente': {
                        grams: ['180gr'],
                        finishes: ['Brillante', 'Mate']
                    },
                    'Blanco': {
                        grams: ['180gr'],
                        finishes: ['Brillante', 'Mate', 'Microperforado']
                    },
                    'Negro': {
                        grams: ['180gr'],
                        finishes: ['Brillante', 'Mate', 'Microperforado']
                    }
                }

            }
        };
    }

    getVinylOptions(): any {
        return {
            'Transparente': {
                finishes: ['Brillante', 'Mate']
            },
            'Blanco': {
                finishes: ['Brillante', 'Mate']
            },
            'Negro': {
                finishes: ['Brillante', 'Mate']
            }
        }
    }

    getCutOptions(): any {
        return {
            'Cartón kraft': {
                max: 'Pliego',
            },
            'Cartón industrial': {
                max: 'Pliego',
            },
            'Cartón microcorrugado': {
                max: 'Pliego',
                thickness: '1.5mm'
            },
            'Cartón corrugado': {
                max: 'Pliego',
                thickness: '3mm'
            },
            'Cartón paja blanco': {
                max: 'Pliego',
            },
            'Basic': {
                max: 'Pliego',
            },
            'MDF': {
                max: 'Pliego',
                thickness: '3mm'
            },
            'Acrílico': {
                max: 'Pliego',
                thickness: '2mm',
                colors: ['Transparente', 'Blanco', 'Negro', 'Rojo traslúcido']
            },
            'Balso': {
                max: 'Lámina de 15cm x 100cm',
                thickness: '2mm'
            },
            'Spectra': {
                max: 'Pliego',
                thickness: '1mm'
            }
        }
    }
}
