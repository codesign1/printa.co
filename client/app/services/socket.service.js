/**
 * Created by jsrolon on 09-08-2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var io = require('socket.io-client');
var core_1 = require("@angular/core");
var SocketService = (function () {
    function SocketService() {
        this.socket = io('http://localhost:8080');
    }
    SocketService = __decorate([
        core_1.Injectable()
    ], SocketService);
    return SocketService;
}());
exports.SocketService = SocketService;
