/**
 * Created by jsrolon on 28-11-2016.
 */

import {Injectable, OnInit} from "@angular/core";
import {AuthService} from "./auth.service";
import {DataService} from "./data.service";
import {Http} from "@angular/http";
import {SocketService} from "./socket.service";

declare var $: any;

@Injectable()
export class ColorService {
    colored : boolean;
    magenta : boolean;
    yellow  : boolean;
    count : number;
    toggle: boolean;

    logo : string;
    logo2 : string;

    constructor() {
        this.colored = false;
        this.magenta = false;
        this.yellow = false;
        this.count = 0;
        this.toggle = false;

        this.logo = 'img/logo.svg';
        this.logo2 = 'img/PrintaBlackLogo.svg'
    }

    toggleColor() {
        if(this.count == 0 && this.yellow == false){
            this.colored = !this.colored;
            this.count++;
            this.toggle = !this.toggle;
        }
        else if(this.count == 1 && !this.colored){
            this.magenta = !this.magenta;
            this.count++;
            this.toggle = !this.toggle;
        }
        else if(this.magenta == false && this.count == 2){
            this.yellow = !this.yellow;
            this.count = 0;
            this.toggle = !this.toggle;
        }
        else{
            this.colored = false;
            this.magenta = false;
            this.yellow = false;
            this.toggle = false;
        }

        if(this.colored){
            this.logo = 'img/logored.svg';
            this.logo2 = 'img/PrintaCianLogo.svg';
        }else if(this.magenta){
            this.logo = 'img/logomagenta.svg';
            this.logo2 = 'img/PrintaMagentaLogo.svg';
        }else if(this.yellow){
            this.logo = 'img/logoyellow.svg';
            this.logo2 = 'img/PrintaYellowLogo.svg';
        }else{
            this.logo = 'img/logo.svg';
            this.logo2 = 'img/PrintaBlackLogo.svg';
        }
    }
}
