/**
 * Created by jsrolon on 09-08-2016.
 */

import * as io from 'socket.io-client';
import {Injectable, OnInit} from "@angular/core";
import {AuthService} from "./auth.service";
import {DataService} from "./data.service";
import {Router} from "@angular/router";

@Injectable()
export class SocketService implements OnInit {
    socket: any;

    constructor(private auth: AuthService, private dataService: DataService, private router: Router) {
        this.socket = io();

        this.socket.on('error', function (error) {
            // alert('Hay un problema con la conexión en tiempo real al servidor. Si eso no funciona, '
            //     + 'es posible que el servidor haya caído.');
            console.error(error);
        });

        // register the event listener for the auth service
        let emitNewConnection = userProfile => {
            this.socket.emit('new-connection', userProfile);
        };

        if (this.auth.userProfile) {
            emitNewConnection(this.auth.userProfile);
        } else {
            this.auth.userProfileChange.subscribe(userProfile => {
                emitNewConnection(userProfile);
            });
        }

        // register the event listener
        this.socket.on('order-status-change', function (msg) {
            this.dataService.saveOrder(msg);
        }.bind(this));

        this.socket.on('order-deleted', function (orderId) {
            if (this.router.url !== '/mis-pedidos') {
                if (localStorage.getItem('order')
                    && JSON.parse(localStorage.getItem('order')).id === orderId
                    && !this.auth.isAdmin()) {
                    alert('La orden con ID ' + orderId + ' ha sido cancelada.');
                    this.router.navigate(['/mis-pedidos']);
                    this.dataService.cleanEverythingUp(true);
                }
            }
        }.bind(this));
    }

    ngOnInit() {
    }
}
