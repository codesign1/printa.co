/**
 * Created by jsrolon on 18-08-2016.
 */

export class PrintSettings {
    type : string;
    color : string;
    size : string;
    paper : string;
    numCopies : number;
    tiro : boolean;
    additionals : any;
    grams : string;
    finish : string;
    additionalFinishes : any;
    userInstructions : string;

    constructor() {
        this.type = 'normal';
        this.color = 'Full color';
        // this.size = 'Rollo (Ancho máx 127cm)';
        this.paper = 'Bond';
        this.numCopies = 1;
        this.additionals = {};
        this.additionalFinishes = {
            redBull: 0,
            asesoria_escala: false,
            asesoria_color: false,
            encuadernado: false,
            anillado: false
        };
    }
}
