/**
 * Created by jsrolon on 29-11-2016.
 */

export class NormalProduct {
    material : string;
    tamano: string;
    gramaje : number;
    cantidad : number;
    cantidad_alerta : number;
    habilitado : boolean;
    valor_unitario : number;
    id : number;
}
