import {Document} from "./document";
/**
 * Created by jsrolon on 09-08-2016.
 */

export class Order {
    id : string;
    documents : Document[];
    totalPrice : number = 0;
    owner : any;
    user : string;
    deliveryType : string;
    deliveryLocation : string;
    deliveryTime : string;
    paymentType : string;
    paid : boolean;
    status : string;
    quoteAccepted : boolean;
    shippingCost : number;
    taxRate : number;

    creation_date : date;
    creation_time : time;
    creation_month : string;
    redbullprice : number = 0;
    redbullcant : number = 0;


    promoCode : string;
    discountAmount : number;
    discountType : string;
    paymentTotal : number;

    flowStatus : string;

    quoteSent : boolean;

    specialPromotions : any;

    constructor() {
        var date = new Date();

        var d = new Date(),
            msSinceMidnight = d.getTime() - d.setHours(0,0,0,0);

        this.id = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' +
            date.getDate() + '-' + Math.ceil(msSinceMidnight / 100);
        this.documents = [];
        this.quoteAccepted = false;
        this.status = 'cotizar';
        this.taxRate = 0.19; /*cambiar a IVA 2017*/ 
        this.shippingCost = 0;
        this.deliveryType = 'casa';
        this.quoteSent = false;

        this.specialPromotions = {};
    }
}
