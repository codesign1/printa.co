import {Document} from "./document";
/**
 * Created on 15-05-2017.
 */

export class Video {
    id : string;
    title : string;
    url : string;
    show : boolean;
    position : number;
    videocode : string;

    thumbnail : string;

    constructor() {
    }
}
