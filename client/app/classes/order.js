"use strict";
/**
 * Created by jsrolon on 09-08-2016.
 */
var Order = (function () {
    function Order() {
        this.totalPrice = 0;
        var date = new Date();
        this.id = date.getFullYear() + '/' + date.getMonth() + '/' +
            date.getDate() + '-' + 'T' + Math.floor(Math.random() * 9999) + 1000;
        this.documents = [];
    }
    return Order;
}());
exports.Order = Order;
