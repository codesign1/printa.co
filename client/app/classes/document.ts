import {PrintSettings} from "./print-settings";
/**
 * Created by jsrolon on 09-08-2016.
 */

export class Document {
    id;
    name;
    format;
    server_filename;
    price;
    printSettings : PrintSettings;
    fileSize;
    filePages;
    sheets;
    verificado : boolean;

    constructor() {
        this.price = 0;
        this.printSettings = new PrintSettings();
        this.verificado = false;
    }
}
