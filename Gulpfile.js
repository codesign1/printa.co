/**
 * Printa.co Gulpfile
 * Created by: jsrolon
 * 03-08-2016
 */

'use strict';

// ---------------------------------
// Functions
// ---------------------------------
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const rename = require('gulp-rename');

// watches and reloads automatically etc
const nodemon = require('gulp-nodemon');

// run gulp tasks in sequence
const runSequence = require('run-sequence');

// typescript stuff
const del = require("del");
const tsc = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const tsProject = tsc.createProject("config/tsconfig.json");

// stylesheet stuff
const sass = require('gulp-sass');

// will only process files that changed since the last time it was done
const changed = require('gulp-changed');
const gulpif = require('gulp-if');
const conflict = require('gulp-conflict');

// deploy stuff
const browserify = require('gulp-browserify');
const uglify = require('gulp-uglify');
const htmlReplace = require('gulp-html-replace');
const pump = require('pump');

// ---------------------------------
// Constants
// ---------------------------------

// delay in ms 
const BROWSER_SYNC_RELOAD_DELAY = 500;

const paths = {
    typescript: 'client/app/**/*.ts',
    public: 'public',
    buildFiles: ['public/*.js', 'public/*.js.map'],
    sass: 'client/assets/sass/**/*.scss',
    templates: 'client/app/templates/*.html'
};

// ---------------------------------
// Construction tasks
// ---------------------------------
gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: 'localhost:8080',
        port: 3000,
        ghostMode: false,
        browser: 'google chrome'
    });
});

// run the express dev server using nodemon
gulp.task('server', function (cb) {
    var called = false;
    return nodemon({
        verbose: true,
        script: 'server/app.js',

        // watch core files that need the server to be restarted
        watch: ['server/app.js']
    })
    .on('start', function () {
        // ensure start only got called once
        if (!called) {
            called = true;
            cb();
        }
    })
    .on('restart', function () {
        // reload connected browsers after a slight delay
        setTimeout(function () {
            browserSync.reload({
                stream: false
            });
        }, BROWSER_SYNC_RELOAD_DELAY);
    });
});

// set up the watch tasks
gulp.task('watch', function () {
    gulp.watch([paths.typescript], ['compile', browserSync.reload]);
    gulp.watch([paths.sass], ['sass:watch']);
    gulp.watch([paths.templates], ['templates', browserSync.reload]);
});

// clean up the build dir
gulp.task('clean', function (cb) {
    return del(paths.buildFiles, cb);
});

// compile TypeScript sources and create sourcemaps in build directory
gulp.task('compile', ['clean'], function () {
    var tsResult = gulp.src(paths.typescript)
    .pipe(sourcemaps.init())
    .pipe(tsc(tsProject));
    return tsResult.js
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.public));
});

// move the templates to a place where they can be collected by the AJAX requests
gulp.task('templates', function () {
    del('public/templates/*.html');
    return gulp.src(paths.templates)
    .pipe(gulp.dest(paths.public + '/templates'))
});

// add the css tasks for assets
gulp.task('sass', function () {
    return gulp.src(paths.sass)
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('public/'));
});

gulp.task('sass:watch', ['sass'], function () {
    return gulp.src('public/*.css')
    .pipe(browserSync.stream() );
});

gulp.task('index:default', function () {
    gulp.src('client/assets/index.html')
    .pipe(gulp.dest('public/'));
});

// ---------------------------------
// deploy-intended tasks
// ---------------------------------
gulp.task('browserify', function () {
    return gulp.src('public/main.js')
    .pipe(browserify())
    .pipe(rename('bundle.js'))
    .pipe(gulp.dest('public/'));
});

gulp.task('uglify', ['browserify'], function () {
    return pump([
            gulp.src('public/bundle.js'),
            uglify(),
            rename('printa.min.js'),
            gulp.dest('public/')
        ]
    );
});

gulp.task('index:deploy', function () {
    gulp.src('client/assets/index.html')
    .pipe(htmlReplace({
        js: 'printa.min.js'
    }))
    .pipe(gulp.dest('public/'));
});

// ---------------------------------
// CLI-intended tasks
// ---------------------------------
gulp.task('default', function () {
    runSequence('compile', 'templates', 'sass', 'index:default', 'server', 'browser-sync', 'watch');
});

gulp.task('deploy', function () {
    runSequence('compile', 'uglify', 'templates', 'sass', 'index:deploy');
});
