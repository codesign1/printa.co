$(document).on('click', 'html', function (event) {
    if ($('#menu').hasClass('open')) {
        $("#menu").animate({
            left: "-=320"
        }, 800);
        $("header").animate({
            left: "-=320"
        }, 800);
        $(".maincontent").animate({
            left: "-=320"
        }, 800);
        $("footer").animate({
            left: "-=320"
        }, 800);
        $('#iconmenu').addClass('icon-hamburger');
        $('#iconmenu').removeClass('icon-times');
        $('#menu').removeClass('open');
    }
    if ($('#detallesdiv').hasClass('open')) {
        $('#detallesdiv').fadeOut('slow');
        $('#detallesdiv').removeClass('open');
    }
    if ($('#archivosdiv').hasClass('open')) {
        $('#archivosdiv').fadeOut('slow');
        $('#archivosdiv').removeClass('open');
    }
    if ($('#settings-div').hasClass('open')) {
        $('#settings-div').fadeOut('slow');
        $('#settings-div').removeClass('open');
    }
});

$(document).on('click', '#menu a, #menu-log-out', function (event) {
    if ($('#menu').hasClass('open')) {
        $("#menu").animate({
            left: "-=320"
        }, 800);
        $("header").animate({
            left: "-=320"
        }, 800);
        $(".maincontent").animate({
            left: "-=320"
        }, 800);
        $("footer").animate({
            left: "-=320"
        }, 800);
        $('#iconmenu').addClass('icon-hamburger');
        $('#iconmenu').removeClass('icon-times');
        $('#menu').removeClass('open');
    }
});

$(document).on('click', '#menu, .caracteristicas', function (event) {
    event.stopPropagation();
});

$(document).on('click', '#iconmenu', function (event) {
    event.stopPropagation();
    if (!$('#menu').hasClass('open')) {
        $("#menu").animate({
            left: "+=320"
        }, 800);
        $("header").animate({
            left: "+=320"
        }, 800);
        $(".maincontent").animate({
            left: "+=320"
        }, 800);
        $("footer").animate({
            left: "+=320"
        }, 800);
        $('#iconmenu').removeClass('icon-hamburger');
        $('#iconmenu').addClass('icon-times');
        $('#menu').addClass('open');
    } else {
        $("#menu").animate({
            left: "-=320"
        }, 800);
        $("header").animate({
            left: "-=320"
        }, 800);
        $(".maincontent").animate({
            left: "-=320"
        }, 800);
        $("footer").animate({
            left: "-=320"
        }, 800);
        $('#iconmenu').addClass('icon-hamburger');
        $('#iconmenu').removeClass('icon-times');
        $('#menu').removeClass('open');
    }
});

$(document).on('click', '.openarchivos', function (event) {
    event.stopPropagation();
    if (!$('#archivosdiv').hasClass('open')) {
        $('#archivosdiv').fadeIn('slow');
        $('#archivosdiv').addClass('open');
    }
});

$(document).on('click', '.opendetalles', function (event) {
    event.stopPropagation();
    if (!$('#detallesdiv').hasClass('open')) {
        $('#detallesdiv').fadeIn('slow');
        $('#detallesdiv').addClass('open');
    }
});

$(document).on('click', '.openVacaciones', function (event) {
    event.stopPropagation();
    if (!$('#vacaciones').hasClass('open')) {
        $('#vacaciones').fadeIn('slow');
        $('#vacaciones').addClass('open');
    }
});

$(document).on('click', '#save-document-price', function (event) {
    event.stopPropagation();
    if ($('#archivosdiv').hasClass('open')) {
        $('#archivosdiv').fadeOut('slow');
        $('#archivosdiv').removeClass('open');
    }
});

$(document).on('click', '.closearchivosdiv', function (event) {
    event.stopPropagation();
    if ($('#archivosdiv').hasClass('open')) {
        $('#archivosdiv').fadeOut('slow');
        $('#archivosdiv').removeClass('open');
    }
});

$(document).on('click', '.closeVacation', function (event) {
    event.stopPropagation();
    if($('#vacaciones').hasClass('open')){
        $('#vacaciones').fadeOut('slow');
        $('#vacaciones').removeClass('open');
    }
})

$(document).on('click', '#save-and-send-quote', function (event) {
    event.stopPropagation();
    if ($('#archivosdiv').hasClass('open')) {
        $('#archivosdiv').fadeOut('slow');
        $('#archivosdiv').removeClass('open');
    }
});

$(document).on('click', '#save-shipping-price', function (event) {
    event.stopPropagation();
    if ($('#detallesdiv').hasClass('open')) {
        $('#detallesdiv').fadeOut('slow');
        $('#detallesdiv').removeClass('open');
    }
});

$(document).on('click', '.closedetallesdiv', function (event) {
    event.stopPropagation();
    if ($('#detallesdiv').hasClass('open')) {
        $('#detallesdiv').fadeOut('slow');
        $('#detallesdiv').removeClass('open');
    }
});

$(document).on('click', '#save-settings-button', function (event) {
    event.stopPropagation();
    if ($('#settings-div').hasClass('open')) {
        $('#settings-div').fadeOut('slow');
        $('#settings-div').removeClass('open');
    }
});

$(document).on('click', '.document-in-grid', function (event) {
    event.stopPropagation();
    if (!$('#settings-div').hasClass('open')) {
        $('#settings-div').fadeIn('slow');
        $('#settings-div').addClass('open');
    }
});

$(window).load(function() {

var map;

  function initMap() {
    
    map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 13,
      center: {
        lat: 4.6597289,
        lng: -74.0675389
      },
      mapTypeId: 'terrain',
      styles: [{
          "elementType": "geometry",
          "stylers": [{
            "color": "#f5f5f5"
          }]
        },
        {
          "elementType": "labels.icon",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#616161"
          }]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [{
            "color": "#f5f5f5"
          }]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#bdbdbd"
          }]
        },
        {
          "featureType": "poi",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [{
            "color": "#eeeeee"
          }]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#757575"
          }]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [{
            "color": "#e5e5e5"
          }]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#9e9e9e"
          }]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [{
            "color": "#ffffff"
          }]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#757575"
          }]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [{
            "color": "#dadada"
          }]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#616161"
          }]
        },
        {
          "featureType": "road.local",
          "elementType": "labels",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#9e9e9e"
          }]
        },
        {
          "featureType": "transit",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry",
          "stylers": [{
            "color": "#e5e5e5"
          }]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [{
            "color": "#eeeeee"
          }]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [{
            "color": "#c9c9c9"
          }]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#9e9e9e"
          }]
        }
      ]
    });

    var cityCircle = new google.maps.Circle({
      strokeColor: '#000',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#000',
      fillOpacity: 0.35,
      map: map,
      center: {
        lat: 4.6597289,
        lng: -74.0675389
      },
      radius: 8000
    });
  }

  initMap();
});