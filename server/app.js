'use strict';

// set up the logger
const winston = require('winston');
var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            timestamp: function () {
                return new Date().toString();
            },
            formatter: function (options) {
                // Return string will be passed to logger.
                return options.timestamp() + ' :: ' + options.level.toUpperCase() + ' :: '
                    + (undefined !== options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
            }
        })
    ]
});
logger.info('Winston logger initialized');

const PRODUCTION = (process.env.PRINTA_PRODUCTION === 'true');

var APP_PORT = 8080;
var ALLOWED_ORIGIN = "http://localhost:3000";
var HOST_IP = '127.0.0.1';
if (PRODUCTION) {
    // add the file transport
    logger.configure({
        transports: [
            new (winston.transports.File)({
                timestamp: function () {
                    return new Date().toString();
                },
                formatter: function (options) {
                    // Return string will be passed to logger.
                    return options.timestamp() + ' :: ' + options.level.toUpperCase() + ' :: '
                        + (undefined !== options.message ? options.message : '') +
                        (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '' );
                },
                filename: '/var/log/printa.log'
            })
        ]
    });

    logger.info('Production mode on');
    APP_PORT = 80;
    ALLOWED_ORIGIN = "http://printadelivery.com";
    HOST_IP = '0.0.0.0';
}

// pure web server stuff
var express = require('express');
var path = require('path');
var app = express();
var compression = require('compression');

// file upload deps
var bodyParser = require("body-parser");
var multer = require("multer");
var fs = require('fs');

if (PRODUCTION) {
    var sslOpts = {
        key: fs.readFileSync('/etc/letsencrypt/live/printadelivery.com/privkey.pem'),
        cert: fs.readFileSync('/etc/letsencrypt/live/printadelivery.com/fullchain.pem')
    };
}

// email related stuff
var api_key = 'key-35f54002012f90dbee0e59421dd26e93';
var domain = 'mg.printadelivery.com';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

// socket etc
var http = require('http').Server(app);
var io;

// ssl
var https = require('https');
var forceSSL = require('express-force-ssl');

if (PRODUCTION) {
    var sslServer = https.createServer(sslOpts, app);
    sslServer.listen(443, function () {
        logger.info('https server started');
    });
    app.use(forceSSL);
    io = require('socket.io')(sslServer);
} else {
    // Define the port to run on
    io = require('socket.io')(http);
}

// listen everywhere, to force SSL
http.listen(APP_PORT, HOST_IP);

//database
var pg = require('pg');
var dbConfig = {
    user: 'printa',
    database: 'printa',
    password: 'RgKD}.pekjzA+.2^&T!q',
    // user: 'postgres',
    // database: 'printa',
    // password: 'root',
    host: 'localhost',
    max: 100,
    idleTimeoutMillis: 1000
};

var dbPool = new pg.Pool(dbConfig);

if (PRODUCTION) {
    app.use(compression());
}

// require requests for using with the http module


const UNIVERSAL_ADMIN = 'contacto@printadelivery.com';

app.use('/lib', express.static(path.join(__dirname, '../node_modules')));
app.use(express.static(path.join(__dirname, '../public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", ALLOWED_ORIGIN);
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var renderIndex = function (req, res) {
    res.sendFile(path.resolve(__dirname, '../public/index.html'));
};

app.get('/uploads/:name', function (req, res) {
    var filename = './server/uploads/' + req.params['name'];

    try {
        logger.info('trying to serve file ' + filename);
        var file = fs.createReadStream(filename);
        file.on('error', function (err) {
            console.log(err);
            res.redirect('/');
        });

        res.setHeader('Content-Type', 'application/pdf');
        file.pipe(res);
    } catch (e) {
        console.log(e);
    }
});

app.get('/*', renderIndex);

app.post("/upload", multer({dest: "./server/uploads/"}).single("file"), function (req, res) {
    logger.info('upload finished, starting sending file ' + req.file);
    res.send(req.file);
    res.end();
    logger.info('response end for file ' + req.file);
});

/**
 * Confirmation page and response page
 */
app.post("/api/confirmation", function (req, res) {
    if (req.body['response_message_pol'] === 'APPROVED') {
        // yay!
        var orderId = req.body['reference_sale'];

        // get the admin who has this order
        dbPool.query("SELECT * FROM assigned_orders WHERE \"order\"->>'id' = '" + orderId + "'")
        .then(function (res) {
            if (res.rows[0]) {
                var order = res.rows[0]['order'];
                var userEmail = res.rows[0]['order']['owner']['email'];

                // the transaction was approved!
                // change the order status to Printing, so that no matter what happens the user believes there's
                // something happening
                order.paid = true;
                order.status = 'imprimiendo';
                order.flowStatus = 'estado';
                order.quoteAccepted = true;

                sendPrintingEmail(userEmail);

                // save the order
                var query = "UPDATE assigned_orders SET \"order\" = '" + JSON.stringify(order)
                    + "' where \"order\"->>'id' = '" + orderId + "'";
                dbPool.query(query)
                .then(function (r) {
                    // send the new order to the admin
                    var admin = res.rows[0]['assigned_to'];
                    io.to(admins[admin].socketId).emit('payment-done', order);
                    io.to('admins').emit('payment-done', order);

                    // notify the user that the order has changed status to paid,
                    // because maybe the user is still waiting
                    // TODO: maybe the info can be sent without using so much bandwidth
                    io.to(users[userEmail]).emit('order-status-change', order);
                }).catch(function (err) {
                    logger.error(err);
                });
            }
        }).catch(function (err) {
            logger.error(err);
        });
    }
    res.send('OK')
});

/**
 * All socket.io connections etc logic
 */
var admins = [];
var leastOccupiedAdmin;

var users = [];

// code to retrieve all the admins from the auth0 db
var allAdminEmails = [];
var options = {
    hostname: 'estudiocodesign.auth0.com',
    port: 443,
    path: '/api/v2/users?q=app_metadata.roles%3A%22admin%22%20AND%20email%3A*printadelivery.com',
    method: 'GET',
    headers: {
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJZYXRMVXI5TjlRZkdVY0NtS0JRbG5qZTdGdTdtVzY4cCIsInNjb3BlcyI6eyJ1c2VycyI6eyJhY3Rpb25zIjpbInJlYWQiXX19LCJpYXQiOjE0NzU1MTAyMjUsImp0aSI6IjdmM2I1MzRhOWZkNWZkNmZkZDhhNmE5YmZjMTdjODU5In0.hf9mH0n2M8i-LnLolkoZqmtc9oPq2Xo0VeAweWLMV7I'
    }
};

var req = https.request(options, function (res) {
    res.on('data', function (d) {
        var adminProfiles = JSON.parse(d);
        var adminEmails = adminProfiles.map(function (curr) {
            return curr.email;
        });
        allAdminEmails = adminEmails;
    });
});
req.end();

req.on('error', function (e) {
    console.error(e);
});

io.on('connection', function (socket) {
    /**
     * user related events
     */
    socket.on('new-connection', function (userProfile) {
        logger.info('connection on ' + socket.id + ' for user ' + userProfile.email);
        if (userProfile) { // we have a new user connection, we care about this
            var admin = userProfile['app_metadata']
                && userProfile['app_metadata']['roles']
                && userProfile['app_metadata']['roles'].indexOf('admin') > -1;

            if (admin) {
                // we need to send them their assigned orders
                registerAdminConnection(userProfile.email);
                sendAssignedOrdersTo(userProfile.email, socket.id);
            } else {
                users[userProfile.email] = socket.id;
            }
        }
    });

    socket.on('admin-login', function (adminEmail) {
        registerAdminConnection(adminEmail);
    });

    socket.on('socket-confirmation', function (userEmail) {
        logger.info('received socket confirmation for user ' + userEmail + ' @socket ' + socket.id);
        users[userEmail] = socket.id;
    });

    socket.on('disconnect', function () {
        logger.info('user disconnected on socket ' + socket.id);
        // we need to delete the register that contains the socket, because
        // the user is not connected anymore
        // TODO: make this code more functional
        var index = -1;
        for (var i = 0; i < admins.length; i++) {
            if (admins[i].socketId === socket.id) {
                index = i;
                break;
            }
        }
        if (index !== -1) {
            admins.splice(index, 1);
        } else { // the disconnected user was not an admin
            for (var j = 0; j < users.length; j++) {
                if (users[j].socketId === socket.id) {
                    index = j;
                    break;
                }
            }
            users.splice(index, 1);
        }
    });

    /**
     * order-related events
     */
    // when a new order is received
    socket.on('new-order', function (msg) {
        // TODO: CHANGE THIS WHEN IS NEEDED
        leastOccupiedAdmin = UNIVERSAL_ADMIN;

        var order = msg.order;
        var userProfile = msg.userProfile;

        // make sure the user is on the correct socket
        users[userProfile.email] = socket.id;

        // we execute the changes regarding promo codes
        if (order.promoCode !== '') {
            // get the promo code characteristics
            var query = "SELECT * FROM promo_codes WHERE codigo = '" + order.promoCode + "'";
            dbPool.query(query)
            .then(function (res) {
                if (res.rows[0]) {
                    order.discountAmount = parseFloat(res.rows[0]['valor']);
                    order.discountType = res.rows[0]['tipo'];
                    order.paymentTotal = 0;
                    logger.info('discount info applied to the order');
                }

                var updateQuery = "UPDATE promo_codes SET cantidad = cantidad - 1 where codigo = '" + order.promoCode + "'";
                dbPool.query(updateQuery)
                .then(function (res) {
                    logger.info('discount code updated');
                }).catch(function (err) {
                    logger.error(err);
                })
            })
            .catch(function (err) {
                console.error(err);
            });
        }

        // check if the order with the given id is already in the dbPool
        dbPool.query("select * from assigned_orders where \"order\"->>'id' = '" + order.id + "'")
        .then(function (res) {
            if (!res.rows[0]) { // there's no record, so we save this order transactionally
                var creationDate = new Date();
                var creationDateString = creationDate.toISOString()
                var query = "INSERT INTO assigned_orders VALUES ('" + JSON.stringify(order) + "', "
                    + "'" + leastOccupiedAdmin + "', '" + creationDateString + "')";
                dbPool.query(query)
                .then(function (res) {
                    // send the order to the admin, only if it was successful
                    // todo: what if there are no admins currently available? dbPool?
                    // the line below was commented because the least occupied admin was getting stuff twice
                    // io.to(admins[leastOccupiedAdmin].socketId).emit('assigned-order', order);
                    io.to('admins').emit('assigned-order', order);

                    if (PRODUCTION) {
                        sendReceptionEmail(userProfile.email);
                    }

                    if (PRODUCTION) {
                        logger.info('admins are ' + allAdminEmails);
                        allAdminEmails.map(function (current) {
                            var string = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>' +
                                'La orden con código ' + order.id + ' se encuentra lista para la cotización.<br>' +
                                '<b>Nombre de contacto:</b> ' + order.owner.name + '<br>' +
                                '<b>Email de contacto:</b> ' + order.owner.email + '<br>' +
                                '<b>Teléfono de contacto:</b> ' + order.owner.phone_number + '<br>' +
                                '<b>Hora de creación:</b> ' + creationDate.getDate() + '/' + (creationDate.getMonth() + 1)
                                + '/' + creationDate.getFullYear() + ' ' + creationDate.getHours() + ':' + creationDate.getMinutes() + '</body></html>';
                            logger.info(string);
                            mailgun.messages().send({
                                    from: 'PrintaDelivery <contacto@printadelivery.com>',
                                    to: current,
                                    subject: 'Hay una nueva orden en Printa',
                                    html: string
                                },
                                function (error, body) {
                                    logger.info('email successfully sent to user ' + current);
                                }
                            );
                        });
                    }
                });
            } else {
                logger.info('record found!');
            }
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    socket.on('save-order-status', function (order) {
        var query = "UPDATE assigned_orders SET \"order\" = '" + JSON.stringify(order) +
            "' WHERE \"order\"->>'id' = '" + order.id + "'";
            console.log(query);
        dbPool.query(query)
        .then(function () {
            logger.info('order ' + order.id + ' has successfully been saved');

            // we should notify the other admins of this
            socket.to('admins').emit('dashboard-update-status', order);
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    // TODO: see what to make with this piece of code
    socket.on('cash-payment-done', function (msg) {
        var orderId = msg.orderId;
        var userEmail = msg.userEmail;
        dbPool.query("SELECT * FROM assigned_orders WHERE \"order\"->>'id' = '" + orderId + "'")
        .then(function (res) {
            if (res.rows[0]) {
                var order = res.rows[0]['order'];

                // update order payment type
                order.paymentType = 'cash';
                // the order starts printing
                order.status = 'imprimiendo';
                // the order is now in status flowStatus
                order.flowStatus = 'estado';

                sendPrintingEmail(userEmail);

                // save the order
                var query = "UPDATE assigned_orders SET \"order\" = '" + JSON.stringify(order)
                    + "' where \"order\"->>'id' = '" + orderId + "'";
                dbPool.query(query)
                .then(function (r) {
                    // send the new order
                    // var admin = res.rows[0]['assigned_to'];
                    // io.to(admins[admin].socketId).emit('payment-done', order);
                    io.to('admins').emit('payment-done', order);
                }).catch(function (err) {
                    logger.error(err);
                });
            }
        }).catch(function (err) {
            logger.error(err);
        });

        // make sure the user is on the correct socket
        users[userEmail.email] = socket.id;
    });

    socket.on('finished-quote', function (order) {
        var query = "UPDATE assigned_orders SET \"order\" = '" + JSON.stringify(order) +
            "' WHERE \"order\"->>'id' = '" + order.id + "'";
        dbPool.query(query)
        .then(function (res) {
            io.to(users[order.owner.email]).emit('finished-quote', order);
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    socket.on('quote-accepted', function (order) {
        // get the admin who has this order
        dbPool.query("SELECT assigned_to FROM assigned_orders WHERE \"order\"->>'id' = '" + order.id + "'")
        .then(function (res) {
            if (res.rows[0]) {
                // add the new status to the order, namely payment
                order.flowStatus = 'paga';

                // save the order
                var query = "UPDATE assigned_orders SET \"order\" = '" + JSON.stringify(order)
                    + "' where \"order\"->>'id' = '" + order.id + "'";
                dbPool.query(query)
                .then(function (r) {
                    // send the new order
                    // var admin = res.rows[0]['assigned_to'];
                    // io.to(admins[admin].socketId).emit('quote-accepted', order);
                    io.to('admins').emit('quote-accepted', order);
                    logger.info('quote was accepted on order ' + order.id);
                }).catch(function (err) {
                    logger.error(err);
                });
            }
        });
    });

    socket.on('order-status-change', function (order) {
        var query = "UPDATE assigned_orders SET \"order\" = '" + JSON.stringify(order) +
            "' WHERE \"order\"->>'id' = '" + order.id + "'";
        dbPool.query(query)
        .then(function (res) {
            if (order.status === 'despachado' && PRODUCTION) {
                sendDispatchEmail(order.owner.email);
            }
            socket.broadcast.emit('order-status-change', order);
            logger.info('to dashboard-update-status');
            io.to('admins').emit('dashboard-update-status', order);
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    socket.on('get-order', function (orderId) {
        logger.info('socket ' + socket.id + ' wants order ' + orderId);
        var query = "SELECT \"order\" FROM assigned_orders WHERE \"order\"->>'id' = '" + orderId + "'";
        dbPool.query(query)
        .then(function (res) {
            if (res.rows[0]) {
                logger.info('sending order ' + orderId + ' to socket ' + socket.id);
                io.to(socket.id).emit('order-status-change', res.rows[0].order);
            } else {
                logger.info('order ' + orderId + ' was not found');
                io.to(socket.id).emit('order-not-found', orderId);
            }
        })
        .catch(function (err) {
            logger.info(err);
        });
    });

    socket.on('delete-order', function (orderId) {
        logger.info('socket ' + socket.id + ' requests to delete order ' + orderId);
        var query = "DELETE FROM assigned_orders WHERE \"order\"->>'id' = '" + orderId + "'";
        dbPool.query(query)
        .then(function (res) {
            logger.info('order ' + orderId + ' successfully deleted, alerting sockets');
            io.emit('order-deleted', orderId);
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    socket.on('get-assigned-orders', function (adminEmail) {
        dbPool.query("select \"order\",\"creation_date\" from assigned_orders " +
            // "where assigned_to = '" + adminEmail + "'" +
            " order by creation_date desc")
        .then(function (res) {
            if (res.rows) {
                io.to(socket.id).emit('assigned-orders', res.rows);
            }
        });
    });

    socket.on('get-user-orders', function (userEmail) {
        logger.info('user ' + userEmail + ' @socket ' + socket.id + ' is requesting his/her orders');
        dbPool.query("select \"order\" from assigned_orders where \"order\"->'owner'->>'email' = '" +
            userEmail + "' order by creation_date desc")
        .then(function (res) {
            if (res.rows) {
                var orders = res.rows.map(function (curr) {
                    return curr['order'];
                });
                io.to(users[userEmail]).emit('user-orders', orders);
            }
        }).catch(function (err) {
            logger.error(err);
        });
    });

    socket.on('quote-time-change', function (msg) {
        var orderId = msg.orderId;
        var quoteTime = msg.quoteTime;

        var query = "SELECT \"order\" FROM assigned_orders WHERE \"order\"->>'id' = '" + orderId + "'";
        dbPool.query(query)
        .then(function (res) {
            if (res.rows[0]) {
                io.to(users[res.rows[0].order.owner.email]).emit('quote-time-change', quoteTime);
            }
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    /**
     * payment-related events
     */
    socket.on('verify-promo-code', function (dataPromoCode) {
        var todayDate = new Date();
        var dateString = todayDate.toISOString()
        var fecha_string = dateString.substr(0,10);

        var promoCode =  dataPromoCode.promoCode;
        var buyer = dataPromoCode.buyer;

        var validCodeUser = true;

        var query = "SELECT * FROM promo_codes WHERE codigo = '" + promoCode + "' AND habilitado = true AND fecha_inicio <= '"+ fecha_string +"' AND fecha_caducidad >= '"+ fecha_string +"' ";
        console.log(query);
        dbPool.query(query)
        .then(function (res) {
            console.log('query executed');
            var valid = false;

            var queryorders = "SELECT * FROM assigned_orders WHERE \"order\"->>'promoCode' = '" + promoCode + "' AND \"order\"->'owner'->>'email' = '"+ buyer +"' ";
            console.log(queryorders);
            dbPool.query(queryorders)
            .then(function (resOrders) {
                if (resOrders.rows.length > 0) {
                    validCodeUser = false;
                }else{
                    validCodeUser = true;
                }
                console.log('validCodeuser: '+ validCodeUser);

                if (res.rows[0]) {
                    console.log('found');
                    // promo code was found
                    logger.info('promo code' + promoCode + ' found');
                    if(parseInt(res.rows[0]['cantidad']) > 0 && validCodeUser){
                        valid = true;
                    }else{
                        valid = false;
                    }
                }

                console.log(valid);
                io.to(socket.id).emit('promo-code-valid', {
                    promoCode: promoCode,
                    valid: valid
                });
            })
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    /**
     * Product management-related events
     */
    socket.on('get-products', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM products_normal"),
            dbPool.query("SELECT * FROM products_vinyl"),
            dbPool.query("SELECT * FROM products_laser"),
            dbPool.query("SELECT * FROM products_others")
        ]).then(function (res) {
            var allProds = [];
            for (var i = 0; i < 4; i++) {
                allProds = allProds.concat(res[i].rows);
            }
            io.to(socket.id).emit('products', allProds);
        });
    });

    socket.on('get-normal-print-options', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM products_normal WHERE habilitado = true")
        ]).then(function (res) {
            var allProds = res[0].rows;
            io.to(socket.id).emit('normal-print-options', allProds);
        });
    });

    socket.on('get-vinyl-options', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM products_vinyl WHERE habilitado = true")
        ]).then(function (res) {
            var allProds = res[0].rows;
            io.to(socket.id).emit('vinyl-options', allProds);
        });
    });

    socket.on('get-laser-options', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM products_laser WHERE habilitado = true")
        ]).then(function (res) {
            var allProds = res[0].rows;
            io.to(socket.id).emit('laser-options', allProds);
        });
    });

    socket.on('get-products-for-type', function (type) {
        dbPool.query("SELECT * FROM productos_" + type)
        .then(function (res) {
            if (res.rows) {
                io.to(socket.id).emit('products-for-type', {
                    type: type,
                    products: res.rows
                });
            }
        })
    });


    socket.on('add-product', function (prodInfo) {
        logger.info(prodInfo);

        var query = '';

        switch(prodInfo.category){
            case 'impresion':
                let mtscounter = prodInfo.product.cantidad * prodInfo.product.metro_largo_rollo;
                var query = "INSERT INTO products_normal (material, tamano, gramaje, cantidad, cantidad_alerta, habilitado, valor_unitario, tipo, metro_ancho_max, metro_largo_rollo, mtscounter) " +
                "VALUES ('" + prodInfo.product.material + "', '" + prodInfo.product.tamano + "', " + prodInfo.product.gramaje + ", " +
                prodInfo.product.cantidad + ", " + prodInfo.product.cantidad_alerta + ", " + prodInfo.product.habilitado + 
                ", " + prodInfo.product.valor_unitario + ", 'impresion', "+ prodInfo.product.metro_ancho_max +", "+ prodInfo.product.metro_largo_rollo +", "+ mtscounter +")";
            break;
            case 'vinilo':
            
                let mtscounterVin = prodInfo.product.cantidad * prodInfo.product.metro_largo_rollo;
                var query = "INSERT INTO products_vinyl (material, acabado, formato, gramaje, cantidad, metro_ancho_max, cantidad_alerta, habilitado, valor_unitario, metro_largo_rollo, tipo, color, mtscounter) " +
                "VALUES ('Vinilo', '" + prodInfo.product.acabado + "', 'Rollo', " + prodInfo.product.gramaje + "," + prodInfo.product.cantidad + ", " + prodInfo.product.metro_ancho_max + ", " + prodInfo.product.cantidad_alerta + ", " + prodInfo.product.habilitado + ", " + prodInfo.product.valor_unitario + ", " + prodInfo.product.metro_largo_rollo + ", 'vinilo', '" + prodInfo.product.color + "',"+ mtscounterVin +")";
            break;
            case 'laser':
                var query = "INSERT INTO products_laser (valor_unitario, material, habilitado, espesor, color, cantidad_alerta, cantidad, tipo) " +
                "VALUES (" + prodInfo.product.valor_unitario + ", '" + prodInfo.product.material + "', " + prodInfo.product.habilitado + "," + prodInfo.product.espesor + ", '" + prodInfo.product.color + "', " + prodInfo.product.cantidad_alerta + ", " + prodInfo.product.cantidad + ", 'laser')";
            break;
            case 'tinta':
                var query = "INSERT INTO products_others (material, cantidad, cantidad_alerta, valor_unitario, tipo) " +
                "VALUES ('" + prodInfo.product.material + "', " + prodInfo.product.cantidad + ", " + prodInfo.product.cantidad_alerta + "," + prodInfo.product.valor_unitario + ", 'tinta')";
            break;
        }

        dbPool.query(query)
        .then(function(res) {
            // send back all products to the user so that the event listener in the frontend
            // shows the new products
            Promise.all([
                dbPool.query("SELECT * FROM products_normal"),
                dbPool.query("SELECT * FROM products_vinyl"),
                dbPool.query("SELECT * FROM products_laser"),
                dbPool.query("SELECT * FROM products_others")
            ]).then(function (res) {
                var allProds = [];
                for (var i = 0; i < 4; i++) {
                    allProds = allProds.concat(res[i].rows);
                }
                io.to(socket.id).emit('products', allProds);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('edit-product', function (prodInfo) {
        logger.info(prodInfo);

        var query = '';

        switch(prodInfo.category){
            case 'impresion':
                let mtscounterimp = prodInfo.product.cantidad * prodInfo.product.metro_largo_rollo;
                var query = "UPDATE products_normal SET material='"+ prodInfo.product.material +"', gramaje="+ 
                prodInfo.product.gramaje +", cantidad="+ prodInfo.product.cantidad +", cantidad_alerta="+ 
                prodInfo.product.cantidad_alerta +", metro_largo_rollo="+ prodInfo.product.metro_largo_rollo +
                ", metro_ancho_max="+ prodInfo.product.metro_ancho_max + ", mtscounter="+ mtscounterimp +
                ", valor_unitario="+ prodInfo.product.valor_unitario +", tamano='"+ prodInfo.product.tamano +
                "', habilitado="+ prodInfo.product.habilitado +", tipo='impresion' WHERE id = "+ prodInfo.product.id +"";
            break;
            case 'vinilo':
                let mtscounterVin = prodInfo.product.cantidad * prodInfo.product.metro_largo_rollo;
                var query = "UPDATE products_vinyl SET formato='Rollo', gramaje="+ prodInfo.product.gramaje +
                ", cantidad="+ prodInfo.product.cantidad +", metro_ancho_max="+ prodInfo.product.metro_ancho_max +
                ", cantidad_alerta="+ prodInfo.product.cantidad_alerta +", valor_unitario="+ prodInfo.product.valor_unitario +
                ", metro_largo_rollo="+ prodInfo.product.metro_largo_rollo +", acabado='"+ prodInfo.product.acabado +
                "', habilitado="+ prodInfo.product.habilitado +", tipo='vinilo', material='Vinilo', color='"+ prodInfo.product.color +
                "', mtscounter="+ mtscounterVin +
                " WHERE id="+ prodInfo.product.id +" ";
            break;
            case 'laser':
                var query = "UPDATE products_laser SET valor_unitario="+ prodInfo.product.valor_unitario +
                ", material='"+ prodInfo.product.material +"', espesor="+ prodInfo.product.espesor +
                ", cantidad_alerta="+ prodInfo.product.cantidad_alerta +", cantidad="+ prodInfo.product.cantidad +
                ", color='"+ prodInfo.product.color +"', habilitado="+ prodInfo.product.habilitado +
                ", tipo='laser' WHERE id="+ prodInfo.product.id +" ";
            break;
            case 'tinta':
                var query = "UPDATE products_others SET cantidad="+ prodInfo.product.cantidad +
                ", cantidad_alerta="+ prodInfo.product.cantidad_alerta +", valor_unitario="+ prodInfo.product.valor_unitario +
                ", tipo='tinta', material='"+ prodInfo.product.material +"' WHERE id="+ prodInfo.product.id +" ";
            break;
        }
        
        dbPool.query(query)
        .then(function(res) {
            // send back all products to the user so that the event listener in the frontend
            // shows the new products
            Promise.all([
                dbPool.query("SELECT * FROM products_normal"),
                dbPool.query("SELECT * FROM products_vinyl"),
                dbPool.query("SELECT * FROM products_laser"),
                dbPool.query("SELECT * FROM products_others")
            ]).then(function (res) {
                var allProds = [];
                for (var i = 0; i < 4; i++) {
                    allProds = allProds.concat(res[i].rows);
                }
                io.to(socket.id).emit('products', allProds);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('delete-product', function (prodInfo) {
        logger.info(prodInfo);

        var query = '';

        switch(prodInfo.category){
            case 'impresion':
                var query = "DELETE FROM products_normal WHERE id="+ prodInfo.product.id +" ";
            break;
            case 'vinilo':
                var query = "DELETE FROM products_vinyl WHERE id="+ prodInfo.product.id +" ";
            break;
            case 'laser':
                var query = "DELETE FROM products_laser WHERE id="+ prodInfo.product.id +" ";
            break;
            case 'tinta':
                var query = "DELETE FROM products_others WHERE id="+ prodInfo.product.id +" ";
            break;
        }

        dbPool.query(query)
        .then(function(res) {
            // send back all products to the user so that the event listener in the frontend
            // shows the new products
            Promise.all([
                dbPool.query("SELECT * FROM products_normal"),
                dbPool.query("SELECT * FROM products_vinyl"),
                dbPool.query("SELECT * FROM products_laser"),
                dbPool.query("SELECT * FROM products_others")
            ]).then(function (res) {
                var allProds = [];
                for (var i = 0; i < 4; i++) {
                    allProds = allProds.concat(res[i].rows);
                }
                io.to(socket.id).emit('products', allProds);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('remove-product', function (prodInfo) {
        logger.info('Quit product item form stock');
        var query = "";
        switch(prodInfo.product.printSettings.type){
            case 'normal':
                var query = "UPDATE products_normal SET cantidad = cantidad - "+ prodInfo.sheets +" where material = '" + prodInfo.product.printSettings.paper + 
                "' AND tamano = '"+ prodInfo.product.printSettings.size +"' AND gramaje = "+ prodInfo.product.printSettings.grams +" ";
                break;
            case 'laser':
                var query = "UPDATE products_laser SET cantidad = cantidad - "+ prodInfo.sheets +" where material = '" + prodInfo.product.printSettings.paper + 
                "' AND color = '"+ prodInfo.product.printSettings.color +"' ";
                break;
            case 'vinilo':
                var query = "UPDATE products_vinyl  SET cantidad = cantidad - "+ prodInfo.sheets +" where acabado = '" + prodInfo.product.printSettings.paper + 
                "' AND color = '"+ prodInfo.product.printSettings.color +"' AND gramaje = "+ prodInfo.product.printSettings.grams +" ";
                break;
        }
        console.log(query);
        dbPool.query(query)
        .then(function (res) {
            logger.info('product item form stock removed');
            io.emit('product-removed', prodInfo);
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    socket.on('remove-product-vinyl', function (prodInfo) {
        logger.info('Search product to validate roll qnt');
        
        var query = "SELECT * FROM products_vinyl WHERE acabado = '" + prodInfo.product.printSettings.paper + 
        "' AND color = '"+ prodInfo.product.printSettings.color +"' AND gramaje = "+ prodInfo.product.printSettings.grams +" ";
        
        console.log(query);
        dbPool.query(query)
        .then(function (res) {
            var sheets = prodInfo.sheets;
            var mtsRoll = res.rows[0]['metro_largo_rollo'];
            var rolls = res.rows[0]['cantidad'];
            var counter = res.rows[0]['mtscounter'];
            var idProduct = res.rows[0]['id'];
            var x = 0;
            var query2 = '';
            var query3 = '';

            console.log(sheets);
            console.log(counter);

            if(sheets >= counter){
                var x = sheets - counter;
                rolls = rolls -1;
                var counter = mtsRoll;
                query2 = "UPDATE products_vinyl SET cantidad="+ rolls +", mtscounter="+ counter +" WHERE id="+ idProduct +" ";
                console.log(query2);
                Promise.all([
                    dbPool.query(query2)
                ]).then(function (roll) {
                    
                    if(x > counter){
                        while(x > 0){
                            x = x - counter;
                            rolls = rolls -1;
                            if(x < counter){
                                counter = counter - x;
                                x = 0;
                            }
                        }
                        query3 = "UPDATE products_vinyl SET cantidad="+ rolls +", mtscounter="+ counter +" WHERE id="+ idProduct +" ";
                        console.log(query3);
                        Promise.all([
                            dbPool.query(query3)
                        ]).then(function (roll) {
                            logger.info('less rolls and counter changed');
                            io.emit('product-removed', prodInfo);
                        });

                    }else{
                        counter = counter - x;
                        query3 = "UPDATE products_vinyl SET mtscounter="+ counter +" WHERE id="+ idProduct +" ";
                        console.log(query3);
                        Promise.all([
                            dbPool.query(query3)
                        ]).then(function (roll) {
                            logger.info('counter changed');
                            io.emit('product-removed', prodInfo);
                        });
                    }
                    
                    logger.info('one roll less, mtscounter updated');
                    io.emit('product-removed', prodInfo);
                });
            }else{
                var x = counter - sheets;
                query2 = "UPDATE products_vinyl SET mtscounter="+ x +" WHERE id="+ idProduct +" ";
                console.log(query2);
                Promise.all([
                    dbPool.query(query2)
                ]).then(function (roll) {
                    logger.info('counter changed');
                    io.emit('product-removed', prodInfo);
                });
            }
            logger.info('product item from stock removed');
            io.emit('product-removed', prodInfo);
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    socket.on('remove-product-normal', function (prodInfo) {
        logger.info('Search product to validate roll qnt');
        
        var query = "SELECT * FROM products_normal WHERE material = '" + prodInfo.product.printSettings.paper + 
        "' AND tamano = '"+ prodInfo.product.printSettings.size +"' AND gramaje = "+ prodInfo.product.printSettings.grams +" ";
        
        console.log(query);
        dbPool.query(query)
        .then(function (res) {
            var sheets = prodInfo.sheets;
            var mtsRoll = res.rows[0]['metro_largo_rollo'];
            var rolls = res.rows[0]['cantidad'];
            var counter = res.rows[0]['mtscounter'];
            var idProduct = res.rows[0]['id'];
            var x = 0;
            var query2 = '';
            var query3 = '';

            console.log(sheets);
            console.log(counter);

            if(sheets >= counter){
                var x = sheets - counter;
                rolls = rolls -1;
                var counter = mtsRoll;
                query2 = "UPDATE products_normal SET cantidad="+ rolls +", mtscounter="+ counter +" WHERE id="+ idProduct +" ";
                console.log(query2);
                Promise.all([
                    dbPool.query(query2)
                ]).then(function (roll) {
                    
                    if(x > counter){
                        while(x > 0){
                            x = x - counter;
                            rolls = rolls -1;
                            if(x < counter){
                                counter = counter - x;
                                x = 0;
                            }
                        }
                        query3 = "UPDATE products_normal SET cantidad="+ rolls +", mtscounter="+ counter +" WHERE id="+ idProduct +" ";
                        console.log(query3);
                        Promise.all([
                            dbPool.query(query3)
                        ]).then(function (roll) {
                            logger.info('less rolls and counter changed');
                            io.emit('product-normal-removed', prodInfo);
                        });

                    }else{
                        counter = counter - x;
                        query3 = "UPDATE products_normal SET mtscounter="+ counter +" WHERE id="+ idProduct +" ";
                        console.log(query3);
                        Promise.all([
                            dbPool.query(query3)
                        ]).then(function (roll) {
                            logger.info('counter changed');
                            io.emit('product-normal-removed', prodInfo);
                        });
                    }
                    
                    logger.info('one roll less, mtscounter updated');
                    io.emit('product-normal-removed', prodInfo);
                });
            }else{
                var x = counter - sheets;
                query2 = "UPDATE products_normal SET mtscounter="+ x +" WHERE id="+ idProduct +" ";
                console.log(query2);
                Promise.all([
                    dbPool.query(query2)
                ]).then(function (roll) {
                    logger.info('counter changed');
                    io.emit('product-normal-removed', prodInfo);
                });
            }
            logger.info('product item from stock removed');
            io.emit('product-normal-removed', prodInfo);
        })
        .catch(function (err) {
            console.error(err);
        });
    });

    /**
     * Codigos de promoción
     */

    socket.on('get-codes', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM promo_codes ORDER BY id ASC")
        ]).then(function (res) {
            //traer la cantidad de veces usado el codigo
            res[0].rows.forEach(element => {
                dbPool.query("SELECT * FROM assigned_orders WHERE \"order\"->>'promoCode' = '" + element.codigo +"'")
                .then(function (resOrders) {
                    element.used = resOrders.rows.length;
                    io.to(socket.id).emit('codes', res[0].rows);
                });
            });
            
        });
    });

    socket.on('add-code', function (codeInfo) {
        logger.info(codeInfo);

        var inicioDate = new Date(codeInfo.code.fecha_inicio);
        var inicioDateString = inicioDate.toISOString()

        var caducidadDate = new Date(codeInfo.code.fecha_caducidad);
        var caducidadDateString = caducidadDate.toISOString()
        
        var query = "INSERT INTO promo_codes (codigo, cantidad, valor, fecha_inicio, fecha_caducidad, habilitado, tipo, cantidad_total) " +
        "VALUES ('" + codeInfo.code.codigo + "', " + codeInfo.code.cantidad + ", " + codeInfo.code.valor + ", '" +
        inicioDateString + "', '" + caducidadDateString + "', true, " + codeInfo.code.tipo + ", " + codeInfo.code.cantidad + ")";

        dbPool.query(query)
        .then(function(res) {
            // send back all products to the user so that the event listener in the frontend
            
            // shows the new products
            Promise.all([
                dbPool.query("SELECT * FROM promo_codes")
            ]).then(function (res) {
                
                //traer la cantidad de veces usado el codigo
                res[0].rows.forEach(element => {
                    dbPool.query("SELECT * FROM assigned_orders WHERE \"order\"->>'promoCode' = '" + element.codigo +"'")
                    .then(function (resOrders) {
                        element.used = resOrders.rows.length;
                        io.to(socket.id).emit('codes', res[0].rows);
                    });
                });

            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('change-state-code', function (codeInfo) {
        logger.info(codeInfo);

        var query = "UPDATE promo_codes SET habilitado="+ codeInfo.habilitado +" WHERE id = "+ codeInfo.id +" ";

        dbPool.query(query)
        .then(function(res) {
            // send back all products to the user so that the event listener in the frontend
            // shows the new products
            Promise.all([
                dbPool.query("SELECT * FROM promo_codes ORDER BY id ASC")
            ]).then(function (res) {
                io.to(socket.id).emit('codes', res[0].rows);
                //traer la cantidad de veces usado el codigo
                res[0].rows.forEach(element => {
                    dbPool.query("SELECT * FROM assigned_orders WHERE \"order\"->>'promoCode' = '" + element.codigo +"'")
                    .then(function (resOrders) {
                        element.used = resOrders.rows.length;
                        io.to(socket.id).emit('codes', res[0].rows);
                    });
                });
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    /**
     * Lugares de entrega
     */

    socket.on('get-places', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM places_delivery")
        ]).then(function (res) {
            io.to(socket.id).emit('place-delivery', res[0].rows);
        });
    });

    socket.on('add-place', function (placeInfo) {
        logger.info(placeInfo);

        var query = "INSERT INTO places_delivery (lugar, referencia, valor_envio, hora_inicio, hora_fin, habilitado) " +
        "VALUES ('" + placeInfo.place.lugar + "', '" + placeInfo.place.referencia + "', " + placeInfo.place.valor_envio + ", '" +
        placeInfo.place.hora_inicio + "', '" + placeInfo.place.hora_fin + "', "+ placeInfo.place.habilitado +" )";

        console.log(query);

        dbPool.query(query)
        .then(function(res) {
            Promise.all([
                dbPool.query("SELECT * FROM places_delivery")
            ]).then(function (res) {
                io.to(socket.id).emit('place-delivery', res[0].rows);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('edit-place', function (placeInfo) {
        logger.info(placeInfo);
        
        var query = "UPDATE places_delivery SET lugar='" + placeInfo.place.lugar + "', referencia='" + 
        placeInfo.place.referencia + "', hora_inicio='" + placeInfo.place.hora_inicio + "', hora_fin='" + 
        placeInfo.place.hora_fin + "', habilitado="+ placeInfo.place.habilitado +", valor_envio=" + 
        placeInfo.place.valor_envio + " WHERE id="+ placeInfo.place.id +" ";

        console.log(query);
        
        dbPool.query(query)
        .then(function(res) {
            Promise.all([
                dbPool.query("SELECT * FROM places_delivery")
            ]).then(function (res) {
                io.to(socket.id).emit('place-delivery', res[0].rows);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('delete-place', function (placeInfo) {
        logger.info(placeInfo);

        var query = '';

        var query = "DELETE FROM places_delivery WHERE id="+ placeInfo.place.id +" ";   

        dbPool.query(query)
        .then(function(res) {
            Promise.all([
                dbPool.query("SELECT * FROM places_delivery")
            ]).then(function (res) {
                io.to(socket.id).emit('place-delivery', res[0].rows);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    /* lugares de entrega en horario habil */

    socket.on('get-valid-places', function (msg) {

        var datemy = new Date();
        var DateUTC = datemy.getTime() - new Date().getTimezoneOffset();
        var DateNow = new Date(DateUTC);
        var dateString = String(DateNow);

        var time_string = dateString.substr(15,8);

        Promise.all([
            dbPool.query("SELECT * FROM places_delivery WHERE hora_inicio <= '"+ time_string +"' AND hora_fin >= '"+ time_string +"' AND habilitado = true")
        ]).then(function (res) {
            io.to(socket.id).emit('valid-place-delivery', res[0].rows);
        });

    });

    /* videos */

    socket.on('get-videos', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM video ORDER BY position ASC")
        ]).then(function (res) {
            io.to(socket.id).emit('get-all-videos', res[0].rows);
        });
    });

    socket.on('get-videos-front', function (msg) {
        Promise.all([
            dbPool.query("SELECT * FROM video WHERE show = true ORDER BY position ASC")
        ]).then(function (res) {
            io.to(socket.id).emit('get-all-videos-front', res[0].rows);
        });
    });

    socket.on('change-positions', function (infoPositions) {
        logger.info(infoPositions);
        var inicial = infoPositions.infoPositions.inicial;
        var final = infoPositions.infoPositions.final;
        var query = "";
        
        if(inicial > final){
            query = "SELECT * FROM video WHERE position < "+ inicial +" AND position >= "+ final;
        }else{
            query = "SELECT * FROM video WHERE position > "+ inicial +" AND position <= "+ final;
        }

        console.log(query);

        dbPool.query(query)
        .then(function(res) {
            if(inicial > final){
                for (var i = 0; i < res.rows.length; i++) {
                    var newposition = res.rows[i].position + 1;
                    dbPool.query('UPDATE video SET position ='+ newposition + ' WHERE id = '+ res.rows[i].id)
                    .then(function(res) {
                        console.log('CHANGED');
                    }).catch(function(err){
                        console.log(err);
                    });
                }
            }else{
                for (var i = 0; i < res.rows.length; i++) {
                    var newposition = res.rows[i].position - 1;
                    dbPool.query('UPDATE video SET position ='+ newposition + ' WHERE id = '+ res.rows[i].id)
                    .then(function(res) {
                        console.log('CHANGED');
                    }).catch(function(err){
                        console.log(err);
                    });
                }
            }
        }).catch(function(err){
            console.log(err);
        });
    });

    
    socket.on('count-videos', function (msg) {
        Promise.all([
            dbPool.query("SELECT COUNT(*) FROM video")
        ]).then(function (res) {
            io.to(socket.id).emit('get-count-videos', res[0].rows);
        });
    });

    socket.on('change-video', function (videoInfo) {
        logger.info(videoInfo);

        var query = "UPDATE video SET show = "+ videoInfo.videoInfo.show +" WHERE id = "+ videoInfo.videoInfo.id;

        console.log(query);

        dbPool.query(query)
        .then(function(res) {
            Promise.all([
                dbPool.query("SELECT * FROM video ORDER BY position ASC")
            ]).then(function (res) {
                io.to(socket.id).emit('get-all-videos', res[0].rows);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('add-video', function (videoInfo) {
        logger.info(videoInfo);

        var query = "INSERT INTO video (title, url, show, position, videocode) VALUES ('" + videoInfo.videoInfo.title + 
        "', '" + videoInfo.videoInfo.url + "', false, " + videoInfo.videoInfo.position + ", '" + videoInfo.videoInfo.videocode + "')";

        console.log(query);

        dbPool.query(query)
        .then(function(res) {
            Promise.all([
                dbPool.query("SELECT * FROM video ORDER BY position ASC")
            ]).then(function (res) {
                io.to(socket.id).emit('get-all-videos', res[0].rows);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('update-video', function (video) {
        logger.info(video);

        var query = "UPDATE video SET title = '"+ video.video.title +"', url = '"+ video.video.url +"', position = "+ video.video.position +", videocode = '"+ video.video.videocode+
        "' WHERE id = "+ video.idVideo;

        console.log(query);

        dbPool.query(query)
        .then(function(res) {
            Promise.all([
                dbPool.query("SELECT * FROM video ORDER BY position ASC")
            ]).then(function (res) {
                io.to(socket.id).emit('get-all-videos', res[0].rows);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    socket.on('delete-video', function (video) {
        logger.info(video);
        
        var query = "DELETE FROM video WHERE id="+ video.video +" ";

        console.log(query);

        dbPool.query(query)
        .then(function(res) {
            Promise.all([
                dbPool.query("SELECT * FROM video ORDER BY position ASC")
            ]).then(function (res) {
                io.to(socket.id).emit('get-all-videos', res[0].rows);
            });
        }).catch(function(err){
            console.log(err);
        });
    });

    /**
     * socket-dependent functions
     */
    function registerAdminConnection(adminEmail) {
        // TODO: this should be changed in a further version
        // assigns all orders to the universal printadelivery user
        adminEmail = UNIVERSAL_ADMIN;

        logger.info('admin connected');

        // force the admin to join the admins room
        socket.join('admins');

        // if an admin logs in, they should be available to start receiving orders.
        // we id admins by their emails, and store their assigned orders and socketIds
        admins[adminEmail] = {
            assignedOrders: getAssignedOrders(adminEmail),
            socketId: socket.id
        };
        leastOccupiedAdmin = adminEmail;
    }
});

io.on('error', function (error) {
    console.error(error);
});

/**
 * Application logic
 */

function getAssignedOrders(adminEmail) {
    // TODO: this should check the dbPool or whatever to know the REAL amount of assigned orders
    return 0;
}

function sendAssignedOrdersTo(adminEmail, socketId) {
    // fetch the admin's assigned orders and send them to the admin
    dbPool.query("select \"order\",\"creation_date\" from assigned_orders where assigned_to = '" + adminEmail + "' order by creation_date desc")
    .then(function (res) {
        if (res.rows) {
            io.to(socketId).emit('assigned-orders', res.rows);
        }
    });
}

// TODO: see wth is wrong with this
function setLeastOccupiedAdmin() {
    var lowestOccupation = Number.MAX_SAFE_INTEGER;
    admins.forEach(function (admin, index) {
        if (admin.assignedOrders <= lowestOccupation) {
            leastOccupiedAdmin = index;
        }
    });
}

function sendPrintingEmail(userEmail) {
    mailgun.messages().send({
        from: 'PrintaDelivery <contacto@printadelivery.com>',
        to: userEmail,
        subject: 'Tu orden se encuentra en impresión',
        html: '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body> <div> <center> <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding:9px"> <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%"> <tbody><tr> <td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center"> <img align="center" alt="" src="https://gallery.mailchimp.com/a1df14b37221231d0591a5773/images/4f7ff180-9e4a-4416-87de-83e3c8e3bb14.jpg" width="300" style="max-width:300px;padding-bottom:0;display:inline!important;vertical-align:bottom"> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding:0px 18px 9px;color:#585656"> <div style="text-align:center"><br><br><span style="font-size:24px"><strong><font face="arial, helvetica neue, helvetica, sans-serif"><span style="line-height:32px">&iexcl;Tu pedido est&aacute; en proceso de impresi&oacute;n/corte!</span></font></strong></span><br><br><font face="arial, helvetica neue, helvetica, sans-serif"><span style="font-size:17px;line-height:22.4px">Si ocurre alg&uacute;n inconveniente, nos contactaremos inmediatamente contigo.</span></font><br><br><br><span style="font-size:19px;line-height:22.4px">PrintaDelivery</span><br>#PrintaTeSalva<br></div></td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td style="min-width:100%;padding:18px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top:2px solid #eaeaea"> <tbody><tr> <td> <span></span> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td align="center" valign="top" style="padding:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody><tr> <td align="center" style="padding-left:9px;padding-right:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody><tr> <td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px"> <table align="center" border="0" cellpadding="0" cellspacing="0"> <tbody><tr> <td align="center" valign="top"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline"> <tbody><tr> <td valign="top" style="padding-right:10px;padding-bottom:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr> <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" width=""> <tbody><tr> <td align="center" valign="middle" width="24"> <a href="https://www.facebook.com/printadelivery/?fref=ts&amp;ref=br_tf" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-facebook-48.png" style="display:block" height="24" width="24"></a> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline"> <tbody><tr> <td valign="top" style="padding-right:0;padding-bottom:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr> <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" width=""> <tbody><tr> <td align="center" valign="middle" width="24"> <a href="https://printadelivery.com/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-link-48.png" style="display:block" height="24" width="24"></a> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px"> <p style="text-align:center"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px">Cra. 9 #67a-19 Oficina 210<br>Cel: +(57) 313-433-0750</span></span><br><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><a href="mailto:contacto@printadelivery.com" target="_blank">contacto@printadelivery.com</a></span></span></p></td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td style="min-width:100%;padding:10px 18px 25px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top-width:2px;border-top-style:solid;border-top-color:#eeeeee"> <tbody><tr> <td> <span></span> </td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px"> <em>Copyright © 2016 Grupo Printa SAS, All rights reserved.</em><br></td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"></td></tr></table> </td></tr></table> </center> </div></body></html>'
    }, function (error, body) {
        logger.info('printing email successfully sent to user ' + userEmail);
    });
}

function sendDispatchEmail(userEmail) {
    mailgun.messages().send({
        from: 'PrintaDelivery <contacto@printadelivery.com>',
        to: userEmail,
        subject: 'Ya despachamos tu pedido',
        html: '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body> <div> <center> <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding:9px"> <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%"> <tbody><tr> <td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center"> <img align="center" alt="" src="https://gallery.mailchimp.com/a1df14b37221231d0591a5773/images/4f7ff180-9e4a-4416-87de-83e3c8e3bb14.jpg" width="300" style="max-width:300px;padding-bottom:0;display:inline!important;vertical-align:bottom"> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding:0px 18px 9px;color:#585656"> <div style="text-align:center"><br><br><span style="font-size:24px"><strong><font face="arial, helvetica neue, helvetica, sans-serif"><span style="line-height:32px">&iexcl;Tu pedido ha sido despachado!</span></font></strong></span><br><br><br><span style="font-size:19px;line-height:22.4px">PrintaDelivery</span><br>#PrintaTeSalva<br></div></td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td style="min-width:100%;padding:18px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top:2px solid #eaeaea"> <tbody><tr> <td> <span></span> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td align="center" valign="top" style="padding:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody><tr> <td align="center" style="padding-left:9px;padding-right:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody><tr> <td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px"> <table align="center" border="0" cellpadding="0" cellspacing="0"> <tbody><tr> <td align="center" valign="top"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline"> <tbody><tr> <td valign="top" style="padding-right:10px;padding-bottom:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr> <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" width=""> <tbody><tr> <td align="center" valign="middle" width="24"> <a href="https://www.facebook.com/printadelivery/?fref=ts&amp;ref=br_tf" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-facebook-48.png" style="display:block" height="24" width="24"></a> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline"> <tbody><tr> <td valign="top" style="padding-right:0;padding-bottom:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr> <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" width=""> <tbody><tr> <td align="center" valign="middle" width="24"> <a href="https://printadelivery.com/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-link-48.png" style="display:block" height="24" width="24"></a> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px"> <p style="text-align:center"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px">Cra. 9 #67a-19 Oficina 210<br>Cel: +(57) 313-433-0750</span></span><br><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><a href="mailto:contacto@printadelivery.com" target="_blank">contacto@printadelivery.com</a></span></span></p></td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td style="min-width:100%;padding:10px 18px 25px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top-width:2px;border-top-style:solid;border-top-color:#eeeeee"> <tbody><tr> <td> <span></span> </td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px"> <em>Copyright © 2016 Grupo Printa SAS, All rights reserved.</em><br></td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"></td></tr></table> </td></tr></table> </center> </div></body></html>'
    }, function (error, body) {
        logger.info('dispatch email successfully sent to user ' + userEmail);
    });
}

function sendReceptionEmail(userEmail) {
    mailgun.messages().send({
        from: 'PrintaDelivery <contacto@printadelivery.com>',
        to: userEmail,
        subject: 'Hemos recibido tus archivos',
        html: '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body> <div> <center> <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"> <tr> <td align="center" valign="top"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding:9px"> <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="min-width:100%"> <tbody><tr> <td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center"> <img align="center" alt="" src="https://gallery.mailchimp.com/a1df14b37221231d0591a5773/images/4f7ff180-9e4a-4416-87de-83e3c8e3bb14.jpg" width="300" style="max-width:300px;padding-bottom:0;display:inline!important;vertical-align:bottom"> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding:0px 18px 9px;color:#585656"> <div style="text-align:center"><br><br><span style="font-size:25px"><font face="arial, helvetica neue, helvetica, sans-serif"><span style="line-height:32px">&iexcl;Hola!</span></font></span><br><br><font face="arial, helvetica neue, helvetica, sans-serif"><span style="font-size:17px;line-height:22.4px">Hemos recibido tus archivos, qu&eacute;date en l&iacute;nea para recibir tu cotizaci&oacute;n. </span></font><br><br><br><span style="font-size:19px;line-height:22.4px">PrintaDelivery</span><br>#PrintaTeSalva<br></div></td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td style="min-width:100%;padding:18px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top:2px solid #eaeaea"> <tbody><tr> <td> <span></span> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td align="center" valign="top" style="padding:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody><tr> <td align="center" style="padding-left:9px;padding-right:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody><tr> <td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px"> <table align="center" border="0" cellpadding="0" cellspacing="0"> <tbody><tr> <td align="center" valign="top"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline"> <tbody><tr> <td valign="top" style="padding-right:10px;padding-bottom:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr> <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" width=""> <tbody><tr> <td align="center" valign="middle" width="24"> <a href="https://www.facebook.com/printadelivery/?fref=ts&amp;ref=br_tf" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-facebook-48.png" style="display:block" height="24" width="24"></a> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline"> <tbody><tr> <td valign="top" style="padding-right:0;padding-bottom:9px"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody><tr> <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" width=""> <tbody><tr> <td align="center" valign="middle" width="24"> <a href="https://printadelivery.com/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-color-link-48.png" style="display:block" height="24" width="24"></a> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table> </td></tr></tbody></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px"> <p style="text-align:center"><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px">Cra. 9 #67a-19 Oficina 210<br>Cel: +(57) 313-433-0750</span></span><br><span style="font-family:arial,helvetica neue,helvetica,sans-serif"><span style="font-size:14px"><a href="mailto:contacto@printadelivery.com" target="_blank">contacto@printadelivery.com</a></span></span></p></td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td style="min-width:100%;padding:10px 18px 25px"> <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top-width:2px;border-top-style:solid;border-top-color:#eeeeee"> <tbody><tr> <td> <span></span> </td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%"> <tbody> <tr> <td valign="top" style="padding-top:9px"> <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%" width="100%"> <tbody><tr> <td valign="top" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px"> <em>Copyright © 2016 Grupo Printa SAS, All rights reserved.</em><br></td></tr></tbody></table> </td></tr></tbody></table></td></tr><tr> <td valign="top"></td></tr></table> </td></tr></table> </center> </div></body></html>'
    }, function (error, body) {
        logger.info('email successfully sent to user ' + userEmail);
    });
}